export const product_list = [
    {
        id: 1,
        productName: "The Sneaky lips",
        productPrice: "38.00",
        productCategory: "Face",
        productRating: 5   
    },
    {
        id: 2,
        productName: "White Facial Cream",
        productPrice: "38.00",
        productCategory: "Face",
        productRating: 4,
    },
    {
        id: 3,
        productName: "Orange Massage Cream",
        productPrice: "55.00",
        productCategory: "Face",
        productRating: 4,
    },
    {
        id: 4,
        productName: "The expert mascaraa",
        productPrice: "35.00",
        productCategory: "Eyes",
        productRating: 4,
        arrivalType: "New"
    },
    {
        id: 5,
        productName: "Velvet Melon High Intensity",
        productPrice: "38.00",
        productCategory: "Eyes",
        productRating: 5,
    },
    {
        id: 6,
        productName: "Leather shopper Bag",
        productPrice: "35.00",
        productCategory: "Eyes",
        productRating: 4,
        discountPercent: "15%",
        discountPrice: "30.00"
    },
    {
        id: 7,
        productName: "Luxe jewel lipstick",
        productPrice: "38.00",
        productCategory: "Eyes",
        productRating: 5,
    },
    {
        id: 8,
        productName: "Penpoint seamless beauty",
        productPrice: "40.00",
        productCategory: "Face",
        productRating: 5,
        discountPercent:"50%",
        discountPrice: "20.00"
    },
]

export const shopping_list = [
    ...product_list,
    {
        id: 9,
        productName: "Valinta Fairness Massage Cream",
        productPrice: "27.00",
        productCategory: "Lips",
        productRating: 4,
        
    },
    {
        id: 10,
        productName: "Massage Cream Cucumber",
        productPrice: "16.00",
        productCategory: "Lips",
        productRating: 4,   
    },
    {
        id: 11,
        productName: "Matte Walnut & Bamboo Scrub",
        productPrice: "32.00",
        productCategory: "Lips",
        productRating: 4,
    },
    {
        id: 12,
        productName: "Castor Oil BP",
        productPrice: "24.00",
        productCategory: "Lips",
        productRating: 4,
    }
]