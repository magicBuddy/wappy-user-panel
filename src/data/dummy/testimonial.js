import Testimonial1 from '../../assets/images/testimonial/TestimonialOne/1.png';
import Testimonial2 from '../../assets/images/testimonial/TestimonialOne/2.png';
import Testimonial3 from '../../assets/images/testimonial/TestimonialOne/3.png';
import Testimonial4 from '../../assets/images/testimonial/TestimonialOne/4.png';
import Testimonial5 from '../../assets/images/testimonial/TestimonialOne/5.png';
import Testimonial6 from '../../assets/images/testimonial/TestimonialOne/6.png';
import Testimonial7 from '../../assets/images/testimonial/TestimonialOne/7.png';
import Testimonial8 from '../../assets/images/testimonial/TestimonialOne/8.png';

export const testimonials = [
  {
    "id": 1,
    "image": Testimonial1,
    "name": "Alexander Ball",
    "city": "New York",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  },
  {
    "id": 2,
    "image": Testimonial2,
    "name": "Izabel Watt",
    "city": "Michigan",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  },
  {
    "id": 3,
    "image": Testimonial3,
    "name": "Rachel Regan",
    "city": "Sydney",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  },
  {
    "id": 4,
    "image": Testimonial4,
    "name": "Malika Kenny",
    "city": "Ha Noi",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  },
  {
    "id": 5,
    "image": Testimonial5,
    "name": "Javier Bender",
    "city": "Tokyo",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  },
  {
    "id": 6,
    "image": Testimonial6,
    "name": "Paul Brookes",
    "city": "Berlin",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  },
  {
    "id": 7,
    "image": Testimonial7,
    "name": "Bilaal Gunn",
    "city": "Denver",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  },
  {
    "id": 8,
    "image": Testimonial8,
    "name": "Musab O'Sullivan",
    "city": "Paris",
    "review": "I love my lash tint! I don't have extremely blonde lashes, but I do like that they can be even darker than they are. It makes my eyes stand out more and I love the way it looks! Now, I just need to add on a bit of mascara for length and I am set.",
    "rate": 4
  }
]
