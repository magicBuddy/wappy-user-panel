import './assets/css/bootstrap.css';
import './assets/css/style.css';
import './assets/css/slick.min.css';
import './assets/css/fontawesome.css';
import './assets/css/jquery.modal.min.css';
import './assets/css/bootstrap-drawer.min.css';
import Home from './pages/HomePage/Home';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import AboutUs from './pages/AboutUsPage/AboutUs';
import React from 'react';
import Shop from './pages/ShopPage/Shop';
import ProductDetailsPage from './pages/ProductDetailsPage/ProductDetailsPage';
import Cart from './pages/CartPage/Cart';
import Checkout from './pages/CheckoutPage/Checkout';
import Contact from './pages/ContactPage/Contact';
import Blogs from './pages/BlogPage/Blogs';
import ScrollToTop from './utils/ScrollToTop/ScrollToTop';
import { Provider, useDispatch } from 'react-redux';
import { store } from './redux/store/store';
import Login from './pages/Auth/Login';
import Register from './pages/Auth/Register';
import ProtectedRoute from './utils/ProtectedRoute/ProtectedRoute';
import jwt_decode from 'jwt-decode';
import { getUserInfo } from './redux/action/action';
import ShopByCategory from './pages/ShopByCategory/ShopByCategory';
import EmailVerification from './pages/EmailVerification/EmailVerification';
import Wishlist from './pages/WishlistPage/Wishlist';
import PaymentStatus from './pages/PaymentStatus/PaymentStatus';
import ForgotPassword from './pages/Auth/ForgotPassword';
import ChangePassword from './pages/Auth/ChangePassword';

function App() {

  const token = localStorage.getItem('access_token_wappy');
  
  if (token) {

    const decoded_token = jwt_decode(token);

    const currentTime = Date.now() / 1000;

    if (decoded_token.exp < currentTime) {

      localStorage.clear();
      window.location.href = '/login';

    }

    else {

        const {auth} = store.getState();
        if (!auth.userDetails.id) store.dispatch(getUserInfo())

    }

  }

  return (
    
    <Provider store={store}>
      <BrowserRouter>
        <ScrollToTop/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/forgot-password" component={ForgotPassword}/>
          <Route exact path="/change-password" component={ChangePassword}/>
          <Route exact path="/email-verification" component={EmailVerification}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/shop" component={Shop}/>
          <Route exact path="/categories/:categoryName" component={ShopByCategory}/>
          <Route exact path="/categories/:categoryName/:subCategoryName" component={ShopByCategory}/>
          <Route exact path="/categories/:categoryName/:subCategoryName/:subSubCategoryName" component={ShopByCategory}/>
          <Route exact path="/product-details/:productId" component={ProductDetailsPage}/>
          <Route exact path="/about" component={AboutUs}/>
          <Route exact path="/blog" component={Blogs}/>
          <Route exact path="/contact" component={Contact}/>
          <Route exact path="/payment-status" component={PaymentStatus}/>
          <ProtectedRoute exact path="/cart" component={Cart}/>
          <ProtectedRoute exact path="/wishlist" component={Wishlist}/>
          <ProtectedRoute exact path="/checkout" component={Checkout}/>
          <Redirect to="/"/>
        </Switch>
      </BrowserRouter>    
    </Provider>  

  );
}

export default App;
