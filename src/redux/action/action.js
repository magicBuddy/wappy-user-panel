import setTokenInHeaders from "../../axios/axios-instance";
import axiosInstance from "../../axios/axios-instance";
import * as actionTypes from './action-type/action-type';

export function setError(error) {

    return {
        type: actionTypes.SET_ERROR,
        payload: error
    }

}

//Action Creators for Product Reducer

export function setProductList(productList) {
    
    return {
        type: actionTypes.SET_PRODUCT_LIST,
        payload: productList
    }

}

export function setProductRangeList(productList) {

    return {
        type: actionTypes.SET_PRODUCT_RANGE_LIST,
        payload: productList
    }

}

export function setNewArrivalsList(productList) {

    return {
        type: actionTypes.SET_NEW_ARRIVALS_LIST,
        payload: productList
    }

}

export function setTrendingImage(trendingImage) {

    return {
        type: actionTypes.SET_TRENDING_IMAGE,
        payload: trendingImage
    }

}

export function setBannerImages(banners) {

    return {
        type: actionTypes.SET_BANNER_IMAGES,
        payload: banners
    }

}

export function setSeasonTrends(seasonTrends) {

    return {
        type: actionTypes.SET_SEASON_TRENDS,
        payload: seasonTrends
    }

}


export function setLoadingState(isLoading) {
    
    return {
        type: actionTypes.SET_LOADING_STATE,
        payload: isLoading
    }

}

export function setProgressState(isProgressing) {

    return {
        type: actionTypes.SET_PROGRESS_STATE,
        payload: isProgressing
    }

}

export function setPaginationLinks(links) {

    return {
        type: actionTypes.SET_PAGINATION_LINKS,
        payload: links
    }

}

export function getProductList(limit, page) {
    
    return async function (dispatch) {

        let response;
        
        try {

            dispatch(setLoadingState(true));
            
            response = await axiosInstance.get('/product', {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
                },
                params: {
                    limit,
                    page
                }               
            })

        }
        
        catch (err) {
            console.log(err);
        }

        if (response && response.data && response.data.code === 200) {

            dispatch(setProductList(response.data.data && response.data.data.data));
            dispatch(setPaginationLinks(response.data.data && response.data.data.links));
            dispatch(setActiveCategory(""));
            dispatch(setLoadingState(false));
        }
    }
}

export function getNewArrivalsList(limit, page) {

    return async function (dispatch) {

        let response;

        try {

            dispatch(setLoadingState(true));
            response = await axiosInstance.get('/product', {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
                },
                params: {
                    limit,
                    page
                }
            })

        } catch (err) {
            console.log(err);
        }

        if (response && response.data && response.data.code === 200) {

            dispatch(setNewArrivalsList(response.data.data && response.data.data.data));
            dispatch(setLoadingState(false));
        }
    }
}

export function setCategories(categories) {

    return {
        type: actionTypes.SET_CATEGORIES,
        payload: categories
    }

}

// export function getCategories() {

//     return async function (dispatch) {

//         const response = await axiosInstance.get('/getAllCategories', {
//             headers: {
//                 Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
//             }
//         })
//         .catch(err => {console.log(err)});

//         if (response && response.data.code === 200) {

//             dispatch(setCategories(response.data.data));

//         }
//     }

// }

export function setActiveCategory (title) {
    
    return {
        type: actionTypes.SET_ACTIVE_CATEGORY,
        payload: title
    }

}


//Action Creators for Cart Reducer

export function updateCart (itemDetails) {

    return {
        type: actionTypes.UPDATE_CART,
        payload: itemDetails
    }

}

export function addItemToCart(itemDetails, itemIndex=null) {

    return async function (dispatch, getState) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.post('/addCartProduct', {
            product_id: itemDetails.productId,
            quantity: itemDetails.productQuantity
        }, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => console.log(err));

        if (response && response.data.code === 200) {

            const cartItemDetails = {...itemDetails};
            cartItemDetails.cartProductId = response.data.data.id;
            cartItemDetails.cartId = response.data.data.cart_id;

            dispatch(setProgressState(false));

            if (itemIndex !== null && itemIndex !== -1 && itemDetails) {
                
                dispatch(updateItemInCart(cartItemDetails, itemIndex));
                dispatch(showSnackbar({message: "Quantity Increased!", type: "info", open: true}))
                const {cart: {cartProducts}} = getState();
                dispatch(calculateTotalPriceOfCartItems(cartProducts));

            }

            else {
            
                dispatch(updateCart(cartItemDetails));
                dispatch(showSnackbar({message: "Cart Updated!", type: "success", open: true}))                    
                const {cart: {cartProducts}} = getState();
                dispatch(calculateTotalPriceOfCartItems(cartProducts));
            
            }
        }
    }
}

export function updateItemInCart(itemDetails, itemIndex) {

    return {
        type: actionTypes.UPDATE_ITEM_IN_CART,
        payload: {itemDetails, itemIndex}
    }

}

export function getCartDetails() {
    return async function (dispatch) {

        const response = await axiosInstance.get('/getCartProducts', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => {

            if (err && !err.response) {

                return;
            }

            else if (err.response.data.code === 401) {

                dispatch(setCartDetails([]));
                dispatch(calculateTotalPriceOfCartItems([]));
            }
        });

        if (response && response.data.code === 200) {

            if (response.data.data.length > 0) {

                const cartDetails = response.data.data.map(
                    ({
                        product_id: productId, 
                        quantity: productQuantity, 
                        name: productName, 
                        price: productPrice,
                        image_url: productImg,
                        id: cartProductId,
                        cart_id: cartId
                    }) => ({productId, productPrice, productQuantity, productName, productImg, cartProductId, cartId}));
                
                dispatch(setCartDetails(cartDetails));
                dispatch(calculateTotalPriceOfCartItems(cartDetails));
            }
        }

    }
}

export function setCartDetails(cartDetails) {

    return {
        type: actionTypes.SET_CART_DETAILS,
        payload: cartDetails
    }
    
}

export function removeItemFromCart(productId, cartProductId) {
    
    return async function(dispatch, getState) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.post('/removeCartProduct', {
            cart_product_ids: cartProductId
        }, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => console.log(err));

        if (response && response.data.code === 200) {

            dispatch(setProgressState(false));
            dispatch({
                type: actionTypes.REMOVE_ITEM_FROM_CART,
                payload: productId
            })     

            const {cart: {cartProducts}} = getState();       
            dispatch(showSnackbar({message: "Item Removed!", type: "error", open: true}))
            dispatch(calculateTotalPriceOfCartItems(cartProducts));
        } 
    }
}

export function decreaseQuantityFromCart(cartProductId, itemIndex, currentQuantity) {

    return async function(dispatch, getState) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.post('/updateCartProduct', {
            cart_product_id: cartProductId,
            quantity: currentQuantity - 1
        }, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => console.log(err));

        if (response && response.data.code === 200) {
            
            const { cart: {cartProducts} } = getState();
            const itemDetails = {...cartProducts[itemIndex]};

            if (itemDetails.productQuantity === 1) {

                dispatch({
                    type: actionTypes.REMOVE_ITEM_FROM_CART,
                    payload: itemDetails.productId
                })

            }
            else {

                itemDetails.productQuantity = itemDetails.productQuantity - 1;
                dispatch(updateItemInCart(itemDetails, itemIndex));

            }

            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: "Quantity Decreased!", type: "info", open: true}));
            const { cart: {cartProducts: products} } = getState();
            dispatch(calculateTotalPriceOfCartItems(products));
        }
    }
}

export function clearShoppingCart() {

    return async function (dispatch, getState) {

        dispatch(setProgressState(true));
        const {cart: {cartProducts}} = getState();
        const arrayOfCartProductIds = cartProducts.map(item => item.cartProductId);

        const response = await axiosInstance.post('/removeCartProduct', {
            cart_product_ids: arrayOfCartProductIds.join(",")
        }, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })

        if (response && response.data.code === 200) {
        
            dispatch({
                type: actionTypes.CLEAR_CART,
                payload: []
            })
            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: "Shopping Cart Cleared!", type: "error", open: true}));
            dispatch(updateTotalPriceOfCartItems(0));
        }
    } 
}

export function calculateTotalPriceOfCartItems(cartProducts) {

    return function(dispatch) {
        let totalPrice = 0;

        for (let product of cartProducts) {
            totalPrice += Number(product.productPrice) * product.productQuantity;
        }

        dispatch(updateTotalPriceOfCartItems(totalPrice));
    }

}

export function updateTotalPriceOfCartItems(price) {

    return {
        type: actionTypes.UPDATE_TOTAL_PRICE,
        payload: price.toFixed(2)
    }

}

export function getShippingDetails() {
    
    return async function (dispatch) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.get('/user-address', {
            headers: {
                Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => console.log(err))

        if (response && response.data.code === 200) {
            
            dispatch({

                type: actionTypes.SET_SHIPPING_DETAILS,
                payload: response.data.data

            })
            dispatch(setProgressState(false));

        }

    }
}

export function updateShippingDetail (details) {

    return async function (dispatch) {

        dispatch(setProgressState(true));
        const { shippingDetails } = details;
        
        if (shippingDetails.address_id) {

            const response = await axiosInstance.post('/update-user-address', shippingDetails, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            })
            .catch(err => console.log(err))

            if (response && response.data.code === 200) {

                dispatch({
                    type: actionTypes.UPDATE_SHIPPING_DETAIL,
                    payload: shippingDetails
                })
                dispatch(setProgressState(false));
                dispatch(showSnackbar({message: "Address updated successfully!", type: "success", open: true}));
            }
        }
    }
}
export function setShippingDetails (details) {

    return async function (dispatch) {

        dispatch(setProgressState(true));
        const { shippingDetails } = details;
        
        if (shippingDetails.address_id) {

            const response = await axiosInstance.post('/update-user-address', shippingDetails, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            })
            .catch(err => console.log(err))

            if (response && response.data.code === 200) {

                dispatch({
                    type: actionTypes.SET_CURRENT_SHIPPING_DETAILS,
                    payload: shippingDetails
                })
                dispatch(setProgressState(false));
            }
        }

        else {

            const response = await axiosInstance.post('/user-address', shippingDetails, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            })
            .catch(err => console.log(err))

            if (response && response.data.code === 200) {

                const currentShippingDetails = {...shippingDetails};
                const address_id = response.data.data.id;
                currentShippingDetails.address_id = address_id;

                dispatch({
                    type: actionTypes.SET_CURRENT_SHIPPING_DETAILS,
                    payload: currentShippingDetails
                })
                dispatch(setProgressState(false));
            }
        }
    }
}

export function setCurrentPaymentMethod(paymentMethodId) {

    return {
        type: actionTypes.SET_CURRENT_PAYMENT_METHOD,
        payload: paymentMethodId
    }
}

export function onPlaceOrderDetails(paymentMethodId, stripeToken) {

    return async function(dispatch, getState) {
        
        dispatch(setProgressState(true));
        const {cart: {cartProducts}, shipping: {currentShippingDetails}} = getState();
        const cart_id = cartProducts[0].cartId;
        const address_id = currentShippingDetails.address_id;
        const payment_method_id = Number(paymentMethodId);
        
        if (stripeToken) {
        
            dispatch(placeOrder({address_id, cart_id, payment_method_id, stripeToken})); 
            return;
        }

        dispatch(placeOrder({address_id, cart_id, payment_method_id})); 

    }
}

export function setOrderCheckoutDetails(details) {

    return async function(dispatch, getState) {

        dispatch(setProgressState(true));
        const {shippingDetails, paymentMethodId, isPaymentUsingStripe, stripeToken} = details;

        if (shippingDetails.address_id) {

            const response = await axiosInstance.post('/update-user-address', shippingDetails, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            })
            .catch(err => console.log(err))

            if (response && response.data.code === 200) {

                const address_id = response.data.data.id;
                const {cart: {cartProducts}} = getState();
                const cart_id = cartProducts[0].cartId;
                const payment_method_id = Number(paymentMethodId);

                if (isPaymentUsingStripe && stripeToken) {

                    dispatch(placeOrder({address_id, cart_id, payment_method_id, stripeToken})); 
                    
                }

                else {

                    dispatch(placeOrder({address_id, cart_id, payment_method_id})); 

                }

                dispatch({
                    type: actionTypes.SET_SHIPPING_DETAILS,
                    payload: []
                })
                return;           

            }
        }

        else {

            const response = await axiosInstance.post('/user-address', shippingDetails, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            })
            .catch(err => console.log(err))

            if (response && response.data.code === 200) {

                const address_id = response.data.data.id;
                const {cart: {cartProducts}} = getState();
                const cart_id = cartProducts[0].cartId;
                const payment_method_id = Number(paymentMethodId);

                if (isPaymentUsingStripe && stripeToken) {

                    dispatch(placeOrder({address_id, cart_id, payment_method_id, stripeToken})); 
                    
                }

                else {

                    dispatch(placeOrder({address_id, cart_id, payment_method_id})); 

                }

                dispatch({
                    type: actionTypes.SET_SHIPPING_DETAILS,
                    payload: []
                })
            }
        }

    }
}

export function placeOrder ({address_id, cart_id, payment_method_id, stripeToken}) {

    return async function (dispatch) {

        const orderResponse = await axiosInstance.post('/place-order', {address_id, cart_id, payment_method_id}, {
            headers: {
                Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => console.log(err));
        

        if (orderResponse && orderResponse.data.code === 200) {
    
            if (stripeToken) {

                dispatch(paymentUsingStripe(stripeToken, orderResponse.data.data.id))
                return;

            }

            dispatch({
                type: actionTypes.CLEAR_CART,
                payload: []
            })
            
            dispatch(setProgressState(false));
            dispatch(updateTotalPriceOfCartItems(0));
            dispatch(showSnackbar({message: "Order Placed Successfully!", type: "success", open: "true"}));                

        }

    }

}

export function paymentUsingStripe(token, orderId) {

    return async function (dispatch) {

        dispatch(setProgressState(true));
        
        const response = await axiosInstance.post('/stripe', {stripeToken: token, order_id: orderId}, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        }).catch(err => console.log(err))

        if (response && response.data.code === 200) {

            dispatch({
                type: actionTypes.CLEAR_CART,
                payload: []
            })
            
            dispatch(setProgressState(false));
            dispatch(updateTotalPriceOfCartItems(0));
            dispatch(showSnackbar({message: "Order Placed Successfully", type: "success", open: "true"}));                

        }

    }

}

export function removeShippingDetail(address_id) {


    return async function(dispatch) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.post('/delete-user-address', {address_id}, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        }).catch(err => console.log(err))
        
        if (response && response.data.code === 200) {

            dispatch({
                type: actionTypes.REMOVE_SHIPPING_DETAIL,
                payload: address_id
            });
            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: "Address removed successfully!", type: "success", open: true}));
        
        }
    }

}


export function onUserLogin(email, password) {

    return async function(dispatch) {
        
        dispatch(setLoadingState(true));
        const response = await axiosInstance.post('/login', {email, password})
            .catch(err => {
                if (err.response.data.message) {
                    dispatch(setLoadingState(false));
                    dispatch(setError({
                        errorFor: "login",
                        errorMessage: err.response.data.message
                    }))
                }
                if (err.response.data.code === 422) {
                    if (err.response.data.errors && err.response.data.errors.length > 0) {
                        dispatch(setLoadingState(false));
                        dispatch(setError({
                            errorFor: "login",
                            errorMessage: err.response.data.errors[0].password
                        }))
                    }
                }
            });

        if (response && response.data.code === 200) {

            localStorage.setItem("access_token_wappy", response.data.data.token);
            window.location.href = '/';
            dispatch(getUserInfo());
        }
    }

}

export function onUserRegister(name, username, email, password) {

    return async function(dispatch) {

        dispatch(setLoadingState(true));
        const response = await axiosInstance.post('/register', {name, username, email, password})
            .catch(err => {
                if (err.response.data.code === 422) {
                    if (err.response.data.errors) {
                        dispatch(setLoadingState(false));
                        dispatch(setError({errorFor: "register", errorMessage: err.response.data.errors.email[0]}))
                    }
                }
            })

        if (response && response.data.code === 200) {

            dispatch(setLoadingState(false));
            dispatch(showSnackbar({message: "Registration successful, Check your email for verification!", type: "success", open: true}));

        }

    }
}

export function getUserInfo() {
    
    return async function(dispatch) {

        dispatch(setLoadingState(true));

        const profileInfoResponse = await axiosInstance.get('/getProfileInfo', {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
                }
            })

            .catch(err => {

                if (err.response.data.code === 401) {

                    dispatch({
                        type: actionTypes.SET_PRODUCT_LIST,
                        payload: {}
                    })
                }
            });

        if (profileInfoResponse && profileInfoResponse.data.code === 200) {

            dispatch({
                type: actionTypes.SET_USER_INFO,
                payload: profileInfoResponse.data.data
            })

            dispatch(setProgressState(false));
            dispatch(setLoadingState(false));
        }
    }
}

export function getPaymentMethods() {

    return async function(dispatch) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.get('/getCardAndPayMethods', {
            headers: {
                Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => console.log(err))

        if (response && response.data.code === 200) {

            dispatch(setPaymentMethods(response.data.data.payment_methods));
            dispatch(setListOfCards(response.data.data?.user_cards));
            dispatch(setProgressState(false));

        }

    }

}

export function setListOfCards(userCards) {
    
    return {
        type: actionTypes.SET_LIST_OF_CARDS,
        payload: userCards
    }

}


export function removeCardDetail(card_id) {


    return async function(dispatch) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.delete(`/card/${card_id}`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        }).catch(err => console.log(err))
        
        if (response && response.data.code === 200) {

            dispatch({
                type: actionTypes.REMOVE_CARD_DETAIL,
                payload: card_id
            });
            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: "Card removed successfully!", type: "success", open: true}));
        
        }
    }

}


export function setPaymentMethods(paymentMethods) {

    return {
        type: actionTypes.SET_PAYMENT_METHODS,
        payload: paymentMethods
    }
    
}

export function setProductReviewList(productReviewList) {

    return {

        type: actionTypes.SET_PRODUCT_REVIEW_LIST,
        payload: productReviewList
    }

}

export function addReviewToProductReviewsList(productReview) {

    return {
        type: actionTypes.SET_PRODUCT_REVIEW,
        payload: productReview
    }

}

export function removeReviewFromProductReviewList(reviewId) {

    return {
        type: actionTypes.REMOVE_REVIEW_FROM_PRODUCT_REVIEW_LIST,
        payload: reviewId
    }

}

export function updateProductReview(reviewIndex, reviewDetails) {

    return {
        type: actionTypes.UPDATE_PRODUCT_REVIEW,
        payload: {
            reviewIndex,
            reviewDetails
        }
    }
}

export function addLikeToProductReview(reviewIndex) {

    return {
        type: actionTypes.ADD_LIKE_TO_PRODUCT_REVIEW,
        payload: reviewIndex
    }
}

export function addDislikeToProductReview(reviewIndex) {

    return {
        type: actionTypes.ADD_DISLIKE_TO_PRODUCT_REVIEW,
        payload: reviewIndex
    }
}

export function setInstagramPosts(instagramPosts) {

    return {

        type: actionTypes.SET_INSTAGRAM_POSTS,
        payload: instagramPosts
    }
}

export function getWishlist() {

    return async function (dispatch) {

        dispatch(setLoadingState(true));
        const response = await axiosInstance.get('/getWishlist', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        }).catch(err => console.log(err));

        if (response && response.data.code === 200) {

            dispatch({
                
                type: actionTypes.SET_WISHLIST_DETAILS,
                payload: response.data.data
            })
            dispatch(setLoadingState(false));

        }
    }
}

export function removeItemFromWishlist(wishlistId) {
    
    return async function(dispatch) {

        dispatch(setProgressState(true));
        const response = await axiosInstance.delete(`/deleteWishlist/${wishlistId}`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => console.log(err));

        if (response && response.data.code === 200) {

            dispatch(setProgressState(false));
            dispatch({
                type: actionTypes.REMOVE_ITEM_FROM_WISHLIST,
                payload: wishlistId
            })     

            dispatch(showSnackbar({message: "Item Removed!", type: "error", open: true}))
        } 
    }
}

export function clearWishlist() {

    return async function (dispatch) {

        dispatch(setProgressState(true));
        
        const response = await axiosInstance.get('/emptyWishlist', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })

        if (response && response.data.code === 200) {
        
            dispatch({
                type: actionTypes.CLEAR_WISHLIST,
                payload: []
            })
            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: "Wishlist Cleared!", type: "error", open: true}));
        }
    } 
}

export function addItemToWishlist(productId) {

    return async function (dispatch) {

        dispatch(setProgressState(true));
        
        const formData = new FormData();
        formData.append(`product_id[${0}]`, productId);

        const response = await axiosInstance.post('/addWishlist', formData, {
            headers: {
                Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
            }
        }).catch(err => {

            if (err.response.data && err.response.data.code === 422) {

                dispatch(showSnackbar({message: err.response.data.message, type: "error", open: true}));                
                dispatch(setProgressState(false));
            }
        });

        if (response && response.data.code === 200) {

            dispatch({
                type: actionTypes.SET_ADD_TO_WISHLIST,
                payload: response.data.data[0]
            })
            dispatch(showSnackbar({message: "Added to wishlist!", type: "success", open: true}));                
            dispatch(setProgressState(false));
        }
    }
}

export function getConfigData() {

    return async function (dispatch) {

        const response = await axiosInstance.get('/config').catch(err => console.log(err))

        if (response && response.data.code === 200) {
            
            dispatch({

                type: actionTypes.SET_CONFIG_DATA,
                payload: response.data.data

            })
            
        }
    }
}

export function logoutUser() {

    return async function(dispatch) {

        const response = await axiosInstance.post('/logout', {}, {
            headers: {
                Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
            }
        }).catch(err => console.log(err))

        if (response && response.data.message === "You have been successfully logged out!") {

            window.location.href = "/login";
            localStorage.clear();
            dispatch({
                type: actionTypes.LOGOUT_USER
            });
        }
    }
}

export function showSnackbar(snackbarDetails) {

    return {
        type: actionTypes.SHOW_SNACKBAR,
        payload: snackbarDetails
    }

}

export function resetSnackbar() {
    
    return {
        type: actionTypes.RESET_SNACKBAR
    }

}

export function getHomeData() {

    return async function (dispatch) {

        dispatch(setLoadingState(true));
        const response = await axiosInstance.get('/getHomeData', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            }
        })
        .catch(err => {

            console.log(err);

        })

        if (response && response.data.code === 200) {

            if (response.data.data) {

                if (response.data.data.banner.length > 0) {

                    const banners = response.data.data.banner.map(({
                        image_url
                    }) => image_url);
                    const randomIndex = Math.floor(Math.random() * banners.length);
                    dispatch(setTrendingImage(banners[randomIndex]));
                    dispatch(setBannerImages(response.data.data && banners));

                }

                if (response.data.data.panel.length > 0) {

                    dispatch(setSeasonTrends(response.data.data && response.data.data.panel));

                }

                if (response.data.data.new_arr.length > 0) {

                    dispatch(setNewArrivalsList(response.data.data && response.data.data.new_arr));

                }

                if (response.data.data.adminCategory.length > 0) {

                    const categories = [];

                    const adminCategory = response.data.data.adminCategory;

                    for (let data of adminCategory) {

                        let categoryObject = {
                            sub_categories: []
                        };

                        const categoryIndex = categories.findIndex(category => category.title === data.category.parent_category_name);

                        if (data.category.title && data.category.parent_category_name) {

                            categoryObject.title = data.category.parent_category_name;
                            categoryObject.id = data.category.parent_id;

                            if (data.category.sub_categories.length > 0) {

                                let subCategoryObject = {
                                    sub_categories: []
                                };

                                for (let subCategory of data.category.sub_categories) {

                                    subCategoryObject.title = data.category.title;
                                    subCategoryObject.id = subCategory.parent_id;

                                    if (subCategory.title && subCategory.parent_category_name === subCategoryObject.title) {

                                        subCategoryObject.sub_categories.push({
                                            id: subCategory.id,
                                            title: subCategory.title,
                                            sub_categories: []
                                        });
                                    }
                                }

                                if (categoryIndex !== -1) {

                                    categories[categoryIndex].sub_categories.push(subCategoryObject)
                                } else {

                                    categoryObject.sub_categories.push(subCategoryObject);
                                }
                            } else {

                                if (categoryIndex !== -1) {

                                    categories[categoryIndex].sub_categories.push({
                                        id: data.category.id,
                                        title: data.category.title,
                                        sub_categories: []
                                    })
                                }
                            }
                        } else if (data.category.title && !data.category.parent_category_name) {

                            categoryObject.title = data.category.title;
                            categoryObject.id = data.category.id;

                            if (data.category.sub_categories.length > 0) {

                                categoryObject.sub_categories = data.category.sub_categories;
                                categoryObject.sub_categories = categoryObject.sub_categories.map(category => {
                                    return {
                                        ...category,
                                        sub_categories: []
                                    }
                                })
                            }
                        }

                        if (categoryIndex === -1) {

                            categories.push(categoryObject);
                        }
                    }

                    dispatch(setCategories(categories));
                    dispatch(setProductRangeList(response.data.data.adminCategory));
                }
            }
            dispatch(setLoadingState(false));
        }
    }
}