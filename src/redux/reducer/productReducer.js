import * as actionTypes from '../action/action-type/action-type';
import BackgroundImg from '../../assets/images/introduction/IntroductionFive/1.png';

const initialState = {
    productList: [],
    isLoading: false,
    isProgressing: false,
    error: { errorFor: "" , errorMessage: "" },
    activePage: 1,
    lastPage: 0,
    pageLimit: 12,
    paginationLinks: [],
    newArrivalsList: [],
    bannerImages: [],
    trendingImage: BackgroundImg,
    seasonTrends: [],
    categories: [],
    activeCategory: "",
    productReviewList: [],
    configData: [],
    productRangeList: [],
    instagramPosts: []
}

export const productReducer = (state=initialState, action) => {
    
    switch (action.type) {
        
        case actionTypes.SET_PRODUCT_LIST:
            return { ...state, productList: action.payload }
        
        case actionTypes.SET_TRENDING_IMAGE:
            return { ...state, trendingImage: action.payload }
        
        case actionTypes.SET_BANNER_IMAGES:
            return { ...state, bannerImages: action.payload }
    
        case actionTypes.SET_SEASON_TRENDS:
            return { ...state, seasonTrends: action.payload }
        
        case actionTypes.SET_NEW_ARRIVALS_LIST:
            return { ...state, newArrivalsList: action.payload }
            
        case actionTypes.SET_LOADING_STATE: 
            return {...state, isLoading: action.payload }

        case actionTypes.SET_PROGRESS_STATE: 
            return {...state, isProgressing: action.payload }

        case actionTypes.SET_PAGINATION_LINKS: 
            return { ...state, paginationLinks: action.payload }

        case actionTypes.SET_ERROR: 
            return {...state, error: action.payload }

        case actionTypes.SET_CATEGORIES: 
            return {...state, categories: action.payload }

        case actionTypes.SET_ACTIVE_CATEGORY:
            return {...state, activeCategory: action.payload }

        case actionTypes.SET_PRODUCT_RANGE_LIST:
            return {...state, productRangeList: action.payload }
            
        case actionTypes.SET_PRODUCT_REVIEW_LIST: 
            return {...state, productReviewList: action.payload}

        case actionTypes.SET_INSTAGRAM_POSTS:
            return {...state, instagramPosts: action.payload }
            
        case actionTypes.REMOVE_REVIEW_FROM_PRODUCT_REVIEW_LIST: 

            const copyOfProductReviewList = [...state.productReviewList].filter(review => review.id !== action.payload);
            
            return {...state, productReviewList: copyOfProductReviewList}

        case actionTypes.SET_PRODUCT_REVIEW: 
        
            const updatedProductReviewList = [...state.productReviewList];

            updatedProductReviewList.unshift(action.payload);

            return {...state, productReviewList: updatedProductReviewList};

        case actionTypes.UPDATE_PRODUCT_REVIEW:

            const updateProductReviewList = [...state.productReviewList];
            
            updateProductReviewList[action.payload.reviewIndex] = action.payload.reviewDetails;
            
            return {...state, productReviewList: updateProductReviewList}

        case actionTypes.ADD_LIKE_TO_PRODUCT_REVIEW:

            const productReviewListCopy = [...state.productReviewList];

            if (productReviewListCopy[action.payload].likes === null) {
            
                productReviewListCopy[action.payload].likes = (1).toString();
                productReviewListCopy[action.payload].has_logged_user_liked_review = 1;
            }

            else {

                productReviewListCopy[action.payload].likes = (Number(productReviewListCopy[action.payload].likes) + 1).toString();
                productReviewListCopy[action.payload].has_logged_user_liked_review = 1;

            }

            if (productReviewListCopy[action.payload].dislikes) {

                productReviewListCopy[action.payload].dislikes = (Number(productReviewListCopy[action.payload].dislikes) - 1).toString();

                if (productReviewListCopy[action.payload].has_logged_user_disliked_review) {

                    productReviewListCopy[action.payload].has_logged_user_disliked_review = 0;
                }
            }

            return {...state, productReviewList: productReviewListCopy}

        case actionTypes.ADD_DISLIKE_TO_PRODUCT_REVIEW:

            const productReviewListCopy2 = [...state.productReviewList];
            
            if (productReviewListCopy2[action.payload].dislikes === null) {
            
                productReviewListCopy2[action.payload].dislikes = (1).toString();
                productReviewListCopy2[action.payload].has_logged_user_disliked_review = 1;

            }

            else {

                productReviewListCopy2[action.payload].dislikes = (Number(productReviewListCopy2[action.payload].dislikes) + 1).toString();
                productReviewListCopy2[action.payload].has_logged_user_disliked_review = 1;

            }

            if (productReviewListCopy2[action.payload].likes) {

                productReviewListCopy2[action.payload].likes = (Number(productReviewListCopy2[action.payload].likes) - 1).toString();

                if (productReviewListCopy2[action.payload].has_logged_user_liked_review) {

                    productReviewListCopy2[action.payload].has_logged_user_liked_review = 0;
                }
            }

            return {...state, productReviewList: productReviewListCopy2};

        case actionTypes.SET_CONFIG_DATA:
            return {...state, configData: action.payload}
        
        // case actionTypes.LOGOUT_USER:
        //     return initialState

        default:
            return state;

    }

}