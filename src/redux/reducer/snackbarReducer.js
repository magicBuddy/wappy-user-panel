import * as actionTypes from '../action/action-type/action-type';

const initialState = {

    snackbarDetails: {
        type: "",
        message: "",
        open: false
    }

}

export const snackbarReducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.SHOW_SNACKBAR:
            return {...state, snackbarDetails: action.payload}

        case actionTypes.RESET_SNACKBAR:
            return initialState;

        case actionTypes.LOGOUT_USER:
            return initialState

        default:
            return state;

    }

}