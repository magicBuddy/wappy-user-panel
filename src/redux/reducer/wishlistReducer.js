import * as actionTypes from '../action/action-type/action-type';

const initialState = {
    wishlistProducts : []
}

export const wishlistReducer = (state=initialState, action) => {
    
    switch (action.type) {
        
        case actionTypes.SET_ADD_TO_WISHLIST: 

            const wishlist = [action.payload, ...state.wishlistProducts];

            return {...state, wishlistProducts: wishlist}

        case actionTypes.SET_WISHLIST_DETAILS: 
            return { ...state, wishlistProducts: action.payload}

        case actionTypes.REMOVE_ITEM_FROM_WISHLIST: 
            return {...state, wishlistProducts: state.wishlistProducts.filter(wishlist => wishlist.id !== action.payload)}
            
        case actionTypes.CLEAR_WISHLIST: 
            return {...state, wishlistProducts: []}

        case actionTypes.LOGOUT_USER:
            return initialState

        default:
            return state;
    }

}