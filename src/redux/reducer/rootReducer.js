import { combineReducers } from "redux";
import { cartReducer } from "./cartReducer";
import { productReducer } from "./productReducer";
import { snackbarReducer } from "./snackbarReducer";
import { shippingReducer } from "./shippingReducer";
import { orderReducer } from "./orderReducer";
import { authReducer } from "./authReducer";
import { wishlistReducer } from "./wishlistReducer";

export const rootReducer = combineReducers({
    products: productReducer,
    cart: cartReducer,
    snackbar: snackbarReducer,
    shipping: shippingReducer,
    order: orderReducer,
    auth: authReducer,
    wishlist: wishlistReducer
})