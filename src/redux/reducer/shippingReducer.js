import * as actionTypes from '../action/action-type/action-type';

const initialState = {

    shippingDetails: [],
    listOfCards: [],
    currentShippingDetails : null

}

export const shippingReducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.SET_SHIPPING_DETAILS: 
            return {...state, shippingDetails: action.payload}
        
        case actionTypes.SET_CURRENT_SHIPPING_DETAILS: 
            return {...state, currentShippingDetails: action.payload}

        case actionTypes.REMOVE_SHIPPING_DETAIL:
            
            const updatedShippingDetails = [...state.shippingDetails]
                                            .filter(shippingDetail => shippingDetail.id !== action.payload);

            return {...state, shippingDetails: updatedShippingDetails}

        case actionTypes.UPDATE_SHIPPING_DETAIL: 
           
            const updateShippingDetails = [...state.shippingDetails]
            
            const indexOfShippingDetailToUpdate = updateShippingDetails
                                                    .findIndex(shippingDetail => shippingDetail.id === action.payload.address_id);
            
            updateShippingDetails[indexOfShippingDetailToUpdate].address = action.payload.address;
            updateShippingDetails[indexOfShippingDetailToUpdate].phone = action.payload.phone;
            updateShippingDetails[indexOfShippingDetailToUpdate].post_code = action.payload.post_code;

            return {...state, shippingDetails: updateShippingDetails}

        case actionTypes.REMOVE_CARD_DETAIL:
            
            const updatedCardDetails = [...state.listOfCards]
                                            .filter(cardDetail => cardDetail.id !== action.payload);

            return {...state, listOfCards: updatedCardDetails}

        case actionTypes.CLEAR_CART: 
            return {...state, currentCardDetails: null}

        case actionTypes.SET_LIST_OF_CARDS: 
            return {...state, listOfCards: action.payload}
            
        case actionTypes.LOGOUT_USER:
            return initialState

        default:
            return state;

    }

}