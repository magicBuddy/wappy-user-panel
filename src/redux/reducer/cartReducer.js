import * as actionTypes from '../action/action-type/action-type';

const initialState = {
    cartProducts : [],
    isCartDetailsFetched : false,
    totalCartPrice: "0"
}

export const cartReducer = (state=initialState, action) => {
    
    switch (action.type) {
        
        case actionTypes.UPDATE_CART:
            return { ...state, cartProducts: [...state.cartProducts, action.payload] }

        case actionTypes.UPDATE_ITEM_IN_CART:

            const products = [...state.cartProducts];
            products[action.payload.itemIndex] = action.payload.itemDetails;
            
            return {...state, cartProducts: products }

        case actionTypes.SET_CART_DETAILS: 
            return { ...state, cartProducts: action.payload.filter(product => product.productQuantity !== 0), isCartDetailsFetched: true }

        case actionTypes.REMOVE_ITEM_FROM_CART: 
            return {...state, cartProducts: state.cartProducts.filter(product => product.productId !== action.payload)}
            
        case actionTypes.CLEAR_CART: 
            return {...state, cartProducts: action.payload}

        case actionTypes.UPDATE_TOTAL_PRICE:
            return {...state, totalCartPrice: action.payload}

        case actionTypes.LOGOUT_USER:
            return initialState

        default:
            return state;

    }

}