import * as actionTypes from '../action/action-type/action-type';

const initialState = {

    userDetails: {},
    isUserLoggedOut: true
    
}

export const authReducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.SET_USER_INFO: 
            return {...state, userDetails: action.payload, isUserLoggedOut: false}
                
        case actionTypes.LOGOUT_USER:
            return initialState

        default:
            return state;

    }

}