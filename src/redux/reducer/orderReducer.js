import * as actionTypes from '../action/action-type/action-type';

const initialState = {

    paymentMethods: [],
    currentPaymentMethod: null
    
}

export const orderReducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.SET_PAYMENT_METHODS:
            return {...state, paymentMethods: action.payload}

        case actionTypes.SET_CURRENT_PAYMENT_METHOD: 
            return {...state, currentPaymentMethod: action.payload}
        
        case actionTypes.CLEAR_CART: 
            return {...state, currentPaymentMethod: null}
            
        case actionTypes.LOGOUT_USER: 
            return initialState
            
        default:
            return state;

    }

}