import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import axiosInstance from '../../axios/axios-instance';
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';

function EmailVerification() {

    const code = new URLSearchParams(window.location.search).get('code');
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [messageForSnackbar, setMessageForSnackbar] = useState("");
    const [snackbarType, setSnackbarType] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const history = useHistory();

    useEffect(() => {

        if (code) {

            setIsLoading(true);
            axiosInstance.post(`/verifyEmail`, { code })
                .then(() => {

                    setIsLoading(false);
                    setOpenSnackbar(openSnackbar => !openSnackbar);
                    setSnackbarType("success");
                    setMessageForSnackbar("Email address verified!");
                    setTimeout(() => {
                        history.push('/login');
                    }, 1500)
                })
                .catch(err => {

                    if (err.response && err.response.data.errors) {

                        setIsLoading(false);
                        setOpenSnackbar(openSnackbar => !openSnackbar);
                        setSnackbarType("error");
                        setMessageForSnackbar(err.response.data.errors);
                        setTimeout(() => {
                            history.push('/login');
                        }, 1500)
                    }
                })
        }

    }, []);

    const handleSnackbarClose = () => {
        setOpenSnackbar(false);
        setSnackbarType("");
        setMessageForSnackbar("");
    }

    return (

        <>             
            <LoadingSpinner isLoading={isLoading} />

            <SnackbarToast
                handleSnackbarClose={handleSnackbarClose}
                openSnackbar={openSnackbar}
                snackbarType={snackbarType}
                message={messageForSnackbar}
            /> 
        </>
    )
}

export default EmailVerification
