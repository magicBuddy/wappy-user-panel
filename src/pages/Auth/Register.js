import React, { useEffect, useState } from 'react';
import Logo from '../../assets/images/logo.png';
import LoginImg from '../../assets/images/login.svg';
import {Link} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Backdrop from '../../utils/Backdrop/Backdrop';
import { onUserRegister, resetSnackbar, setError, showSnackbar } from '../../redux/action/action';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';

function Register({history}) {

    const [name, setName] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const {userDetails} = useSelector(state => state.auth);
    const {isLoading, error} = useSelector(state => state.products);
    const dispatch = useDispatch();
    
    useEffect(() => {

        if (userDetails && Object.keys(userDetails).length > 0) {

            history.push('/');

        }

    }, [])

    const onSubmitRegisterForm = (e) => {
        
        e.preventDefault();

        if (
                email.trim().length === 0 ||
                password.trim().length === 0 ||
                name.trim().length === 0 ||
                username.trim().length === 0
           ) {
            
            dispatch(showSnackbar({message: "Any field cannot be empty!", type: "error", open: true}))
            return;

        }

        const regexForEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!regexForEmail.test(String(email).toLowerCase())) {

            dispatch(showSnackbar({message: "Enter valid Email Address!", type: "error", open: true}))
            return;
            
        }

        dispatch(onUserRegister(name, username, email, password));

    }

    const handleSnackbarClose = () => {

        dispatch( setError({errorFor: "", errorMessage: ""}) )

    }

    if (isLoading) return <Backdrop isLoading={isLoading} />

    return (
        <div className="container-fluid">
            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }

            {error && error.errorMessage && error.errorFor === "register" ?                
                <SnackbarToast 
                
                    message = {error.errorMessage} 
                    snackbarType = "error" 
                    openSnackbar = {error.errorMessage && error.errorFor} 
                    handleSnackbarClose = {handleSnackbarClose}

                /> 
                : null 
            }
            <div className="row">
                <div className="col-sm-6 login-section-wrapper" style={{height: "99vh"}}>
                    
                    <div className="brand-wrapper">
                        <img src={Logo} alt="logo" className="logo"/>
                    </div>
                    
                    <div className="login-wrapper my-auto">

                        <h1 className="login-title">Register</h1>
                        <form onSubmit={onSubmitRegisterForm}>
                        
                        <div className="form-group mb-4">
                            <label className="d-block" for="name">Full Name</label>
                            <input onChange={(e) => setName(e.target.value)} value={name} type="name" name="name" id="name" className="w-100 form-control" placeholder="John Doe"/>
                        </div>

                        <div className="form-group mb-4">
                            <label className="d-block" for="name">Username</label>
                            <input onChange={(e) => setUsername(e.target.value)} value={username} type="name" name="name" id="name" className="w-100 form-control" placeholder="johndoe12"/>
                        </div>

                        <div className="form-group mb-4">
                            <label className="d-block" for="email">Email</label>
                            <input onChange={(e) => setEmail(e.target.value)} value={email} type="email" name="email" id="email" className="w-100 form-control" placeholder="email@example.com"/>
                        </div>

                        <div className="form-group mb-4">
                            <label className="d-block" for="password">Password</label>
                            <input onChange={(e) => setPassword(e.target.value)} value={password} type="password" name="password" id="password" className="w-100 form-control" placeholder="******"/>
                        </div>

                        <input name="register" id="register" className="btn btn-block login-btn w-100" type="submit" value="Register"/>
                        </form>
                        <p className="login-wrapper-footer-text">Already have an account? <Link to="/login" className="text-reset">Login here</Link></p>

                    </div>
                    
                </div>
                <div className="col-sm-6 px-0 d-none d-sm-block"  style={{height: "99vh"}}>
                    <img src={LoginImg} alt="register image" className="login-img"/>
                </div>
            </div>
        </div>
    )
}

export default Register
