import React, { useEffect, useState } from 'react';
import Logo from '../../assets/images/logo.png';
import LoginImg from '../../assets/images/login.svg';
import {Link} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';
import { resetSnackbar, setLoadingState, setProgressState, showSnackbar } from '../../redux/action/action';
import axiosInstance from '../../axios/axios-instance';
import Backdrop from '../../utils/Backdrop/Backdrop';

function ForgotPassword({history}) {

    const [email, setEmail] = useState("");
    const dispatch = useDispatch();
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const {isProgressing} = useSelector(state => state.products);

    useEffect(() => {

        if (localStorage.getItem("access_token_wappy")) {

            history.push('/');

        }

    }, [])

    const onSubmit = async (e) => {
        
        e.preventDefault();

        if (email.trim().length === 0) {
            
            dispatch(showSnackbar({message: "Email is required!", type: "error", open: true}))
            return;

        }

        const regexForEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!regexForEmail.test(String(email).toLowerCase())) {

            dispatch(showSnackbar({message: "Enter valid Email Address!", type: "error", open: true}))
            return;
            
        }

        dispatch(setProgressState(true));

        const response = await axiosInstance.post('/forgetPassword', {email})
            .catch(err => {

                if (err && err.response.data.code === 422) {

                    dispatch(setProgressState(false));
                    dispatch(showSnackbar({message: err.response.data.message, type: "error", open: true}));
                }

                if (err && err.response.data.code === 500) {

                    dispatch(setProgressState(false));
                    dispatch(showSnackbar({message: err.response.data.message, type: "error", open: true}));
                }
            })
    
        if (response && response.data.code === 200) {

            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: 'Check your email for OTP verification!', type: "success", open: true}));
            setTimeout(() => history.push('/change-password', {email}), 2000);
        }
    }

    if (isProgressing) return <Backdrop isLoading={isProgressing} />


    return (
        <div className="container-fluid">

            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }

            <div className="row">
                <div className="col-sm-6 login-section-wrapper" style={{height: "99vh"}}>
                    <div className="brand-wrapper">
                        <img src={Logo} alt="logo" className="logo"/>
                    </div>
                    <div className="login-wrapper my-auto">
                        <h1 className="login-title">Forgot Password?</h1>
                        <form onSubmit={onSubmit}>
                        <div className="form-group mb-4">
                            <label className="d-block" for="email">Email</label>
                            <input onChange={(e) => setEmail(e.target.value)} value={email} type="email" name="email" id="email" className="form-control w-100" placeholder="email@example.com"/>
                        </div>
                        <input className="w-100 btn btn-block login-btn" type="submit" value="Submit"/>
                        </form>
                        <p className="login-wrapper-footer-text">Don't have an account? <Link to="/register" className="text-reset">Register here</Link></p>
                    </div>
                </div>
                <div className="col-sm-6 px-0 d-none d-sm-block"  style={{height: "99vh"}}>
                    <img src={LoginImg} alt="login image" className="login-img"/>
                </div>
            </div>
        </div>
    )
}

export default ForgotPassword
