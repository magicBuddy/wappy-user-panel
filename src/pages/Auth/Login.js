import React, { useEffect, useState } from 'react';
import Logo from '../../assets/images/logo.png';
import LoginImg from '../../assets/images/login.svg';
import {Link} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';
import { onUserLogin, resetSnackbar, setError, showSnackbar } from '../../redux/action/action';
import Backdrop from '../../utils/Backdrop/Backdrop';

function Login({history}) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch();
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const {userDetails, isUserLoggedOut} = useSelector(state => state.auth);
    const {isLoading, error} = useSelector(state => state.products);

    useEffect(() => {

        if (localStorage.getItem("access_token_wappy")) {

            history.push('/');

        }

    }, [])

    const onSubmitLoginForm = (e) => {
        
        e.preventDefault();

        if (email.trim().length === 0) {
            
            dispatch(showSnackbar({message: "Email is required!", type: "error", open: true}))
            return;

        }

        if (password.trim().length === 0) {

            dispatch(showSnackbar({message: "Password is required!", type: "error", open: true}))
            return;

        }

        const regexForEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!regexForEmail.test(String(email).toLowerCase())) {

            dispatch(showSnackbar({message: "Enter valid Email Address!", type: "error", open: true}))
            return;
            
        }

        dispatch(onUserLogin(email,password));

    }

    const handleSnackbarClose = () => {

        dispatch( setError({errorFor: "", errorMessage: ""}) )

    }

    if (isLoading && !isUserLoggedOut) return <Backdrop isLoading={isLoading} />


    return (
        <div className="container-fluid">

            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }

            {error && error.errorMessage && error.errorFor === "login" ?                
                <SnackbarToast 
                
                    message = {error.errorMessage} 
                    snackbarType = "error" 
                    openSnackbar = {error.errorMessage && error.errorFor} 
                    handleSnackbarClose = {handleSnackbarClose}

                /> 
                : null 
            }
            
            <div className="row">
                <div className="col-sm-6 login-section-wrapper" style={{height: "99vh"}}>
                    <div className="brand-wrapper">
                        <img src={Logo} alt="logo" className="logo"/>
                    </div>
                    <div className="login-wrapper my-auto">
                        <h1 className="login-title">Log In</h1>
                        <form onSubmit={onSubmitLoginForm}>
                        <div className="form-group mb-4">
                            <label className="d-block" for="email">Email</label>
                            <input onChange={(e) => setEmail(e.target.value)} value={email} type="email" name="email" id="email" className="w-100 form-control" placeholder="email@example.com"/>
                        </div>
                        <div className="form-group mb-4">
                            <label className="d-block" for="password">Password</label>
                            <input onChange={(e) => setPassword(e.target.value)} value={password} type="password" name="password" id="password" className="w-100 form-control" placeholder="******"/>
                        </div>
                        <input name="login" id="login" className="btn btn-block login-btn w-100" type="submit" value="Login"/>
                        </form>
                        <Link to="/forgot-password" className="forgot-password-link">Forgot password?</Link>
                        <p className="login-wrapper-footer-text">Don't have an account? <Link to="/register" className="text-reset">Register here</Link></p>
                    </div>
                </div>
                <div className="col-sm-6 px-0 d-none d-sm-block"  style={{height: "99vh"}}>
                    <img src={LoginImg} alt="login image" className="login-img"/>
                </div>
            </div>
        </div>
    )
}

export default Login
