import React, { useEffect, useState } from 'react';
import Logo from '../../assets/images/logo.png';
import LoginImg from '../../assets/images/login.svg';
import {Link} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';
import { resetSnackbar, setLoadingState, setProgressState, showSnackbar } from '../../redux/action/action';
import axiosInstance from '../../axios/axios-instance';
import Backdrop from '../../utils/Backdrop/Backdrop';

function ChangePassword({history, location}) {

    const [email, setEmail] = useState("");
    const [otp, setOtp] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
	const [isTimerOn, setIsTimerOn] = useState(false);
	const [timer, setTimer] = useState(60);
	const [countdownTimer, setCountdownTimer] = useState(null);

    const dispatch = useDispatch();
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const {isProgressing} = useSelector(state => state.products);

    useEffect(() => {

        if (localStorage.getItem("access_token_wappy")) {

            history.push('/');

        }

        if (!location.state || !location.state.email) {

            history.push('/forgot-password');
        }

        else {
            
            setEmail(location.state.email);            
            setIsTimerOn(true);
            setCountdownTimer(setInterval(() => {
                setTimer(timer => timer - 1);
            }, 1000));
        }

    }, [])

    useEffect(() => {

        if (timer === 0) {
            clearInterval(countdownTimer);
            setIsTimerOn(false);
            setTimer(60);
        }

    }, [countdownTimer, timer]);

    const onSubmit = async (e) => {
        
        e.preventDefault();

        if (email.trim().length === 0) {
            
            dispatch(showSnackbar({message: "Email is required!", type: "error", open: true}))
            return;

        }

        if (password.trim().length === 0) {

            dispatch(showSnackbar({message: "Password is required!", type: "error", open: true}))
            return;

        }

        if (confirmPassword.trim().length === 0) {

            dispatch(showSnackbar({message: "Confirm Password is required!", type: "error", open: true}))
            return;

        }

        if (password.trim() !== confirmPassword.trim()) {

            dispatch(showSnackbar({message: "Passwords does not match!", type: "error", open: true}))
            return;

        }

        if (otp.trim().length === 0) {

            dispatch(showSnackbar({message: "OTP is required!", type: "error", open: true}))
            return;

        }

        const regexForEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!regexForEmail.test(String(email).toLowerCase())) {

            dispatch(showSnackbar({message: "Enter valid Email Address!", type: "error", open: true}))
            return;
            
        }

        dispatch(setProgressState(true));

        const response = await axiosInstance.post('/changePassword', {email, password, otp})
            .catch(err => {

                if (err && err.response.data.code === 422) {

                    dispatch(setProgressState(false));
                    dispatch(showSnackbar({
                        message: err.response.data.errors?.email ? err.response.data.errors.email[0] : err.response.data.errors.otp[0],
                        type: "error",
                        open: true
                    }));
                }

                if (err && err.response.data.code === 500) {

                    dispatch(setProgressState(false));
                    dispatch(showSnackbar({message: err.response.data.message, type: "error", open: true}));
                }
            })
    
        if (response && response.data.code === 200) {

            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: response.data.message, type: "success", open: true}));
            setEmail("");
            setPassword("");
            setConfirmPassword("");
            setOtp("");
            clearInterval(countdownTimer);
            setIsTimerOn(false);
            setTimer(60);
            setTimeout(() => history.push('/login'), 2000);
        }
    }

    const onResendOtp = async (e) => {

        e.preventDefault();

        if (email.trim().length === 0) {
            
            dispatch(showSnackbar({message: "Email is required!", type: "error", open: true}))
            return;

        }

        const regexForEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!regexForEmail.test(String(email).toLowerCase())) {

            dispatch(showSnackbar({message: "Enter valid Email Address!", type: "error", open: true}))
            return;
            
        }

        dispatch(setProgressState(true));

        const response = await axiosInstance.post('/forgetPassword', {email})
            .catch(err => {

                if (err && err.response.data.code === 422) {

                    dispatch(setProgressState(false));
                    dispatch(showSnackbar({message: err.response.data.message, type: "error", open: true}));
                }

                if (err && err.response.data.code === 500) {

                    dispatch(setProgressState(false));
                    dispatch(showSnackbar({message: err.response.data.message, type: "error", open: true}));
                }
            })
    
        if (response && response.data.code === 200) {

            setPassword("");
            setConfirmPassword("");
            setOtp("");
            dispatch(setProgressState(false));
            dispatch(showSnackbar({message: 'Check your email for OTP verification!', type: "success", open: true}));
            setIsTimerOn(true);
            setCountdownTimer(setInterval(() => {
                setTimer(timer => timer - 1);
            }, 1000));
        }
    }

    if (isProgressing) return <Backdrop isLoading={isProgressing} />

    return (
        <div className="container-fluid">

            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }

            <div className="row">
                <div className="col-sm-6 login-section-wrapper" style={{height: "99vh"}}>
                    <div className="brand-wrapper">
                        <img src={Logo} alt="logo" className="logo"/>
                    </div>
                    <div className="login-wrapper my-auto">
                        <h1 className="login-title">Change Password?</h1>
                        <form onSubmit={onSubmit}>
                        <div className="form-group mb-4">
                            <label className="d-block" for="otp">OTP</label>
                            <input onChange={(e) => setOtp(e.target.value)} value={otp} type="text" name="otp" id="otp" className="form-control w-100" placeholder="12345"/>
                        </div>
                        <div className="form-group mb-4">
                            <label className="d-block" for="password">Password</label>
                            <input onChange={(e) => setPassword(e.target.value)} value={password} type="password" name="password" id="password" className="form-control w-100" placeholder="******"/>
                        </div>
                        <div className="form-group mb-4">
                            <label className="d-block" for="password">Confirm Password</label>
                            <input onChange={(e) => setConfirmPassword(e.target.value)} value={confirmPassword} type="password" name="confirmPassword" id="confirmPassword" className="form-control w-100" placeholder="******"/>
                        </div>

                        { isTimerOn && <span className="mb-4 d-block resend">You can resend OTP in {timer} {timer === 1 ? "second" : "seconds"}. </span> }

                        <div className="d-flex flex-wrap flex-column">
                            <input className="btn btn-block login-btn" type="submit" value="Submit"/>
                            { !isTimerOn && <button onClick={onResendOtp} className="btn btn-block login-btn">Resend OTP</button> }
                        </div>

                        </form>
                        <p className="login-wrapper-footer-text">Don't have an account? <Link to="/register" className="text-reset">Register here</Link></p>
                    </div>
                </div>
                <div className="col-sm-6 px-0 d-none d-sm-block"  style={{height: "99vh"}}>
                    <img src={LoginImg} alt="login image" className="login-img"/>
                </div>
            </div>
        </div>
    )
}

export default ChangePassword
