import React, { useEffect, useState } from 'react'
import AboutUs from '../../components/Home/AboutUs/AboutUs'
import Banner from '../../components/Home/Banner/Banner'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
import NewArrivals from '../../components/Home/NewArrivals/NewArrivals'
import SeasonTrends from '../../components/Home/SeasonTrends/SeasonTrends'
import axiosInstance from '../../axios/axios-instance'
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner'
import BackgroundImg from '../../assets/images/introduction/IntroductionFive/1.png';
import { useDispatch, useSelector } from 'react-redux'
import { getCartDetails, getCategories, getNewArrivalsList, getProductList, resetSnackbar, setBannerImages, setCategories, setInstagramPosts, setNewArrivalsList, setProductRangeList, setSeasonTrends, setTrendingImage } from '../../redux/action/action'
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast'
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader'
import RelatedProducts from '../../components/ProductDetails/RelatedProducts/RelatedProducts'
import InstagramPosts from '../../components/Home/InstagramPosts/InstagramPosts'

function Home({history}) {
    
    const [isBannerLoading, setIsBannerLoading] = useState(false);
    const {
        bannerImages,
        trendingImage, 
        newArrivalsList,
        seasonTrends,
        productRangeList,
        instagramPosts,
        isLoading,
        pageLimit,
        categories
    } = useSelector(state => state.products);
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const {isUserLoggedOut} = useSelector(state => state.auth);
    const dispatch = useDispatch();

    useEffect(() => {

        if (instagramPosts.length === 0) {

            const getInstagramPosts = async () => {

                const response = await axiosInstance.get('/instagramFeedResponse').catch(err => console.log(err));

                if (response && response.data.code === 200) {

                    if (response.data.data.length > 0) {

                        dispatch(setInstagramPosts(response.data.data));
                    }
                }
            }

            getInstagramPosts();
        }
        
        if (newArrivalsList.length === 0 || bannerImages.length === 0 || seasonTrends.length === 0) {

            setIsBannerLoading(true);
            const getHomeData = async () => {

                const response = await axiosInstance.get('/getHomeData', {
                    headers: {
                        Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                    }
                })
                    .catch(err => {

                        console.log(err);

                    })

                if (response && response.data.code === 200) {

                    if (response.data.data) {
                        
                        if (response.data.data.banner.length > 0) {
                            
                            const banners = response.data.data.banner.map(({image_url}) => image_url);
                            const randomIndex = Math.floor(Math.random() * banners.length);
                            dispatch(setTrendingImage(banners[randomIndex]));
                            dispatch(setBannerImages(response.data.data && banners));

                        }

                        if (response.data.data.panel.length > 0) {

                            dispatch(setSeasonTrends(response.data.data && response.data.data.panel));
                            
                        }

                        if (response.data.data.new_arr.length > 0) {

                            dispatch(setNewArrivalsList(response.data.data && response.data.data.new_arr));
                            
                        }

                        if (response.data.data.adminCategory.length > 0) {

                            const categories = [];

                            const adminCategory = response.data.data.adminCategory;

                            for (let data of adminCategory) {

                                let categoryObject = { sub_categories : [] };

                                const categoryIndex = categories.findIndex(category => category.title === data.category.parent_category_name);
                                
                                if (data.category.title && data.category.parent_category_name) {

                                    categoryObject.title = data.category.parent_category_name;
                                    categoryObject.id = data.category.parent_id;
    
                                    if (data.category.sub_categories.length > 0) {
                                        
                                        let subCategoryObject = { sub_categories : [] };

                                        for (let subCategory of data.category.sub_categories) {
                                            
                                            subCategoryObject.title = data.category.title;
                                            subCategoryObject.id = subCategory.parent_id;

                                            if (subCategory.title && subCategory.parent_category_name === subCategoryObject.title) {

                                                subCategoryObject.sub_categories.push({ id: subCategory.id, title : subCategory.title, sub_categories: []});
                                            }
                                        }

                                        if (categoryIndex !== -1) {

                                            categories[categoryIndex].sub_categories.push(subCategoryObject)
                                        }

                                        else {
                                            
                                            categoryObject.sub_categories.push(subCategoryObject);
                                        }
                                    }

                                    else {

                                        if (categoryIndex !== -1) {
                                            
                                            categories[categoryIndex].sub_categories.push({id: data.category.id, title : data.category.title , sub_categories : []})
                                        }
                                    }
                                }

                                else if (data.category.title && !data.category.parent_category_name) {

                                    categoryObject.title = data.category.title;
                                    categoryObject.id = data.category.id;

                                    if (data.category.sub_categories.length > 0) {

                                        categoryObject.sub_categories = data.category.sub_categories;
                                        categoryObject.sub_categories = categoryObject.sub_categories.map(category => { return {...category, sub_categories: []}})
                                    }
                                }

                                if (categoryIndex === -1) {

                                    categories.push(categoryObject);
                                }
                            }

                            dispatch(setCategories(categories));
                            dispatch(setProductRangeList(response.data.data.adminCategory));                        
                        }
                    }

                    setIsBannerLoading(false);                    
                }
            }

            getHomeData();
        }

    }, [])

    // if(isBannerLoading || isLoading) return <LoadingSpinner isLoading={isLoading} />

    return (

        <div className="home--page">
            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast 

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }

            {

                isBannerLoading ?

                <LoadingSpinner isLoading={isBannerLoading} />

                : null

            }

            <Header trendingImg={trendingImage}/>
            <CategoryHeader/>
            <Banner showDots bannerImages={bannerImages} showArrows />
            <AboutUs/>
            {/* <RelatedProducts relatedProducts={productRangeList}/> */}
            {   newArrivalsList.length > 0 &&
                <NewArrivals newArrivalsList={newArrivalsList} />
            }
            {   seasonTrends.length > 0 &&
                <SeasonTrends trendingImage={trendingImage} seasonTrends={seasonTrends} />
            }
            {/* <InstagramPosts instagramPosts={instagramPosts}/> */}
            <Footer/>
        </div>

    )
}

export default Home
