import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import ContactInfo from '../../components/Contact/ContactInfo/ContactInfo'
import Footer from '../../components/Footer/Footer'
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader'
import Header from '../../components/Header/Header'
import { getHomeData } from '../../redux/action/action'
import Backdrop from '../../utils/Backdrop/Backdrop';

function Contact({history}) {

    const {isLoading} = useSelector(state => state.products);
    const dispatch = useDispatch();

    useEffect(() => {
        
        dispatch(getHomeData());
    
    }, []);
    
    return (
        <div className="contact--page">
            {

                isLoading && <Backdrop isLoading={isLoading} />

            }

            <Header/>
            <CategoryHeader/>
            <BreadCrumb title="Contact Us"/>
            <ContactInfo/>
            <Footer/>
        </div>
    )
}

export default Contact
