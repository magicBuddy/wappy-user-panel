import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import Footer from '../../components/Footer/Footer'
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader'
import Header from '../../components/Header/Header'
import WishlistItems from '../../components/Wishlist/WishlistItems'
import { getHomeData, getWishlist, resetSnackbar } from '../../redux/action/action'
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner'
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast'
import Backdrop from '../../utils/Backdrop/Backdrop';

function Wishlist() {

    const {wishlistProducts} = useSelector(state => state.wishlist);
    const {isProgressing, isLoading} = useSelector(state => state.products);
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const dispatch = useDispatch();
    
    useEffect(() => {

        dispatch(getHomeData());
        if (wishlistProducts.length === 0) {

            dispatch(getWishlist());
        }

    }, []);

    return (

        <div className="cart--page">

            {
                isProgressing && <LoadingSpinner isLoading={isProgressing} />
            }
            {
                isLoading && <Backdrop isLoading={isLoading} />
            }
            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }
            <Header/>
            <CategoryHeader/>
            <BreadCrumb title="Wishlist"/>
            <WishlistItems wishlistProducts={wishlistProducts}/>
            <Footer/>
        </div>
    )
}

export default Wishlist
