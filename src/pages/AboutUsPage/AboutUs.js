import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CompanyInfo from '../../components/AboutUs/CompanyInfo/CompanyInfo'
import Intro from '../../components/AboutUs/Intro/Intro'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import Footer from '../../components/Footer/Footer'
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader'
import Header from '../../components/Header/Header'
import { getHomeData } from '../../redux/action/action'
import Backdrop from '../../utils/Backdrop/Backdrop';

function AboutUs() {
    
    const dispatch = useDispatch();
    const {isLoading} = useSelector(state => state.products);
    
    useEffect(() => {

        dispatch(getHomeData());

    }, []);

    return (
        <div className="about_us--page about">
            {
                isLoading && <Backdrop isLoading={isLoading} />
            }
            <Header/>
            <CategoryHeader/>
            <BreadCrumb title="About Us"/>
            <Intro/>
            <CompanyInfo/>
            <Footer/>
        </div>
    )
}

export default AboutUs
