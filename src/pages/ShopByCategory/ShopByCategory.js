import React, { useEffect, useState } from 'react';
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import CategorySidebar from '../../components/Shop/CategorySidebar/CategorySidebar';
import ShoppingList from '../../components/Shop/ShoppingList/ShoppingList';
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner';
import { useDispatch, useSelector } from 'react-redux';
import { getCategories, getHomeData, getProductList, setActiveCategory, setError, setLoadingState, setPaginationLinks, setProductList } from '../../redux/action/action';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';
import { resetSnackbar } from '../../redux/action/action';
import Backdrop from '../../utils/Backdrop/Backdrop';
import { useParams } from 'react-router-dom';
import ParentCategorySidebar from '../../components/ParenyCategorySidebar/ParentCategorySidebar';
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader';
import axiosInstance from '../../axios/axios-instance';

function ShopByCategory({history}) {

    const [view, setView] = useState("Grid");
    const {productList, isLoading, error, pageLimit, categories, isProgressing, activeCategory} = useSelector(state => state.products);
    const dispatch = useDispatch();
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const [category, setCategory] = useState("");
    const [subCategory, setSubCategory] = useState("");
    const [subSubCategory, setSubSubCategory] = useState("");
    const [currentPageCategory, setCurrentPageCategory] = useState([]);
    const {categoryName, subCategoryName, subSubCategoryName} = useParams();

    useEffect(() => {
        
        dispatch(getHomeData());
        const getProductByCategory = async () => {

            const match = {"-" : " "}

            const originalCategoryName = categoryName.replaceAll("-", " ");
            
            const response = await axiosInstance.get('/getProductByKeyword', {
                
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                },
                params: {
                    limit: pageLimit,
                    search_text: originalCategoryName
                }

            }).catch(err => console.log(err))

            if (response && response.data && response.data.code === 200) {
                
                if (categoryName && subCategoryName && subSubCategoryName) {
                    
                    let originalSubSubCategoryName = "";

                    if (subCategoryName.includes("R-Line")) {

                        originalSubSubCategoryName = subSubCategoryName.substring(0,6) + " " +subSubCategoryName.substring(7);
                    }

                    dispatch(setActiveCategory(originalSubSubCategoryName));

                    if (subCategoryName.includes("Permablend")) {

                        if (subSubCategoryName.includes("scar")) {

                            dispatch(setActiveCategory("Perma Blend Areola, Scar and Camouflage"))
                        }
                    }

                }

                else if (categoryName && subCategoryName) {
                    
                    dispatch(setActiveCategory(subCategoryName));
                }

                else dispatch(setActiveCategory(originalCategoryName));
            
                dispatch(setProductList(response.data.data && response.data.data.data));
                dispatch(setPaginationLinks(response.data.data && response.data.data.links));
                dispatch(setLoadingState(false));
            }

        }

        if (productList.length === 0) {
            getProductByCategory();
        }

        // if (categories.length === 0) {

        //     dispatch(getCategories());

        // }

    }, [])

    useEffect(() => {

        const originalCategoryName = categoryName.replaceAll("-", " ");
        setCategory(categoryName);
        setSubCategory(subCategoryName);
        setSubSubCategory(subSubCategoryName);

         if (categoryName && subCategoryName && subSubCategoryName) {

            let originalSubSubCategoryName = subSubCategoryName.replaceAll("-", " ");

            if (subCategoryName.includes("R-Line")) {

                originalSubSubCategoryName = "R-Line" + " " + originalSubSubCategoryName.substring(7);
                dispatch(setActiveCategory(originalSubSubCategoryName));

            }

            else if (subCategoryName.includes("Permablend")) {

                if (subSubCategoryName.includes("Scar")) {

                    dispatch(setActiveCategory("Perma Blend Areola, Scar and Camouflage"))
                }

                else {

                    dispatch(setActiveCategory(originalSubSubCategoryName));
                }
            } 

            else dispatch(setActiveCategory(originalSubSubCategoryName));

         } else if (categoryName && subCategoryName) {

            let originalSubCategoryName = subCategoryName.replaceAll("-", " ");

            dispatch(setActiveCategory(originalSubCategoryName));

         } else dispatch(setActiveCategory(originalCategoryName));

        const currentPageCat = [];

        for (let i=0; i<categories.length; i++) {


            const isCategoryPresent = categories[i].title.includes(categoryName.split("-")[0])
            
            if(isCategoryPresent){

                currentPageCat.push(categories[i]);

            }
        }

        setCurrentPageCategory(currentPageCat);

    }, [categoryName, categories])
    
    // if (isLoading) return <Backdrop isLoading={isLoading} />

    // if (isProgressing) return <Backdrop isProgressing={isProgressing}/>

    const handleSnackbarClose = () => {

        dispatch( setError({errorFor: "", errorMessage: ""}) )

    }

    return (
        <React.Fragment>

            {

                isProgressing && <LoadingSpinner isLoading={isProgressing} />

            }

            {

                isLoading && <Backdrop isLoading={isLoading} />

            }

            {error && error.errorMessage && error.errorFor === "productDetail" ?                
                <SnackbarToast 
                
                    message = {error.errorMessage} 
                    snackbarType = "warning" 
                    openSnackbar = {error.errorMessage && error.errorFor} 
                    handleSnackbarClose = {handleSnackbarClose}

                /> 
                : null 
            }

            
            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast 

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }


            <div className="shop--page">
                <Header/>
                <CategoryHeader/>
                <BreadCrumb title={activeCategory}/>
                <div className="shop container">
                    <div className="row">
                        <ParentCategorySidebar 
                            productsLength={productList.length}
                            categories={currentPageCategory.length > 1 ? currentPageCategory.splice(0,currentPageCategory.length-1) : currentPageCategory} 
                        />
                        <ShoppingList 
                            productList={productList} 
                            view={view} 
                            setView={setView}
                        />
                    </div>
                </div>
                <Footer/>
            </div>

        </React.Fragment>
    )
}

export default ShopByCategory
