import React, { useEffect, useState } from 'react';
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import CategorySidebar from '../../components/Shop/CategorySidebar/CategorySidebar';
import ShoppingList from '../../components/Shop/ShoppingList/ShoppingList';
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner';
import { useDispatch, useSelector } from 'react-redux';
import { getCategories, getHomeData, getProductList, setError } from '../../redux/action/action';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';
import { resetSnackbar } from '../../redux/action/action';
import Backdrop from '../../utils/Backdrop/Backdrop';
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader';

function Shop({history}) {

    const [view, setView] = useState("Grid");
    const {productList, isLoading, error, pageLimit, categories, isProgressing} = useSelector(state => state.products);
    const dispatch = useDispatch();
    const {snackbarDetails} = useSelector(state => state.snackbar);

    useEffect(() => {
        
        dispatch(getHomeData());
        dispatch(getProductList(pageLimit, 1));

    }, [])

    // if (isLoading) return <Backdrop isLoading={isLoading} />

    // if (isProgressing) return <Backdrop isProgressing={isProgressing}/>

    const handleSnackbarClose = () => {

        dispatch( setError({errorFor: "", errorMessage: ""}) )

    }

    return (
        <React.Fragment>

            {

                isProgressing && <LoadingSpinner isLoading={isProgressing} />

            }

            {

                isLoading && <Backdrop isLoading={isLoading} />

            }

            {error && error.errorMessage && error.errorFor === "productDetail" ?                
                <SnackbarToast 
                
                    message = {error.errorMessage} 
                    snackbarType = "warning" 
                    openSnackbar = {error.errorMessage && error.errorFor} 
                    handleSnackbarClose = {handleSnackbarClose}

                /> 
                : null 
            }

            
            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast 

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }


            <div className="shop--page">
                <Header/>
                <CategoryHeader/>
                <BreadCrumb title="Shop"/>
                <div className="shop container">
                    <div className="row">
                        <CategorySidebar productsLength={productList.length} categories={categories} />
                        <ShoppingList 
                            productList={productList} 
                            view={view} 
                            setView={setView}
                        />
                    </div>
                </div>
                <Footer/>
            </div>

        </React.Fragment>
    )
}

export default Shop
