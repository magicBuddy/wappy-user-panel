import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import ProductDetails from '../../components/ProductDetails/ProductDetails';
import RelatedProducts from '../../components/ProductDetails/RelatedProducts/RelatedProducts';
import { shopping_list } from '../../data/dummy/product_list';
import axiosInstance from '../../axios/axios-instance';
import Backdrop from '../../utils/Backdrop/Backdrop';
import { useDispatch, useSelector } from 'react-redux';
import { getHomeData, resetSnackbar, setError, setProductReviewList } from '../../redux/action/action';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader';
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner';

function ProductDetailsPage() {
    
    const [productDetails, setProductDetails] = useState({});
    const [relatedProducts, setRelatedProducts] = useState([]);
    const [canUserReviewProduct, setCanUserReviewProduct] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const {productId} = useParams();
    const dispatch = useDispatch();
    const history = useHistory();

    const {isProgressing, productReviewList} = useSelector(state => state.products);
    const {snackbarDetails} = useSelector(state => state.snackbar);

    useEffect(() => {

        setIsLoading(true);
        dispatch(getHomeData());
        
    }, []);

    useEffect(()=> {
    
        const getSingleProduct = async () => {

            setIsLoading(true);

            const singleProductResponse = await axiosInstance.get(`/product/${productId}`, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
                }
            }).catch(err => {
                
                if(err.response && err.response.data.code === 422 && err.response.data.message) {

                    setIsLoading(false);
                    dispatch( setError({errorMessage: err.response.data.message , errorFor: "productDetail"}) );
                    history.push('/shop');

                }

            })
            
            if (singleProductResponse && singleProductResponse.data.code === 200) {
            
                setProductDetails(singleProductResponse.data.data);
                const keywordsLength = singleProductResponse.data.data.keywords.split(",").length;
                
                if (keywordsLength > 0) {
                    
                    const keywordsArray = singleProductResponse.data.data.keywords.split(",");
                    const randomIndexNumber = Math.floor(Math.random() * keywordsLength);
                    
                    if(keywordsArray[randomIndexNumber]) {
                        const productsByKeywordResponse = await axiosInstance.get(`/getProductByKeyword`, {
                            headers: {
                                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
                            },
                            params: {
                                limit: 8,
                                search_text: keywordsArray[randomIndexNumber]
                            }
                        })
                        .catch(err => {
                            console.log(err.response);
                        })

                        if (productsByKeywordResponse && productsByKeywordResponse.data.code === 200) {

                            setRelatedProducts(productsByKeywordResponse.data.data.data);
                            const productReviewListResponse = await axiosInstance.get(`/getProductReviews`, {
                                params: {
                                    product_id: singleProductResponse.data.data.id,
                                    limit: 8,
                                    page: 1
                                },
                                headers: {
                                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")  
                                }
                            }).catch(err => console.log(err));

                            if (productReviewListResponse && productReviewListResponse.data.code === 200) {

                                setCanUserReviewProduct(productReviewListResponse.data.data.isHeCanReview);
                                dispatch(setProductReviewList(productReviewListResponse.data.data.review?.data));
                                setIsLoading(false);

                            }
                        }
                    }
                }
            }
        }

        getSingleProduct();

    }, [productId])

    if (isLoading) return <Backdrop isLoading={isLoading} />
    
    return (
        <div className="product-details--page">
            {

                isProgressing && <LoadingSpinner isLoading={isProgressing} />
                
            }
            
            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }

            <Header/>
            <CategoryHeader/>
            <BreadCrumb 
                title= {"Product Details"}
                subTitle= {"Shop"}
                productName= {productDetails && productDetails.name ? productDetails.name: ""}
            />
            <ProductDetails
                productReviewList={productReviewList}
                productId = {productDetails && productDetails.id}
                productName = {productDetails && productDetails.name ? productDetails.name: ""}
                productDescription = {productDetails && productDetails.description ? productDetails.description : ""}
                productCategory={productDetails ? productDetails.Category: ""}
                productRating={productDetails ? productDetails.Rating: ""}
                productPrice = {productDetails && productDetails.price ? productDetails.price: ""}
                productImage = {productDetails && productDetails.image_url ? productDetails.image_url : ""}
                productImages = {productDetails && productDetails.images && Array.isArray(productDetails.images) ? productDetails.images : ""}
                discountPrice = {productDetails ? productDetails.discountPrice: ""}
                productAvailability = {productDetails && productDetails.in_stock ? 1 : 0}
                productTotalRating = {productDetails && productDetails.total_rate}
                productAverageRating = {productDetails && productDetails.avg_rate}
                productStatus = {productDetails && productDetails.status ? productDetails.status : ""}
                canUserReview = {canUserReviewProduct}
                productKeywords = {productDetails && productDetails.keywords ? productDetails.keywords.split(",") : []}
            />
            {/* <RelatedProducts relatedProducts={relatedProducts}/> */}
            {/* <FooterProductImages/> */}
            <Footer/>
        </div>
    )
}

export default ProductDetailsPage
