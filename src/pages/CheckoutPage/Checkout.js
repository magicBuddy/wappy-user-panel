import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import CheckoutDetails from '../../components/Checkout/CheckoutDetails/CheckoutDetails'
import Footer from '../../components/Footer/Footer'
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader'
import Header from '../../components/Header/Header'
import { getHomeData, getPaymentMethods, getShippingDetails, resetSnackbar } from '../../redux/action/action'
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner'
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast'
import Backdrop from '../../utils/Backdrop/Backdrop';

function Checkout({history}) {

    const dispatch = useDispatch();
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const {cartProducts} = useSelector(state => state.cart);
    const {shippingDetails} = useSelector(state => state.shipping);
    const {paymentMethods, isOrderPlaced} = useSelector(state => state.order);
    const {isLoading, isProgressing} = useSelector(state => state.products);
   
    useEffect(() => {

        dispatch(getHomeData());

        dispatch(getPaymentMethods());

        dispatch(getShippingDetails());

    }, [])
    
    return (
        <div className="checkout--page">
            {

                isProgressing && <LoadingSpinner isLoading={isProgressing} />
                
            }
            {

                isLoading && <Backdrop isLoading={isLoading} />

            }

            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }
            <Header/>
            <CategoryHeader/>
            <BreadCrumb title="Checkout"/>
            {   
                cartProducts.length > 0 ?
                    <CheckoutDetails 

                        shippingDetails={shippingDetails} 
                        paymentMethods={paymentMethods}

                    />
                :
                    <h5 className="container">No Products in cart.</h5>
            }
            <Footer/>
        </div>
    )
}

export default Checkout
