import React, { useEffect, useState } from 'react'
import axiosInstance from '../../axios/axios-instance';
import Backdrop from '../../utils/Backdrop/Backdrop';
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast';
import PaymentSuccess from '../../assets/images/Payment-Success.svg';
import PaymentFailure from '../../assets/images/Payment-Failure.svg';
import { useHistory } from 'react-router';

function PaymentStatus() {

    const [isPaymentSuccessful, setIsPaymentSuccessful] = useState(false);

    const [transaction_details] = useState(new URLSearchParams(window.location.search).get('transaction_details'));
    const [result] = useState(new URLSearchParams(window.location.search).get('result'));
    const [isLoading, setIsLoading] = useState(false);
    const [snackbarType, setSnackbarType] = useState(false);
    const [message, setMessage] = useState('');
    const [isSnackbarOpen, setIsSnackbarOpen] = useState(false);

    const history = useHistory();

    useEffect(() => {

        const formData = new FormData();

        formData.set('order_id', transaction_details.split(",")[0]);
        formData.set('transaction_id', transaction_details.split(",")[1]);
        formData.set('enc_hex', result);

        setIsLoading(true);

        const getPaymentResponse = async () => {

            const paymentResponse = await axiosInstance.post('/resultPxPayment', formData).catch(err => {

                if (err && err.response.data) {

                    if (err && err.response.data.code) {

                        setMessage(err.response.data.message);
                        setSnackbarType("error");
                        setIsSnackbarOpen(true);
                        setIsLoading(false);
                    }
                }
            });

            if (paymentResponse && paymentResponse.data.code === 200) {

                if (paymentResponse.data.data.Success == 1) {
                  
                    setIsPaymentSuccessful(true);
                    setMessage(paymentResponse.data.message);
                    setSnackbarType("success");
                    setIsSnackbarOpen(true);
                }

                else {
                    
                    setIsPaymentSuccessful(false);
                    setMessage(paymentResponse.data.message);
                    setSnackbarType("error");
                    setIsSnackbarOpen(true);
                }

                setIsLoading(false);
            }
        } 

        getPaymentResponse();

    }, []);

    const handleSnackbarClose = () => {

        setIsSnackbarOpen(false);
        setSnackbarType('');
        setMessage('');
    }

    return (

        isLoading ? <Backdrop isLoading={isLoading} />
        : 
        !isLoading && isPaymentSuccessful ? 
        <div className="flex-column d-flex align-items-center justify-content-center" style={{height: "100vh"}}>

            <img style={{width: "300px", marginBottom: "40px"}} src={PaymentSuccess} alt="Payment Successful"/>
            <SnackbarToast
                message = {message} 
                snackbarType = {snackbarType} 
                openSnackbar = {isSnackbarOpen} 
                handleSnackbarClose = {handleSnackbarClose}
            /> 
            <button class="btn -dark">Payment Successful</button>
        </div>
        :
        !isLoading && !isPaymentSuccessful ? 
        
        <div className="flex-column d-flex align-items-center justify-content-center" style={{height: "100vh"}}>

            <img style={{width: "300px", marginBottom: "40px"}} src={PaymentFailure} alt="Payment Failure"/>
            <SnackbarToast
                message = {message} 
                snackbarType = {snackbarType} 
                openSnackbar = {isSnackbarOpen} 
                handleSnackbarClose = {handleSnackbarClose}
            /> 
            <button class="btn -dark">Payment Unsuccessful</button>
        </div>
        : null
    )
}

export default PaymentStatus
