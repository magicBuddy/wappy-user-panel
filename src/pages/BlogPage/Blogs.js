import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import BlogSection from '../../components/Blogs/BlogSection'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
import { getHomeData } from '../../redux/action/action'
import Backdrop from '../../utils/Backdrop/Backdrop';

function Blogs({history}) {

    const dispatch = useDispatch();
    const {isLoading} = useSelector(state => state.products);
    
    useEffect(() => {

        dispatch(getHomeData());

    }, []);
    
    return (
        <div className="blogs--page">
            {
                isLoading && <Backdrop isLoading={isLoading} />
            }
            <Header/>
            <BreadCrumb title="Blog"/>
            <BlogSection/>
            <Footer/>
        </div>
    )
}

export default Blogs
