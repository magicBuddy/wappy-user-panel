import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import BreadCrumb from '../../components/BreadCrumb/BreadCrumb'
import CartList from '../../components/Cart/CartList/CartList'
import Footer from '../../components/Footer/Footer'
import CategoryHeader from '../../components/Header/CategoryHeader/CategoryHeader'
import Header from '../../components/Header/Header'
import { getHomeData, resetSnackbar } from '../../redux/action/action'
import LoadingSpinner from '../../utils/LoadingSpinner/LoadingSpinner'
import SnackbarToast from '../../utils/SnackbarToast/SnackbarToast'
import Backdrop from '../../utils/Backdrop/Backdrop';

function Cart({history}) {

    const {cartProducts} = useSelector(state => state.cart);
    const {isProgressing, isLoading} = useSelector(state => state.products);
    const {snackbarDetails} = useSelector(state => state.snackbar);
    const dispatch = useDispatch();
    
    useEffect(() => {

        dispatch(getHomeData());

    }, []);
    
    return (
        <div className="cart--page">

            {
                isProgressing && <LoadingSpinner isLoading={isProgressing} />
            }
            {
                isLoading && <Backdrop isLoading={isLoading} />
            }
            {
                snackbarDetails.type && snackbarDetails.open && snackbarDetails.message &&
            
                <SnackbarToast

                    snackbarType={snackbarDetails.type} 
                    openSnackbar={snackbarDetails.open} 
                    message={snackbarDetails.message} 
                    handleSnackbarClose={() => dispatch(resetSnackbar())}
                    
                />

            }
            <Header/>
            <CategoryHeader/>
            <BreadCrumb title="Cart"/>
            <CartList cartProducts={cartProducts}/>
            {/* <FooterProductImages/> */}
            <Footer/>
        </div>
    )
}

export default Cart
