import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: 1111111,
    color: '#fff',
  },
}));

export default function BlockUI({isLoading}) {
  
    const classes = useStyles();

    return (
        <Backdrop style={{backgroundColor: "white"}} className={classes.backdrop} open={isLoading}>
            <CircularProgress style={{color: "black"}} />
        </Backdrop>
    );
}