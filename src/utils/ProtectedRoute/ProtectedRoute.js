import React from 'react'
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

function ProtectedRoute({component: Component, ...otherProps }) {
    
    // const {userDetails} = useSelector(state => state.auth);

    return (
        <Route {...otherProps} render={(props) => {
            
            if (localStorage.getItem("access_token_wappy")) return <Component {...props} /> 
                    
            else return <Redirect to="/login"/>
            
        }}/>
    )
}

export default ProtectedRoute;