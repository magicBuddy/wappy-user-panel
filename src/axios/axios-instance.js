import axios from 'axios';
import { BASE_URL } from './api';


const axiosInstance = axios.create({
    baseURL: `${BASE_URL}`
})

export const setTokenInHeaders = token => {
    console.log(token);
    if (token) {
        axiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    } else {
        delete axiosInstance.defaults.headers.common['Authorization'];
    }
}

export default axiosInstance;