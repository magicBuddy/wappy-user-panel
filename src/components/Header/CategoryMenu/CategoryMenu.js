import { Link } from 'react-router-dom';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axiosInstance from '../../../axios/axios-instance';
import { setActiveCategory, setLoadingState, setPaginationLinks, setProductList } from '../../../redux/action/action';
import { ExpandMore } from '@material-ui/icons';

function CategoryMenu({
    categoryId,
    title,
    subCategories
}) {

    const dispatch = useDispatch();
    const {activeCategory, pageLimit} = useSelector(state => state.products);
    
    const getProductListByKeyword = async (limit, title) => {

        dispatch(setLoadingState(true));
        
        const response = await axiosInstance.get('/getProductByKeyword', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            },
            params: {
                limit,
                search_text: title
            }
        }).catch(err => console.log(err))

        if (response && response.data && response.data.code === 200) {

            dispatch(setActiveCategory(title));
            dispatch(setProductList(response.data.data && response.data.data.data));
            dispatch(setPaginationLinks(response.data.data && response.data.data.links));
            dispatch(setLoadingState(false));
        }
    }

    return (

        <li className="beauty-salon-items">
            <Link to="/shop" onClick={() => getProductListByKeyword(pageLimit,title)}>{title}
                <span className="dropable-icon">
                    { subCategories && subCategories.length > 0 && <i style={{transform:"rotate(-90deg)"}} className="fas fa-angle-down"></i> }
                </span>
            </Link>

            {
                subCategories && subCategories.length > 0 ?

                    <ul className="beauty-salon-dropdown-menu">

                        <li className="pt-3 pb-2 d-flex align-items-center justify-content-center" style={{fontWeight: 600, fontSize: "0.8rem", textAlign: "center", color :"black"}}><span>Sub Category</span><ExpandMore/></li>
                        {
                            subCategories.map(category => (
                                
                                <li className="beauty-salon-items" key={category.id}>
                                    <Link to="/shop" onClick={() => getProductListByKeyword(pageLimit, category.title)}>{category.title}
                                        <span className="dropable-icon">
                                            { category.sub_categories && category.sub_categories.length > 0 && <i style={{transform:"rotate(-90deg)"}} className="fas fa-angle-down"></i> }
                                        </span>
                                    </Link>
                                    {/* <Link  to="/shop" onClick={() => getProductListByKeyword(pageLimit,title,category.title)}>
                                        <span className="dropable-icon d-block text-center mr-2">{category.title}</span>
                                    </Link> */}

                                    {
                                        category.sub_categories && category.sub_categories.length > 0 ?

                                            <ul className="beauty-salon-dropdown-menu">

                                                <li className="pt-3 pb-2 d-flex align-items-center justify-content-center" style={{fontWeight: 600, fontSize: "0.8rem", textAlign: "center", color: "black"}}><span>Sub Category</span><ExpandMore/></li>
                                                {
                                                    category.sub_categories.map(sub_category => (
                                                        
                                                        <li key={sub_category.id}>
                                                            <Link to="/shop" onClick={() => getProductListByKeyword(pageLimit,sub_category.title)}>{sub_category.title}
                                                                <span className="dropable-icon">
                                                                    { sub_category.sub_categories && sub_category.sub_categories.length > 0 && <i style={{transform:"rotate(-90deg)"}} className="fas fa-angle-down"></i> }
                                                                </span>
                                                            </Link>

                                                            {
                                                                sub_category.sub_categories && sub_category.sub_categories.length > 0 ?

                                                                    <ul className="beauty-salon-dropdown-menu">

                                                                        <li className="pt-3 pb-2 d-flex align-items-center justify-content-center" style={{fontWeight: 600, fontSize: "0.8rem", textAlign: "center", color: "black"}}><span>Sub sub_Category</span><ExpandMore/></li>
                                                                        {
                                                                            sub_category.sub_categories.map(category => (
                                                                                
                                                                                <li key={category.id}>
                                                                                    <Link  to="/shop" onClick={() => getProductListByKeyword(pageLimit, category.title)}>
                                                                                        <span className="d-block mr-2">{category.title}</span>
                                                                                    </Link>

                                                                                    {
                                                                                        
                                                                                    }
                                                                                </li>
                                                                                
                                                                            ))
                                                                        }
                                                                    </ul>   
                                                                : null
                                                            }
                                                        </li>
                                                        
                                                    ))
                                                }
                                            </ul>   
                                        : null
                                    }
                                </li>
                                
                            ))
                        }
                    </ul>   
                : null
            }

        </li>
    )
    
}

export default CategoryMenu
