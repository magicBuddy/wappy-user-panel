import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import axiosInstance from '../../../axios/axios-instance';
import { setActiveCategory, setLoadingState, setPaginationLinks, setProductList } from '../../../redux/action/action';
import CategoryMenu from '../CategoryMenu/CategoryMenu';
import SubCategoryMenu from './SubCategoryMenu/SubCategoryMenu';

function CategoryHeader() {

    const {categories, pageLimit} = useSelector(state => state.products);
    const match = {'/': '-', '&': 'and', ',': '', ' ': '-'};

    const dispatch = useDispatch()
    const getProductListByKeyword = async (limit, title) => {

        dispatch(setLoadingState(true));
        
        const response = await axiosInstance.get('/getProductByKeyword', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            },
            params: {
                limit,
                search_text: title
            }
        }).catch(err => console.log(err))

        if (response && response.data && response.data.code === 200) {

            dispatch(setActiveCategory(title));
            dispatch(setProductList(response.data.data && response.data.data.data));
            dispatch(setPaginationLinks(response.data.data && response.data.data.links));
            dispatch(setLoadingState(false));
        }
    }

    return (
        <div className="header -three category-header">
            <div className="menu -style-3">
                <div className="container">
                    <div className="menu__wrapper" style={{justifyContent: "center"}}>
                        <div className="navigator -white">
                            <ul className="top-level-menu">
                                {categories && categories.length > 0 &&
                                    categories.slice(0,7).map(category => (
                                        <li
                                            key={category.id}
                                            className="relative m"                                            
                                        >
                                        
                                        <Link 
                                            style={{cursor: "pointer", color: "black", fontWeight: 600, fontSize: "0.8em"}}
                                            to={`/categories/${category.title.replace(/[/ /&/,]/g, ch => match[ch])}`} 
                                            onClick={() => getProductListByKeyword(pageLimit, category.title)}
                                        >
                                            {category.title}
                                        </Link>

                                        {category.sub_categories && category.sub_categories.length > 0 ?

                                            <ul className="dropdown-menu sub-category-dropdown-menu">

                                                <div className="row" style={{flexDirection: "column"}}>

                                                    {category.sub_categories.map(cat => (

                                                            <SubCategoryMenu

                                                                parentCategory={category.title.replace('/','-').replace(/&/g,'and').replace(/ /g, '-')}
                                                                key={cat.id}
                                                                title={cat.title}
                                                                subCategories={cat.sub_categories}
                                                                categoryId={cat.id}

                                                            />
                                                        ))                                                    
                                                    }        
                                                </div>                                
                                            </ul>

                                            : null
                                        }

                                        </li>
                                    ))
                                }  
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    )
}

export default CategoryHeader
