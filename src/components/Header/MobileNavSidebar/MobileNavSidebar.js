import { Avatar, makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {Link} from 'react-router-dom';
import axiosInstance from "../../../axios/axios-instance";
import { logoutUser, setActiveCategory, setLoadingState, setPaginationLinks, setProductList } from "../../../redux/action/action";

const useStyles = makeStyles((theme) => ({
    large: {
        width: 50,
        height: 50,
        margin: "auto",
        boxShadow: "0px 0px 10px rgba(0,0,0,0.5)"
    }
}))

export default function MobileNavSidebar({ trendingImage, showMobileNav, setShowMobileNav, categories }) {

    const classes = useStyles();
    const [openCategoryMenu, setOpenCategoryMenu] = useState(false);
    const [openSubCategoryMenu, setOpenSubCategoryMenu] = useState(false);
    const dispatch = useDispatch();
    const {activeCategory, pageLimit} = useSelector(state => state.products);
    const {userDetails} = useSelector(state => state.auth);
    const match = {'/': '-', '&': 'and', ',': '', ' ': '-'};
    
    useEffect(() => {

        onToggleMobileSidebar(showMobileNav);

    }, [showMobileNav]);

    useEffect(() => {

    }, [])

    const onToggleMobileSidebar  = showMobileNav => {

        const bodyEl = document.getElementsByTagName("BODY")[0];
        const drawerBackdropEl = document.getElementById("drawer-backdrop");

        if (showMobileNav) {

            bodyEl.classList.add("drawer-open");
            drawerBackdropEl.addEventListener('click', () => {
                setShowMobileNav(false);
            })
            drawerBackdropEl.classList.add("drawer-backdrop", "fade", "show")
        } else {

            if (bodyEl && drawerBackdropEl) {
                bodyEl.classList.remove("drawer-open");
                drawerBackdropEl.classList.remove("drawer-backdrop", "fade", "show")
            }
        }

    }

    const onToggleCategoryMenu = () => {
        setOpenCategoryMenu(openCategoryMenu => !openCategoryMenu);
    }

    const onToggleSubCategoryMenu = (categoryTitle, index) => {

        const categoryTitleEl = document.getElementById(`${categoryTitle}`);
        if (categoryTitleEl.style.display === "block") {
            categoryTitleEl.style.display = "none";
            document.getElementById(`0${index}${categoryTitle}`).classList.replace('fa-angle-up','fa-angle-down')

        }
        else {
            categoryTitleEl.style.display = "block";
            document.getElementById(`0${index}${categoryTitle}`).classList.replace('fa-angle-down','fa-angle-up')
        }
    }

    const getProductListByKeyword = async (limit, title) => {

        dispatch(setLoadingState(true));

        const response = await axiosInstance.get('/getProductByKeyword', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            },
            params: {
                limit,
                search_text: title
            }
        }).catch(err => console.log(err))

        if (response && response.data && response.data.code === 200) {

            dispatch(setActiveCategory(title));
            dispatch(setProductList(response.data.data && response.data.data.data));
            dispatch(setPaginationLinks(response.data.data && response.data.data.links));
            dispatch(setLoadingState(false));
            setShowMobileNav(false);
            onToggleMobileSidebar(false);

        }

    }

    const onUserLogoutHandler = () => {
        dispatch(logoutUser());
        setShowMobileNav(false);
        onToggleMobileSidebar(false);
    }

    return (
    <>
      
      <div class={`drawer drawer-right slide ${showMobileNav && "show"}`} style={{display: `${showMobileNav ? 'block' : 'none'}`}} id="mobile-menu-drawer" tabindex="-1" role="dialog" aria-labelledby="drawer-demo-title" aria-hidden="true">
        <div class="drawer-content drawer-content-scrollable" role="document">
          <div class="drawer-body">
            <div class="cart-sidebar">
                <div class="cart-items__wrapper">
                    <div class="navigation-sidebar">
                        <div class="user-profile-box" style={{background: `url(${trendingImage}) rgba(0,0,0,0.5)`, height: Object.keys(userDetails).length === 0 ? "155.5px" : "auto"}}>
                            { Object.keys(userDetails).length > 0 ?
                                    <Avatar alt="Username" src={userDetails.image_url ? userDetails.image_url : "https://bsbiztogo.org/wp/wp-content/uploads/2016/03/1434558152886.png"} className={classes.large} />
                                : null 
                            }
                            <span className="d-block text-center">{userDetails && userDetails.name}</span>
                        </div>
                        <div class="navigator-mobile">
                            <ul>
                                <li class="relative"><Link class="dropdown-menu-controller" to="/">Home</Link>
                                </li>
                                <li class="relative"><Link class="dropdown-menu-controller" to="/shop">Shop</Link>
                                </li>
                                <li>
                                    <a class="dropdown-menu-controller" onClick={(e) => {e.preventDefault(); onToggleCategoryMenu(true);}}>Categories
                                    { categories && categories.length > 0 ? <span class="dropable-icon"><i class={`${openCategoryMenu ? "fa-angle-up" : "fa-angle-down"} fas`}></i></span> : null}
                                    </a>
                                    {
                                        categories && categories.length > 0 ?

                                        <ul class="dropdown-menu" style={{display: `${openCategoryMenu ? 'block' : 'none'}`}}>

                                            {categories.map((category,index) => (

                                                <ul class="dropdown-menu__col">   

                                                    <li>
                                                        
                                                        <Link to={`/categories/${category.title.replace(/[/ /&/,]/g, ch => match[ch])}`} onClick={(e) => {getProductListByKeyword(pageLimit,category.title);}}> {category.title}
                                                        {
                                                            category.sub_categories && category.sub_categories.length > 0 ?  <span class="dropable-icon" onClick={(e) => {e.preventDefault(); e.stopPropagation(); onToggleSubCategoryMenu(category.title,index);}}><i id={`0${index}${category.title}`} class="fas fa-angle-down"></i></span> : null 
                                                        }                                                               
                                                        </Link>

                                                        {category.sub_categories && category.sub_categories.length > 0 ?

                                                            <ul id={`${category.title}`} style={{display: "none"}} class="dropdown-menu">

                                                            {category.sub_categories.map((sub_category,index) => (

                                                                <ul class="dropdown-menu__col">                                
                                                                    <li>
                                                                        
                                                                        <Link to={`/categories/${category.title.replace(/[/ /&/,]/g, ch => match[ch])}/${sub_category.title.replace('/','-').replace(/&/g,'and').replace(/ /g, '-')}`} onClick={(e) => {getProductListByKeyword(pageLimit, sub_category.title);}}> {sub_category.title}
                                                                        {
                                                                            sub_category.sub_categories && sub_category.sub_categories.length > 0 ?  <span onClick={(e) => { e.preventDefault(); e.stopPropagation(); onToggleSubCategoryMenu(sub_category.title,index);}} class="dropable-icon"><i id={`0${index}${sub_category.title}`} class="fas fa-angle-down"></i></span> : null 
                                                                        }                                                               
                                                                        </Link>

                                                                        {sub_category.sub_categories && sub_category.sub_categories.length > 0 ?

                                                                            <ul id={`${sub_category.title}`} style={{display: "none"}} class="dropdown-menu">

                                                                            {sub_category.sub_categories.map((sub_sub_category,index) => (

                                                                                <ul class="dropdown-menu__col">                                
                                                                                    <li>
                                                                                        
                                                                                        <Link to={`/categories/${category.title.replace(/[/ /&/,]/g, ch => match[ch])}/${sub_category.title.replace('/','-').replace(/&/g,'and').replace(/ /g, '-')}/${sub_sub_category.title.replace(/[/ /&/,]/g, ch => match[ch])}`} onClick={(e) => { getProductListByKeyword(pageLimit, sub_sub_category.title);}}> {sub_sub_category.title}
                                                                                        {
                                                                                            sub_sub_category.sub_categories && sub_sub_category.sub_categories.length > 0 ?  <span onClick={(e) => {e.preventDefault(); e.stopPropagation(); onToggleSubCategoryMenu(sub_sub_category.title,index);}} class="dropable-icon"><i id={`0${index}${sub_category.title}`} class="fas fa-angle-down"></i></span> : null 
                                                                                        }                                                               
                                                                                        </Link>

                                                                                    </li>
                                                                                </ul>

                                                                            ))}

                                                                            </ul>

                                                                            : null
                                                                        }

                                                                    </li>
                                                                </ul>

                                                            ))}

                                                        </ul>
                                                            : null
                                                        }

                                                    </li>
                                                </ul>

                                            ))}

                                        </ul>

                                        : null
                                        
                                    }
                                    
                                </li>
                                <li><Link to="/about">About Us</Link></li>
                                <li><Link to="/contact">Contact Us</Link></li>
                                { Object.keys(userDetails).length > 0 ?
                                    <li><a href="" onClick={(e) => {e.preventDefault(); onUserLogoutHandler();}}>Logout</a></li>
                                    :
                                    <React.Fragment>
                                        <li><Link onClick={() => {setShowMobileNav(false); onToggleMobileSidebar(false);}} to="/login">Login</Link></li>
                                        <li><Link onClick={() => {setShowMobileNav(false); onToggleMobileSidebar(false);}} to="/register">Register</Link></li>
                                    </React.Fragment>
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
