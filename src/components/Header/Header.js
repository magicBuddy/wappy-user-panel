import React, { useEffect, useState } from 'react';
import Logo from '../../assets/images/logo.png';
import CartIcon from '../../assets/images/header/cart-icon-white.png';
import WishlistIcon from '../../assets/images/header/wishlist-icon-white.png';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getCartDetails, getCategories, getConfigData, logoutUser } from '../../redux/action/action';
import CategoryMenu from './CategoryMenu/CategoryMenu';
import { Avatar, makeStyles, Menu, MenuItem } from '@material-ui/core';
import { ExitToApp, ExpandMore, Settings } from '@material-ui/icons';
import MobileNavSidebar from './MobileNavSidebar/MobileNavSidebar';

const useStyles = makeStyles((theme) => ({
    large: {
        width: theme.spacing(4),
        height: theme.spacing(4),
    }
}))

function Header({history, trendingImg}) {
    
    const {cartProducts, isCartDetailsFetched, totalCartPrice} = useSelector(state => state.cart);
    const {categories} = useSelector(state => state.products);
    const {userDetails} = useSelector(state => state.auth);
    const dispatch = useDispatch();
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = useState(null);
    const [showMobileNav, setShowMobileNav] = useState(false);
    const [trendingImage, setTrendingImage] = useState(false);

    const onOpenProfileMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const onCloseProfileMenu = () => {
        setAnchorEl(null);
    };

    const onUserLogoutHandler = () => {
        dispatch(logoutUser());
        setAnchorEl(null);
    }

    useEffect(() => {

        dispatch(getConfigData());
        
    }, [])
    
    useEffect(() => {

        if(!isCartDetailsFetched) {

            dispatch(getCartDetails());

        }

        const banners = localStorage.getItem("banners");

        if (banners && Array.isArray(JSON.parse(banners))) {

            const randomIndex = Math.floor(Math.random() * JSON.parse(banners).length);
            setTrendingImage(JSON.parse(banners)[randomIndex]);

        }

        else {
            setTrendingImage(trendingImg);
        }

    }, [trendingImg])

    return (
        <div className="header -three">
            <div className="menu -style-3">
                <div className="container">
                    <div className="menu__wrapper"><Link to="/"><img width="30" style={{transform: "scale(1.7)"}} src={Logo} alt="Logo"/></Link>
                        <div className="navigator -white">
                            <ul className="top-level-menu">
                                <li>
                                    <Link to="/">Home</Link>                                
                                </li>
                                <li className="relative">
                                    <Link to="/shop">Shop
                                        {/* <span className="dropable-icon">
                                            <i className="fas fa-angle-down"></i>
                                        </span> */}
                                    </Link>

                                    {/* <ul className="dropdown-menu">

                                        <li className="mb-2 d-flex align-items-center" style={{fontWeight: 600, color: "black"}}><span>Categories</span><ExpandMore/></li>
                                        {categories && categories.length > 0 &&
                                            categories.map(category => (

                                                <CategoryMenu

                                                    key={category.id}
                                                    title={category.title}
                                                    subCategories={category.sub_categories}
                                                    categoryId={category.id}

                                                />
                                            ))
                                        }                                        
                                    </ul> */}
                                </li>
                                <li><Link to="/about">About Us</Link></li>                                
                                {/* <li><Link to="/blog">Blog</Link></li> */}
                                <li><Link to="/contact">Contact Us</Link></li>
                            </ul>
                        </div>
                        <div className="menu-functions -white">
                            <Link class="menu-icon -wishlist" to="/wishlist">
                                <img src={WishlistIcon} alt="Wishlist icon"/>
                            </Link>
                            <div className="menu-cart">
                                <Link className="menu-icon -cart" to="/cart">
                                    <img src={CartIcon} alt="Cart icon"/>
                                    <span className="cart__quantity">{cartProducts.length}</span>
                                </Link>
                                <h5>Cart:<span> ${totalCartPrice}</span></h5>
                            </div>
                            <Link className="menu-icon -navbar"
                                onClick={(e) => {
                                    e.preventDefault();
                                    setShowMobileNav(!showMobileNav);
                                }}
                            >
                                <div className="bar"></div>
                                <div className="bar"></div>
                                <div className="bar"></div>
                            </Link>

                            { Object.keys(userDetails).length ?
                                <div className="align-items-center user-profile ml-5">
                                    <Avatar alt="Username" src={userDetails.image_url ? userDetails.image_url : "https://bsbiztogo.org/wp/wp-content/uploads/2016/03/1434558152886.png"} className={classes.large} />
                                    <span style={{color: "white"}} className="d-block ml-3 mr-1">{userDetails && userDetails.name}</span>                                
                                    <ExpandMore onClick={onOpenProfileMenu} style={{color: "white", fontSize: "1.2rem", cursor: "pointer"}}/>
                                    <Menu
                                        id="simple-menu"
                                        anchorEl={anchorEl}
                                        keepMounted
                                        open={Boolean(anchorEl)}
                                        onClose={onCloseProfileMenu}
                                    >

                                    <MenuItem onClick={onUserLogoutHandler}>
                                        <span className="d-flex align-items-center"><ExitToApp style={{fontSize: "1.2rem"}} className="mr-2"/> 
                                            <span style={{fontFamily: "Spartan"}}>Logout</span>
                                        </span>
                                    </MenuItem>

                                    </Menu>
                                </div>
                                : 
                                <Link style={{textDecoration: "none"}} className="login-link d-flex align-items-center" to="/login">
                                    <ExitToApp className="ml-4 mr-1" style={{color: "white"}}/>
                                    <span style={{color: "white", fontSize: "0.875em"}} className="d-block mr-1">Login</span>
                                </Link>
                        }
                        </div>
                        <MobileNavSidebar
                            trendingImage={trendingImage}
                            showMobileNav={showMobileNav}
                            setShowMobileNav={setShowMobileNav}
                            categories={categories}
                        />
                    </div>
                </div>
            </div>
      </div>
    )
}

export default Header
