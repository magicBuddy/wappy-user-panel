import React, { useEffect, useMemo } from "react";
// import { useStripe, useElements, CardElement, CardNumberElement, CardCvcElement, CardExpiryElement } from "@stripe/react-stripe-js";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { Checkbox, FormControlLabel, TextField, withStyles } from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import CreditCardInput from 'react-credit-card-input';

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const useOptions = () => {
  const options = useMemo(
    () => ({
      style: {
        base: {
          color: '#32325d',
          letterSpacing: "0.025em",
          fontFamily: "Source Code Pro, monospace",
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#aab7c4'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      }
    }),
    []
  );

  return options;
};

const StripePaymentForm = ({
  cardDetails,
  handleFormSubmit,
  cardValidationError,
  changeCardDetailsHandler,
  isCardSaved,
  setIsCardSaved
}) => {

  const {totalCartPrice} = useSelector(state => state.cart);

  return (

      // <form onSubmit={handleFormSubmit}>
        
      //   <TextField
      //     className="mb-2 d-block"
      //     placeholder="Card Number"
      //     name="card"
      //     value={cardDetails && cardDetails.card}
      //     onChange={changeCardDetailsHandler}
      //     variant="outlined"
      //   />
      //   <TextField
      //     className="mb-2"
      //     name="month"
      //     placeholder="Expiry Month"
      //     variant="outlined"
      //     value={cardDetails && cardDetails.month}
      //     onChange={changeCardDetailsHandler}
      //   />
      //   <TextField
      //     className="ml-2"
      //     placeholder="Expiry Year"
      //     name="year"
      //     variant="outlined"
      //     value={cardDetails && cardDetails.year}
      //     onChange={changeCardDetailsHandler}
      //   />
      //   <TextField
      //     className="mb-2 d-block"
      //     name="cvv"
      //     placeholder="CVV"
      //     variant="outlined"
      //     value={cardDetails && cardDetails.cvv}
      //     onChange={changeCardDetailsHandler}
      //   />
        
      //   <FormControlLabel
      //     className="mb-2"
      //     style={{display : 'block'}}
      //     control={<GreenCheckbox checked={isCardSaved} onChange={(e) => setIsCardSaved(isCardSaved => !isCardSaved)} name="checkedG" />}
      //     label={<span style={{fontFamily: "Spartan"}}>Save this card</span>}
      //   />
      //   { cardValidationError && <div className="card-errors" role="alert">{cardValidationError}</div> }
      //   <button className={`${cardValidationError ? 'mt-3' : ''} btn -red`} type="submit">
      //     Pay ${totalCartPrice}
      //   </button>
      // </form>
      <>
      <CreditCardInput
        cardNumberInputRenderer={({ handleCardNumberChange, props }) => (
          <input
            {...props}
            name="card"
            value={cardDetails && cardDetails.card}
            onChange={handleCardNumberChange(e =>
              changeCardDetailsHandler(e)
            )}
          />
        )}
        cardExpiryInputRenderer={({ handleCardExpiryChange, props }) => (
          <input
            {...props}
            name="expiry"
            value={cardDetails && cardDetails.expiry}
            onChange={handleCardExpiryChange(e =>
              changeCardDetailsHandler(e)
            )}
          />
        )}
        cardCVCInputRenderer={({ handleCardCVCChange, props }) => (
          <input
            {...props}
            name="cvc"
            value={cardDetails && cardDetails.cvc}
            onChange={handleCardCVCChange(e =>
              changeCardDetailsHandler(e)
            )}
          />
        )}
        fieldClassName="input"
      />

      { cardValidationError && <div className="card-errors" role="alert">{cardValidationError}</div> }
      <FormControlLabel
          className={`${cardValidationError ? 'mt-2' : ''} mb-2`}
          style={{display : 'block', width: 'max-content'}}
          control={<GreenCheckbox checked={isCardSaved} onChange={(e) => setIsCardSaved(isCardSaved => !isCardSaved)} name="checkedG" />}
          label={<span style={{fontFamily: "Spartan"}}>Save this card</span>}
        />
        <button onClick={handleFormSubmit} className={`btn -red`}>
          Pay ${totalCartPrice}
        </button>
      </>
  );
};

export default StripePaymentForm;
