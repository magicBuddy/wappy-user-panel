import React from 'react'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core'

function AddressUpdateDialog({
    openAddressUpdateDialog,
    onCloseAddressUpdateDialog,
    onUpdateShippingDetail,
    setCity,
    setCountry,
    setState,
    setPostCode,
    setStreetAddress,
    setContactNumber,
    city,
    state,
    country,
    streetAddress,
    postCode,
    contactNumber
}) {
    return (
        <Dialog open={openAddressUpdateDialog} onClose={openAddressUpdateDialog}>
            <DialogTitle>Update Shipping Detail</DialogTitle>
            <DialogContent>                
                    <TextField 
                        id="address" 
                        fullWidth 
                        className="mb-1"
                        required
                        label="Address"
                        name="address" 
                        value={streetAddress}
                        onChange={(e) => setStreetAddress(e.target.value)}
                    />
                    <TextField 
                        id="country" 
                        fullWidth 
                        required
                        className="mb-1"
                        label="Country"
                        name="country" 
                        value={country}
                        onChange={(e) => setCountry(e.target.value)}
                    />
                    <TextField
                        id="city" 
                        fullWidth 
                        required
                        className="mb-1"
                        label="Town/city"
                        name="city" 
                        value={city}
                        onChange={(e) => setCity(e.target.value)}
                    />
                    <TextField 
                        id="state" 
                        fullWidth 
                        className="mb-1"
                        required
                        label="State"
                        name="state" 
                        value={state}
                        onChange={(e) => setState(e.target.value)}
                    />
                    <TextField 
                        id="postCode" 
                        fullWidth 
                        required
                        className="mb-1"
                        label="PostCode/ZIP"
                        name="postCode"
                        value={postCode} 
                        onChange={(e) => setPostCode(e.target.value)}
                    />
                    { contactNumber &&
                        <TextField 
                            id="contactNumber" 
                            fullWidth 
                            required
                            className="mb-1"
                            label="Contact Number"
                            name="contactNumber"
                            value={contactNumber} 
                            onChange={(e) => setContactNumber(e.target.value)}
                        />
                    }

            </DialogContent>
            <DialogActions>
                <Button variant="contained" className="btn -red" onClick={onCloseAddressUpdateDialog}>Cancel</Button>
                <Button variant="contained" className="btn -dark" onClick={onUpdateShippingDetail}>Update</Button>
            </DialogActions>
        </Dialog>
    )
}

export default AddressUpdateDialog
