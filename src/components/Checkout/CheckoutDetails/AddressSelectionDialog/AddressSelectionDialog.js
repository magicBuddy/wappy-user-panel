import React from 'react'
import { AppBar, Button, Dialog, Divider, IconButton, List, ListItem, ListItemText, makeStyles, Slide, Toolbar, Typography, Collapse, Fab, Tooltip } from '@material-ui/core';
import { Check, CheckCircle, Close, Delete, Edit, KeyboardArrowDown } from '@material-ui/icons';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import AddressDeleteDialog from '../AddressDeleteDialog/AddressDeleteDialog';
import { removeShippingDetail, setShippingDetails, showSnackbar, updateShippingDetail } from '../../../../redux/action/action';
import AddressUpdateDialog from '../AddressUpdateDialog/AddressUpdateDialog';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});


function AddressSelectionDialog({

    onToggleAddressSelectionDialog,
    isOpenAddressSelectionDialog,
    selectedAddressId,
    setSelectedAddressId,
    userDetails,
    email
}) {
    
    const [openAddressActions, setOpenAddressActions] = useState(false);
    const [openAddressDeleteDialog, setOpenAddressDeleteDialog] = useState(false);
    const [openAddressUpdateDialog, setOpenAddressUpdateDialog] = useState(false);
    const [selectedId, setSelectedId] = useState(null);
    const [streetAddress, setStreetAddress] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [postCode, setPostCode] = useState("");
    const [country, setCountry] = useState("");
    const [contactNumber, setContactNumber] = useState("");

    const {shippingDetails} = useSelector(state => state.shipping);
    const dispatch = useDispatch();
    
    const useStyles = makeStyles((theme) => ({
        root: {
            minWidth: 275,
            position: "relative",
            height: 205,
            overflow: "auto",
            [theme.breakpoints.down(360)]: {
                minWidth: "100%"
            },
            margin: "auto",
            cursor: "pointer",
        },
        bullet: {
            display: 'inline-block',
            margin: '0 2px',
            transform: 'scale(0.8)',
        },
        title: {
            fontSize: 14,
        },
        pos: {
            marginBottom: 12,
            fontFamily: "Spartan",
            fontSize: 14,
            color: 'black'
        },
        posSub: {
            color: 'rgba(0, 0, 0, 0.64)'
        },
        appBar: {
            position: 'relative',
            backgroundColor: '#f26460',
            paddingRight: "0px !important"
        },
        title: {
            fontSize:"1rem",
            flex: 1,
            fontFamily: "Spartan"
        },
        assignButton: {
            color: "white",
            padding: "3px 10px",
            fontSize: "0.7rem",
            backgroundColor: "#26A69A",
            '&:hover': {
            backgroundColor: "#26A69A"
            }
        },
        checkCircle: {
            color: "green",
            position: "absolute",
            top: 0,
            right: 0
        },
        addressActions: {
            display: "flex",
            flexDirection: "column",
            position: "absolute",
            top: "8.5%",
            opacity: openAddressActions ? 1 : 0,
            left: "39%",
        }
    }));

    const classes = useStyles();

    const onCloseAddressDeleteDialog = () => {

        setOpenAddressDeleteDialog(false);
    }

    const onDeleteShippingDetail = () => {

        dispatch(removeShippingDetail(selectedId));
        setOpenAddressDeleteDialog(false);
    }

    const onOpenAddressUpdateDialog = () => {

        const selectedShippingDetails = shippingDetails.find(shippingDetail => shippingDetail.id === selectedId);
        
        if (selectedShippingDetails) {

            // setAddressId(selectedShippingDetails.id);
            const address = selectedShippingDetails && selectedShippingDetails.address ? selectedShippingDetails.address.split(",") : []
            setStreetAddress(address[0] ? address[0].trim() : "");
            setCity(address[1] ? address[1].trim() : "");
            setState(address[2] ? address[2].trim() : "");
            setCountry(address[3] ? address[3].trim() : "");
            setPostCode(selectedShippingDetails.post_code ? selectedShippingDetails.post_code : "");
            setContactNumber(selectedShippingDetails.phone && selectedShippingDetails.phone !== "0" ? selectedShippingDetails.phone : "");
            setOpenAddressUpdateDialog(true);
        }
    }

    const onUpdateShippingDetail = () => {

        if (
          streetAddress.trim().length === 0 || 
          city.trim().length === 0 ||
          state.trim().length === 0 || 
          postCode.trim().length === 0 ||
          country.trim().length === 0 

        ) {

          dispatch(showSnackbar({message: "Fill out required shipping details!", type: "warning", open: true}))
          return;

        }

        dispatch(updateShippingDetail({
            shippingDetails: {

                address: streetAddress + ", " + city + ", " + state + ", " + country,
                post_code: postCode, 
                email: email ? email : userDetails.email,
                address_id : selectedId && selectedId,
                phone: contactNumber ? contactNumber : 0,
                name: userDetails?.name

            }            
        }))
        setOpenAddressUpdateDialog(false);
    }

    const onCloseAddressUpdateDialog = () => {

        const selectedShippingDetails = shippingDetails.find(shippingDetail => shippingDetail.id === selectedId);

        if (selectedShippingDetails) {

            const address = selectedShippingDetails && selectedShippingDetails.address ? selectedShippingDetails.address.split(",") : []
            setStreetAddress(address[0] ? address[0].trim() : "");
            setCity(address[1] ? address[1].trim() : "");
            setState(address[2] ? address[2].trim() : "");
            setCountry(address[3] ? address[3].trim() : "");
            setPostCode(selectedShippingDetails.post_code ? selectedShippingDetails.post_code : "");
            setContactNumber(selectedShippingDetails.phone && selectedShippingDetails.phone !== "0" ? selectedShippingDetails.phone : "");
        
        }
        setOpenAddressUpdateDialog(false);
    }

    return (
        <Dialog maxWidth="sm" fullWidth open={isOpenAddressSelectionDialog} onClose={onToggleAddressSelectionDialog} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
            <Toolbar style={{paddingLeft:"16px", paddingRight:"0px"}}>
                <Typography variant="h6" className={classes.title}>
                    Choose from Saved Address 
                </Typography>
                <Button style={{marginTop: "5px", marginRight: "8px", fontFamily: "Spartan"}} onClick={() => setSelectedAddressId("")}>
                    Clear
                </Button>          
                <IconButton edge="start" color="inherit" onClick={onToggleAddressSelectionDialog} aria-label="close">
                    <Close />
                </IconButton>  
            </Toolbar>
            </AppBar>
            <List className="row">
            {
                shippingDetails.length > 0 ? shippingDetails.map(shippingDetail => (
                    <ListItem key={shippingDetail.id} style={{paddingLeft: "12px"}} className="col-md-6">
                        <Card onClick={(e) => { e.stopPropagation(); setOpenAddressActions(true); setSelectedId(shippingDetail.id); }} style={{backgroundColor: `${openAddressActions && selectedId === shippingDetail.id ? 'rgba(0,0,0,0.8)' : '#fff'}`}} className={`${classes.root} address-card`}>
                            {
                                selectedAddressId && selectedAddressId === shippingDetail.id &&
                                <CheckCircle
                                    className={classes.checkCircle}
                                />
                            }           
                            {   openAddressActions && selectedId === shippingDetail.id &&
                                <div className={`${classes.addressActions} address-actions`}>
                                    <Tooltip title="Select" placement="left" arrow>
                                        <Fab onClick={(e) => { e.stopPropagation(); setSelectedAddressId(shippingDetail.id); setOpenAddressActions(false); }} style={{marginBottom: "10px", width: "50px", height: "50px"}} color="default" aria-label="edit">
                                            <Check />
                                        </Fab>
                                    </Tooltip>
                                    <Tooltip title="Edit" placement="left" arrow>
                                        <Fab onClick={(e) => { e.stopPropagation(); onOpenAddressUpdateDialog(); }} style={{marginBottom: "10px", width: "50px", height: "50px"}} color="primary" aria-label="edit">
                                            <Edit />
                                        </Fab>
                                    </Tooltip>
                                    <AddressUpdateDialog
                                        openAddressUpdateDialog={openAddressUpdateDialog}
                                        onCloseAddressUpdateDialog={onCloseAddressUpdateDialog}
                                        onUpdateShippingDetail={onUpdateShippingDetail}
                                        setCity={setCity}
                                        setCountry={setCountry}
                                        setStreetAddress={setStreetAddress}
                                        setState={setState}
                                        setPostCode={setPostCode}
                                        city={city}
                                        state={state}
                                        country={country}
                                        postCode={postCode}
                                        streetAddress={streetAddress}
                                        contactNumber={contactNumber}
                                        setContactNumber={setContactNumber}
                                    />
                                    <Tooltip title="Delete" placement="left" arrow>
                                        <Fab onClick={(e) => { e.stopPropagation(); setOpenAddressDeleteDialog(true); }} color="secondary" aria-label="edit">
                                            <Delete />
                                        </Fab>
                                    </Tooltip>
                                    <AddressDeleteDialog
                                        openAddressDeleteDialog={openAddressDeleteDialog}
                                        onCloseAddressDeleteDialog={onCloseAddressDeleteDialog}
                                        onDeleteShippingDetail={onDeleteShippingDetail}
                                    />
                                </div> 
                            }            
                            <CardContent>
                                <Typography className={classes.pos} color="textSecondary">
                                    <span className = {
                                        classes.posSub
                                    }> Address: </span>{shippingDetail.address.split(",")[0] ? shippingDetail.address.split(",")[0].trim() : "-"}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                    <span className={classes.posSub}>Country: </span>{shippingDetail.address.split(",")[3] ? shippingDetail.address.split(",")[3].trim() : "-"}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                    <span className={classes.posSub}>Town/City: </span>{shippingDetail.address.split(",")[1] ? shippingDetail.address.split(",")[1].trim() : "-"}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                    <span className={classes.posSub}>State: </span>{shippingDetail.address.split(",")[2] ? shippingDetail.address.split(",")[2].trim() : "-"}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                <span className={classes.posSub}>PostCode/ZIP: </span>{shippingDetail.post_code}
                                </Typography>
                            </CardContent>                        
                        </Card>
                </ListItem>
                ))   
                :
                <h6 style={{fontFamily: "Spartan"}}>No saved address.</h6>                                  
            }
            </List>
        </Dialog>
    )
}

export default AddressSelectionDialog
