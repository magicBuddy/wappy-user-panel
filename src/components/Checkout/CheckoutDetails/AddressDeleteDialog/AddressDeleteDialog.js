import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import { Cancel } from '@material-ui/icons'
import React from 'react'

function AddressDeleteDialog({
    onCloseAddressDeleteDialog,
    openAddressDeleteDialog,
    onDeleteShippingDetail
}) {
    return (
        <Dialog open={openAddressDeleteDialog} onClose={onCloseAddressDeleteDialog}>
            <DialogTitle>
                <div className="d-flex align-items-center">
                    <Cancel style={{color : "crimson", marginRight: "5px"}}/>
                    <span>Delete Shipping Detail</span>
                </div>
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Do you want to remove this shipping detail? This action cannot be undone.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" className="btn -red" onClick={onCloseAddressDeleteDialog}>Cancel</Button>
                <Button variant="contained" className="btn -dark" onClick={onDeleteShippingDetail}>Yes</Button>
            </DialogActions>
        </Dialog>
    )
}

export default AddressDeleteDialog
