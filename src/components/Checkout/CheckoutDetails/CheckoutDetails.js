import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import axiosInstance from '../../../axios/axios-instance';
import { getListOfCards, onPlaceOrderDetails, sendTokenForStripe, setCurrentPaymentMethod, setOrderCheckoutDetails, setPaymentMethods, setProgressState, setShippingDetails, showSnackbar, updateTotalPriceOfCartItems } from '../../../redux/action/action';
import CheckoutStepper from '../CheckoutStepper/CheckoutStepper';
import StripePaymentForm from '../StripePaymentForm/StripePaymentForm';
import AddressSelectionDialog from './AddressSelectionDialog/AddressSelectionDialog';
import CardSelectionDialog from './CardSelectionDialog/CardSelectionDialog';
import OrderDetails from './OrderDetails/OrderDetails';
import ShippingDetails from './ShippingDetails/ShippingDetails';
import * as actionTypes from '../../../redux/action/action-type/action-type';

function CheckoutDetails({
  paymentMethods,
  shippingDetails
}) {

    const [streetAddress, setStreetAddress] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [postCode, setPostCode] = useState("");
    const [country, setCountry] = useState("");
    const [email, setEmail] = useState("");
    const [contactNumber, setContactNumber] = useState("");
    const [paymentMethodId, setPaymentMethodId] = useState("");
    const [addressId, setAddressId] = useState('');
    const [payUsingStripe, setPayUsingStripe] = useState(false);
    const [showPaymentButton, setShowPaymentButton] = useState(false);
    const [isShippingAddressValid, setIsShippingAddressValid] = useState(false);
    const [isOpenAddressSelectionDialog, setIsOpenAddressSelectionDialog] = useState(false);
    const [selectedAddressId, setSelectedAddressId] = useState("");
    const [isOpenCardSelectionDialog, setIsOpenCardSelectionDialog] = useState(false);
    const [selectedCardId, setSelectedCardId] = useState("");
    const [reShowShippingDetail, setReShowShippingDetail] = useState(false);
    const [cardDetails, setCardDetails] = useState({
      card: '',
      expiry: '',
      cvc: ''
    })
    const [cardValidationError, setCardValidationError] = useState('');
    const [isCardSaved, setIsCardSaved] = useState(false);

    const {userDetails} = useSelector(state => state.auth);
    const {listOfCards, currentShippingDetails} = useSelector(state => state.shipping);
    const {isProgressing} = useSelector(state => state.products);
    const {currentPaymentMethod} = useSelector(state => state.order);
    const {cartProducts} = useSelector(state => state.cart);
        
    const dispatch = useDispatch();

    useEffect(() => {

      return () => {
        dispatch({
          type: actionTypes.SET_CURRENT_SHIPPING_DETAILS,
          payload: null
        });
        dispatch({
          type: actionTypes.SET_CURRENT_PAYMENT_METHOD,
          payload: null
        })
      }
    }, [])
    useEffect(() => {

      if (addressId) {

        const selectedShippingDetails = shippingDetails.find(shippingDetail => shippingDetail.id === selectedAddressId);

        setAddressId(selectedShippingDetails.id);
        const address = selectedShippingDetails && selectedShippingDetails.address ? selectedShippingDetails.address.split(",") : []
        setStreetAddress(address[0] ? address[0].trim() : "");
        setCity(address[1] ? address[1].trim() : "");
        setState(address[2] ? address[2].trim() : "");
        setCountry(address[3] ? address[3].trim() : "");
        setPostCode(selectedShippingDetails.post_code ? selectedShippingDetails.post_code : "");
        setContactNumber(selectedShippingDetails.phone && selectedShippingDetails.phone !== "0" ? selectedShippingDetails.phone : "");

      }

    }, [shippingDetails])

    useEffect(() => {
      
      if(paymentMethodId === '1') dispatch(setCurrentPaymentMethod(paymentMethodId));
      if(paymentMethodId === '2') dispatch(setCurrentPaymentMethod(null));

    }, [paymentMethodId])

    useEffect(() => {
      
      if (selectedAddressId === "") {

        setStreetAddress("");
        setCity("");
        setState("");
        setCountry("");
        setPostCode("");
        setContactNumber("");
        setAddressId("");

      }

      if (selectedAddressId > 0) {

        const selectedShippingDetails = shippingDetails.find(shippingDetail => shippingDetail.id === selectedAddressId);
        
        if (selectedShippingDetails) {

          setAddressId(selectedShippingDetails.id);
          const address = selectedShippingDetails && selectedShippingDetails.address ? selectedShippingDetails.address.split(",") : []
          setStreetAddress(address[0] ? address[0].trim() : "");
          setCity(address[1] ? address[1].trim() : "");
          setState(address[2] ? address[2].trim() : "");
          setCountry(address[3] ? address[3].trim() : "");
          setPostCode(selectedShippingDetails.post_code ? selectedShippingDetails.post_code : "");
          setContactNumber(selectedShippingDetails.phone && selectedShippingDetails.phone !== "0" ? selectedShippingDetails.phone : "");

        }
      }

    }, [selectedAddressId])

    useEffect(() => {

      if (selectedCardId) {

        const selectedCardDetails = listOfCards.find(cardDetail => cardDetail.id === selectedCardId);
        setCardDetails({
          card: selectedCardDetails.card,          
          cvc: selectedCardDetails.cvv,
          expiry: selectedCardDetails.month + " / " +selectedCardDetails.year,
        })

      }

      else setCardDetails({card: '', cvc: '', expiry: ''});

    }, [selectedCardId])

    useEffect(() => {

      if (
          streetAddress.trim().length === 0 || 
          city.trim().length === 0 ||
          state.trim().length === 0 || 
          postCode.trim().length === 0 ||
          country.trim().length === 0 

        ) {

          setShowPaymentButton(false);
          setIsShippingAddressValid(false);
          return;

        }

      else if (email) {

        const regexForEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!regexForEmail.test(String(email).toLowerCase())) {

            setShowPaymentButton(false);
            setIsShippingAddressValid(false);
            return;
            
        }

      }

      else {

        setShowPaymentButton(true);
        setIsShippingAddressValid(true);

      }

    }, [streetAddress, city, state, postCode, country, email])

    const onSubmittingShippingDetails = () => {

      if (
          streetAddress.trim().length === 0 || 
          city.trim().length === 0 ||
          state.trim().length === 0 || 
          postCode.trim().length === 0 ||
          country.trim().length === 0 

        ) {

          dispatch(showSnackbar({message: "Fill out required shipping details!", type: "warning", open: true}))
          return;

        }

      if (email) {

        const regexForEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!regexForEmail.test(String(email).toLowerCase())) {

            dispatch(showSnackbar({message: "Enter valid Email Address!", type: "error", open: true}))
            return;
            
        }

      }

      dispatch(setShippingDetails({

        shippingDetails: {
          
          address: streetAddress + ", " + city + ", " + state + ", " + country,
          post_code: postCode, 
          email: email ? email : userDetails.email,
          address_id : addressId && addressId,
          phone: contactNumber ? contactNumber : 0,
          name: userDetails?.name

        }
        
      }))

    }

    const onSubmitPlaceOrder = () => {

      if (paymentMethodId === '1') dispatch(onPlaceOrderDetails(paymentMethodId))

    }

    const onPlaceOrderUsingDebitCard = () => {

      if (paymentMethodId === '2') dispatch(onPlaceOrderDetails(paymentMethodId));
    
    }

    const onHandleStripeToken = (token) => {

      dispatch(setOrderCheckoutDetails({

        shippingDetails: {
          
          address: streetAddress + ", " + city + ", " + state + ", " + country,
          post_code: postCode, 
          email: email ? email : userDetails.email,
          address_id : addressId && addressId,
          phone: contactNumber ? contactNumber : 0,
          name: userDetails?.name
        },
        
        paymentMethodId,
        isPaymentUsingStripe: true,
        stripeToken: token?.id

      }))
        
    }

    const onToggleAddressSelectionDialog = (e) => {
      e.preventDefault();
      setIsOpenAddressSelectionDialog(isOpenAddressSelectionDialog => !isOpenAddressSelectionDialog)
    
    }
    
    const onSelectCardHandler = () => {

      onToggleCardSelectionDialog();

    }

    const onToggleCardSelectionDialog = () => {
      
      setIsOpenCardSelectionDialog(isOpenCardSelectionDialog => !isOpenCardSelectionDialog);

    }

    const changeCardDetailsHandler = ({target}) => {
      
      setCardDetails({...cardDetails, [target.name]: target.value});

    }

    const isCardDetailsValid = () => {

      for (let detail of Object.values(cardDetails)) {

        if (!detail.trim()) {

          dispatch(showSnackbar({message: "Card number, Expiry Date or CVC field cannot be empty!", type: "warning", open: true}))
          return;

        }
      }

      return true;

    }

    const handleFormSubmit = async (e) => {

      e.preventDefault();

      if (isCardDetailsValid()) {

        dispatch(setProgressState(true));

        const formData = new FormData();
        
        for (let key of Object.keys(cardDetails)) {

          if (key === "expiry") {

            formData.append("month", cardDetails[key].split(" / ")[0])
            formData.append("year", cardDetails[key].split(" / ")[1])
            continue;

          }

          if (key === "cvc") {

            formData.append("cvv", cardDetails[key]);
            continue;
            
          }

          formData.append(key, cardDetails[key])

        }
        
        
        const address_id = currentShippingDetails.address_id;
        const cart_id = cartProducts[0].cartId;                
        
        formData.append('cart_id', cart_id);
        formData.append('address_id', address_id);
        formData.append('payment_method_id', paymentMethodId);
        
        const response = await axiosInstance.post('/stripePayment', formData, {
          headers : {
            Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
          }
        })
        .catch(err => {

          if (err.response && err.response.data.code === 500) {

            setCardValidationError(err.response.data.message);
            dispatch(setProgressState(false));

          }

        })

        if (response && response.data.code === 200) {

          setCardValidationError('');

          if (isCardSaved) {

            formData.delete('cart_id');
            formData.delete('address_id');
            formData.delete('payment_method_id');
            
            const saveCardDetailsResponse = await axiosInstance.post('/card', formData, {
              headers: {
                Authorization: 'Bearer ' +localStorage.getItem("access_token_wappy")
              }
            })
            .catch(err => console.log(err))

            if (saveCardDetailsResponse && saveCardDetailsResponse.data.code === 200) {
            
              // onPlaceOrderUsingDebitCard(response.data.data?.id);
              onPaymentSuccessful();
            }
          } 

          else {

            onPaymentSuccessful();

          }
        } 
      }
    }

    const onPaymentSuccessful = () => {
      
      dispatch({
        type: actionTypes.CLEAR_CART,
        payload: []
      })
      
      dispatch(setProgressState(false));
      dispatch(updateTotalPriceOfCartItems(0));
      dispatch(showSnackbar({message: "Order Placed Successfully", type: "success", open: "true"}));                
      
    }

    return (
        <div className="container">
          <div className="checkout">
            <div className="container">
              <div className="row">
                <div className="col-12 mb-2">
                  <CheckoutStepper
                    isProgressing={isProgressing}
                    setPaymentMethodId={setPaymentMethodId}
                    currentShippingDetails={currentShippingDetails}
                    currentPaymentMethod={currentPaymentMethod}
                    setReShowShippingDetail={setReShowShippingDetail}
                    reShowShippingDetail={reShowShippingDetail}
                  />
                </div>
                {
                  !currentShippingDetails && !currentPaymentMethod ?
                    <div className="col-12 col-lg-8">
                      <form action="">
                        <div className="checkout__form">  
                          <div className="checkout__form__contact">
                            { shippingDetails.length > 0 &&
                              <div className="text-right checkout__form__address">
                                <button onClick={onToggleAddressSelectionDialog} className="btn -red">Choose Address</button>
                                <AddressSelectionDialog
                                  onToggleAddressSelectionDialog={onToggleAddressSelectionDialog}
                                  isOpenAddressSelectionDialog={isOpenAddressSelectionDialog}
                                  selectedAddressId={selectedAddressId}
                                  userDetails={userDetails}
                                  email={email}
                                  setSelectedAddressId={setSelectedAddressId}
                                />          
                              </div>
                            }
                            <div className="checkout__form__contact__title">
                              <h5 className="checkout-title">Contact Information</h5>
                            </div>
                            <div className="input-validator">
                              <div className="input-validator">
                                  <label className="mt-3">Name
                                    <input readOnly={true}  value={userDetails.name} type="name" name="name" placeholder="Name"/>
                                  </label>
                              </div>
                              <div className="input-validator">
                                  <label className="mt-3">Contact Number
                                    <input value={contactNumber} onChange={(e) => setContactNumber(e.target.value)} type="text" name="contact"/>
                                  </label>
                              </div>
                              <div className="input-validator">
                                  <label className="mt-3">Email
                                    <input readOnly={true}  value={userDetails.email} type="email" name="email" placeholder="Email"/>
                                  </label>
                              </div>
                            </div>
                          </div>
                          <ShippingDetails 
                            streetAddress={streetAddress}
                            city={city}
                            state={state}
                            postCode={postCode}
                            country={country}
                            setStreetAddress={setStreetAddress}
                            setCity={setCity}
                            setState={setState}
                            setPostCode={setPostCode}
                            setCountry={setCountry}
                          />
                        </div>
                      </form>
                    </div>

                    : currentShippingDetails && (!currentPaymentMethod || currentPaymentMethod === '1') ?

                    <div className="col-12 col-lg-8">
                      <form action="">
                        <div className="checkout__form">  
                          <div className="checkout__form__contact">
                            <div className="checkout__form__contact__title">
                              <h5 className="checkout-title">Payment Method</h5>
                            </div>
                            <div class="checkout__total__price__payment">
                              {
                                paymentMethods.length > 0 && paymentMethods.map(method => (
                                    <label key={method.id} className="checkbox-label" htmlFor={method.title}>
                                        <input id={method.title} checked={paymentMethodId == method.id} value={method.id} type="radio" onChange={(e) => { setPaymentMethodId(e.target.value); }} name="paymentMethod"/>
                                        {method.title}
                                    </label>
                                ))
                              }
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                    : 
                    currentShippingDetails && currentPaymentMethod === '2' ?                      
                      <div className="col-12 col-lg-8">
                          <div className="checkout__form">  
                            <div className="checkout__form__contact">
                              { listOfCards.length > 0 &&
                                <div className="text-right checkout__form__address">
                                  <button onClick={onToggleCardSelectionDialog} className="btn -red">Choose Card</button>
                                  <CardSelectionDialog
                                    onToggleCardSelectionDialog={onToggleCardSelectionDialog}
                                    isOpenCardSelectionDialog={isOpenCardSelectionDialog}
                                    selectedCardId={selectedCardId}
                                    setSelectedCardId={setSelectedCardId}
                                  />          
                                </div>
                              }
                              <div className="checkout__form__contact__title">
                                <h5 className="checkout-title">Debit / Credit Card</h5>
                              </div>
                              <div className="checkout__total__price__payment">
                                  <StripePaymentForm
                                    cardDetails={cardDetails}
                                    cardValidationError={cardValidationError}
                                    changeCardDetailsHandler={changeCardDetailsHandler}
                                    handleFormSubmit={handleFormSubmit}
                                    setIsCardSaved={setIsCardSaved}
                                    isCardSaved={isCardSaved}
                                  />
                              </div>
                            </div>
                          </div>
                      </div>
                  : null
                }

                <OrderDetails 
                  paymentMethods={paymentMethods}
                  onSubmittingShippingDetails={onSubmittingShippingDetails}
                  setPaymentMethodId={setPaymentMethodId}
                  payUsingStripe={payUsingStripe}
                  onHandleStripeToken={onHandleStripeToken}
                  setPayUsingStripe={setPayUsingStripe}
                  showPaymentButton={showPaymentButton}
                  onSelectCardHandler={onSelectCardHandler}
                  isShippingAddressValid={isShippingAddressValid}
                  paymentMethodId={paymentMethodId}
                  onSubmitPlaceOrder={onSubmitPlaceOrder}
                  currentPaymentMethod={currentPaymentMethod}
                  currentShippingDetails={currentShippingDetails}
                />
                
                {/* <CardSelectionDialog
                  setIsOpenCardSelectionDialog={setIsOpenCardSelectionDialog}
                  isOpenCardSelectionDialog={isOpenCardSelectionDialog}
                  cardDetails={listOfCards}
                  setSelectedCardId={setSelectedCardId}
                  selectedCardId={selectedCardId}
                  onToggleCardSelectionDialog={onToggleCardSelectionDialog}
                  onHandleStripeToken={onHandleStripeToken}
                /> */}

              </div>
            </div>
          </div>
        </div>
    )
}

export default CheckoutDetails
