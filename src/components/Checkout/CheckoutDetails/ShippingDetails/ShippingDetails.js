import React, { useState } from 'react'

function ShippingDetails({
    country,
    city,
    streetAddress,
    state,
    postCode,
    setStreetAddress,
    setState,
    setCity,
    setCountry,
    setPostCode
}) {

    return (
        <div className="checkout__form__shipping">
            <h5 className="checkout-title">Shipping Address</h5>
            <div className="row">
                <div className="col-12">
                    <div className="input-validator">
                        <label>Address <span>*</span>
                        <input type="text" required value = {streetAddress} onChange={(e) => setStreetAddress(e.target.value)} name="streetAddress" placeholder="Street Address"/>
                        </label>
                    </div>
                </div>
                <div className="col-12">
                    <div className="input-validator">
                        <label>Country <span>*</span>
                            <input required value = {country} onChange={(e) => setCountry(e.target.value)} type="text" name="country"/>
                        </label>
                    </div>
                </div>
                <div className="col-12">
                    <div className="input-validator">
                        <label>Town/City <span>*</span>
                            <input required value = {city} onChange={(e) => setCity(e.target.value)} type="text" name="town"/>
                        </label>
                    </div>
                </div>
                <div className="col-12">
                    <div className="input-validator">
                        <label>State <span>*</span>
                            <input required value = {state} onChange={(e) => setState(e.target.value)} type="text" name="state"/>
                        </label>
                    </div>
                </div>
                <div className="col-12">
                    <div className="input-validator">
                        <label>Postcode/ZIP <span>*</span>
                            <input required value = {postCode} onChange={(e) => setPostCode(e.target.value)} type="text" name="zip"/>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ShippingDetails
