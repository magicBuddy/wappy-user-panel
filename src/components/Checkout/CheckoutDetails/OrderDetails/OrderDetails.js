import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import StripeCheckout from 'react-stripe-checkout';
import { setCurrentPaymentMethod } from '../../../../redux/action/action';

function OrderDetails({
    onSubmittingShippingDetails,
    paymentMethodId,
    onSubmitPlaceOrder,
    paymentMethods,
    setPaymentMethodId,
    payUsingStripe,
    onHandleStripeToken,
    setPayUsingStripe,
    showPaymentButton,
    onSelectCardHandler,
    isShippingAddressValid,
    currentPaymentMethod,
    currentShippingDetails
}) {

    const {cartProducts, totalCartPrice} = useSelector(state => state.cart);
    const dispatch = useDispatch();
    
    return (
        <div class="col-12 col-lg-4">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-12 ml-auto">
                    <div class="checkout__total">
                    <h5 class="checkout-title">Your order</h5>
                    
                    <div class="checkout__total__price">
                        <h5>{cartProducts.length > 1 ? "Products" : "Product"}</h5>
                        <table>
                        <colgroup>
                            <col style={{width: "70%"}}/>
                            <col style={{width: "30%"}}/>
                        </colgroup>
                        <tbody>
                            {cartProducts.map(product => (
                                <tr key={product.productId}>                            
                                    <td><span>{product.productQuantity < 10 ? `0${product.productQuantity}` : product.productQuantity} x </span>{product.productName}</td>
                                    <td>${(product.productQuantity * Number(product.productPrice)).toFixed(2)}</td>
                                </tr>
                            ))}
                        </tbody>
                        </table>
                        <div class="checkout__total__price__total-count">
                        <table>
                            <tbody>
                            <tr>
                                <td>Subtotal</td>
                                <td>${totalCartPrice}</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>${totalCartPrice}</td>
                            </tr>
                            </tbody>
                        </table>
                        </div>

                        {/* <div class="checkout__total__price__payment">
                        <h5>Payment Method</h5>
                        {
                            paymentMethods.length > 0 && paymentMethods.slice(0,1).map(method => (
                                <label key={method.id} className="checkbox-label" htmlFor={method.title}>
                                    <input id={method.title} value={method.id} type="radio" onChange={(e) => { setPayUsingStripe(false); setPaymentMethodId(e.target.value); }} name="paymentMethod"/>
                                    {method.title}
                                </label>
                            ))
                        }
                            <label className="checkbox-label" htmlFor="stripe">
                                <input id="stripe" value={2} type="radio" onChange={(e) => { setPayUsingStripe(true); setPaymentMethodId(e.target.value); }} name="paymentMethod"/>
                                Stripe
                            </label>
                        </div> */}
                    </div>
                        {/* {
                            showPaymentButton && 
                            !payUsingStripe 
                            ?
                                <button onClick={onSubmittingShippingDetails} class="btn -red">Place order</button>
                            :

                            showPaymentButton && payUsingStripe ?
                            
                            <StripeCheckout 
                                className="stripe-checkout-btn btn -red"
                                stripeKey = {process.env.REACT_APP_STRIPE_KEY}
                                token={onHandleStripeToken} 
                                
                                amount={totalCartPrice * 100} 
                            />    

                            : null
                        } */}
                    </div>
                </div>
            </div>
            {
                !currentShippingDetails && !paymentMethodId ?
                <div className="text-right">
                    <button onClick={onSubmittingShippingDetails} className=" mt-4 text-right btn -dark">Continue</button>
                </div>
                :
                currentShippingDetails && paymentMethodId === '1' ?
                <div className="text-right">
                    <button onClick={onSubmitPlaceOrder} className=" mt-4 text-right btn -red">Place Order</button>
                </div>
                : 
                paymentMethodId === '2' && currentShippingDetails && currentPaymentMethod === null ?
                <div className="text-right">
                    <button onClick={() => dispatch(setCurrentPaymentMethod(paymentMethodId))} className=" mt-4 text-right btn -dark">Continue</button>
                </div>
                : null 
            }
        </div>
    )
}

export default OrderDetails
