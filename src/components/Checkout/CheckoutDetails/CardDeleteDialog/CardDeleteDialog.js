import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import { Cancel } from '@material-ui/icons'
import React from 'react'

function CardDeleteDialog({
    onCloseCardDeleteDialog,
    openCardDeleteDialog,
    onDeleteCardDetail
}) {
    return (
        <Dialog open={openCardDeleteDialog} onClose={onCloseCardDeleteDialog}>
            <DialogTitle>
                <div className="d-flex align-items-center">
                    <Cancel style={{color : "crimson", marginRight: "5px"}}/>
                    <span>Delete Card</span>
                </div>
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Do you want to remove this Card? This action cannot be undone.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" className="btn -red" onClick={onCloseCardDeleteDialog}>Cancel</Button>
                <Button variant="contained" className="btn -dark" onClick={onDeleteCardDetail}>Yes</Button>
            </DialogActions>
        </Dialog>
    )
}

export default CardDeleteDialog
