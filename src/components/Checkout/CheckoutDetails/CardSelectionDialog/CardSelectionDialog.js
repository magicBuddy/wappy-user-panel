import React, { useState } from 'react'
import { AppBar, Button, Dialog, Divider, IconButton, List, ListItem, ListItemText, makeStyles, Slide, Toolbar, Typography, Collapse, Tooltip, Fab } from '@material-ui/core';
import { Check, CheckCircle, Close, Delete, KeyboardArrowDown } from '@material-ui/icons';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import StripeCheckout from 'react-stripe-checkout';
import CardDeleteDialog from '../CardDeleteDialog/CardDeleteDialog';
import { removeCardDetail, removeShippingDetail } from '../../../../redux/action/action';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});


function CardSelectionDialog({

    onToggleCardSelectionDialog,
    isOpenCardSelectionDialog,
    selectedCardId,
    setSelectedCardId

}) {
    
    const {listOfCards} = useSelector(state => state.shipping);

    const [openCardActions, setOpenCardActions] = useState(false);
    const [openCardDeleteDialog, setOpenCardDeleteDialog] = useState(false);
    const [selectedId, setSelectedId] = useState(null);
    const dispatch = useDispatch();

    const useStyles = makeStyles((theme) => ({
        root: {
            minWidth: 275,
            position: "relative",
            height: 205,
            overflow: "auto",
            [theme.breakpoints.down(360)]: {
                minWidth: "100%"
            },
            margin: "auto",
            cursor: "pointer",
        },
        bullet: {
            display: 'inline-block',
            margin: '0 2px',
            transform: 'scale(0.8)',
        },
        title: {
            fontSize: 14,
        },
        pos: {
            marginBottom: 12,
            fontFamily: "Spartan",
            fontSize: 14,
            color: 'black'
        },
        posSub: {
            color: 'rgba(0, 0, 0, 0.64)'
        },
        appBar: {
            position: 'relative',
            backgroundColor: '#f26460',
            paddingRight: "0px !important"
        },
        title: {
            fontSize:"1rem",
            flex: 1,
            fontFamily: "Spartan"
        },
        assignButton: {
            color: "white",
            padding: "3px 10px",
            fontSize: "0.7rem",
            backgroundColor: "#26A69A",
            '&:hover': {
            backgroundColor: "#26A69A"
            }
        },
        checkCircle: {
            color: "green",
            position: "absolute",
            top: 0,
            right: 0
        },
        cardActions: {
            display: "flex",
            flexDirection: "column",
            position: "absolute",
            top: "20.5%",
            opacity: openCardActions ? 1 : 0,
            left: "39%",
        }
    }));

    const onCloseCardDeleteDialog = () => {

        setOpenCardDeleteDialog(false);
    }

    const onDeleteCardDetail = () => {

        dispatch(removeCardDetail(selectedId));
        setOpenCardDeleteDialog(false);
        
    }

    const classes = useStyles();

    return (
        <Dialog maxWidth="sm" fullWidth open={isOpenCardSelectionDialog} onClose={onToggleCardSelectionDialog} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
            <Toolbar style={{paddingLeft:"16px", paddingRight:"0px"}}>
                <Typography variant="h6" className={classes.title}>
                    Choose from Saved Cards 
                </Typography>
                {   listOfCards.length > 0 && selectedCardId !== "" ?
                    <Button style={{marginTop: "5px", marginRight: "8px", fontFamily: "Spartan"}} onClick={() => setSelectedCardId("")}>
                        Clear
                    </Button>
                    : null
                }          
                <IconButton edge="start" color="inherit" onClick={onToggleCardSelectionDialog} aria-label="close">
                    <Close />
                </IconButton>  
            </Toolbar>
            </AppBar>
            <List className="row">
            {
                listOfCards.length > 0 ? listOfCards.map(cardDetail => (
                    <ListItem key={cardDetail.id} style={{paddingLeft: "12px"}} className="col-md-6">
                        <Card onClick={(e) => { e.stopPropagation(); setOpenCardActions(true); setSelectedId(cardDetail.id); }} style={{backgroundColor: `${openCardActions && selectedId === cardDetail.id ? 'rgba(0,0,0,0.8)' : '#fff'}`}} className={`${classes.root} address-card`}>
                            {
                                selectedCardId && selectedCardId === cardDetail.id &&
                                <CheckCircle
                                    className={classes.checkCircle}
                                />
                            }           
                            {   openCardActions && selectedId === cardDetail.id &&
                                <div className={`${classes.cardActions} address-actions`}>
                                    <Tooltip title="Select" placement="left" arrow>
                                        <Fab onClick={(e) => { e.stopPropagation(); setSelectedCardId(cardDetail.id); setOpenCardActions(false); }} style={{marginBottom: "10px", width: "50px", height: "50px"}} color="default" aria-label="edit">
                                            <Check />
                                        </Fab>
                                    </Tooltip>
                                    <Tooltip title="Delete" placement="left" arrow>
                                        <Fab onClick={(e) => { e.stopPropagation(); setOpenCardDeleteDialog(true); }} color="secondary" aria-label="edit">
                                            <Delete />
                                        </Fab>
                                    </Tooltip>
                                    <CardDeleteDialog
                                        openCardDeleteDialog={openCardDeleteDialog}
                                        onCloseCardDeleteDialog={onCloseCardDeleteDialog}
                                        onDeleteCardDetail={onDeleteCardDetail}
                                    />
                                </div> 
                            }            
                            <CardContent>
                                <Typography className={classes.pos} color="textSecondary">
                                    <span className={classes.posSub}>Card Number: </span>{cardDetail.card ? cardDetail.card : "-"}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                    <span className={classes.posSub}>Expiry Month: </span>{cardDetail.month ? cardDetail.month : "-"}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                    <span className={classes.posSub}>Expiry Year: </span>{cardDetail.year ? cardDetail.year : "-"}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                <span className={classes.posSub}>CVC Number: </span>{cardDetail.cvv ? cardDetail.cvv : "-"}
                                </Typography>
                            </CardContent>                        
                        </Card>
                </ListItem>
                ))   
                :
                <h6 style={{fontFamily: "Spartan"}}>No saved cards.</h6>                 
            }
            </List>
        </Dialog>
    )
}

export default CardSelectionDialog
