import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ArrowBack, ArrowBackIos } from '@material-ui/icons';
import { StepLabel, Tooltip } from '@material-ui/core';
import { SET_CURRENT_PAYMENT_METHOD, SET_CURRENT_SHIPPING_DETAILS } from '../../../redux/action/action-type/action-type';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  completed: {
    display: 'inline-block',
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ['Shipping Address', 'Payment method', 'Place Order'];
}

export default function CheckoutStepper({
    isProgressing,
    currentShippingDetails,
    currentPaymentMethod,
    reShowShippingDetail,
    setReShowShippingDetail,
    setPaymentMethodId
}) {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const [completed, setCompleted] = React.useState({});

    const steps = getSteps();
    const dispatch = useDispatch();

    useEffect(() => {
    
        if (currentShippingDetails && !currentPaymentMethod) {
            const newCompleted = {};
            setActiveStep(0);
            newCompleted[0] = true;
            setCompleted(newCompleted);
            handleNext();
        }

    }, [currentShippingDetails])

    useEffect(() => {

        if (currentShippingDetails && currentPaymentMethod) {
            const newCompleted = {0: true};
            setActiveStep(1);
            newCompleted[1] = true;
            setCompleted(newCompleted);
            handleNext();
        }

        if (currentShippingDetails && currentPaymentMethod === null) {
            setActiveStep(1);
            setCompleted({0 : true});
        }

    }, [currentPaymentMethod, activeStep])


    const totalSteps = () => {
        return steps.length;
    };

    const completedSteps = () => {
        return Object.keys(completed).length;
    };

    const isLastStep = () => {
        return activeStep === totalSteps() - 1;
    };

    const allStepsCompleted = () => {
        return completedSteps() === totalSteps();
    };

    const handleNext = () => {
        const newActiveStep =
        isLastStep() && !allStepsCompleted()
            ? // It's the last step, but not all steps have been completed,
            // find the first step that has been completed
            steps.findIndex((step, i) => !(i in completed))
            : activeStep + 1;
        setActiveStep(newActiveStep);
    };  

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
        if (Object.keys(completed).length === 1) {
            setCompleted({});
            setPaymentMethodId("");
            dispatch({
                type: SET_CURRENT_SHIPPING_DETAILS,
                payload: null
            })
        }
        else if (Object.keys(completed).length === 2) {

            if (currentPaymentMethod === '1') {
                setCompleted({});
                setPaymentMethodId("");
                setActiveStep(0);
                dispatch({
                    type: SET_CURRENT_PAYMENT_METHOD,
                    payload: null
                })
                dispatch({
                    type: SET_CURRENT_SHIPPING_DETAILS,
                    payload: null
                })
            }

            else if (currentPaymentMethod === '2') {
                setCompleted({0: true});
                setActiveStep(1);
                dispatch({
                    type: SET_CURRENT_PAYMENT_METHOD,
                    payload: null
                })
            }
        }
    };

    const handleStep = (step) => () => {
        setActiveStep(step);
    };

    const handleComplete = () => {
        const newCompleted = completed;
        newCompleted[activeStep] = true;
        setCompleted(newCompleted);
        handleNext();
    };

    const handleReset = () => {
        setActiveStep(0);
        setCompleted({});
    };

    return (
        <div className={classes.root}>
            <Stepper nonLinear activeStep={activeStep}>
                {steps.map((label, index) => (
                <Step key={label}>
                    <StepLabel completed={completed[index]}>
                    {label}
                    </StepLabel>
                </Step>
                ))}
            </Stepper>
            {
                Object.keys(completed).length > 0 &&  
                
                <Tooltip arrow placement="right" title="Go Back">
                    <ArrowBack style={{cursor: "pointer"}} className="mt-4" onClick={handleBack}/>
                </Tooltip>
            }
        </div>
    );
}