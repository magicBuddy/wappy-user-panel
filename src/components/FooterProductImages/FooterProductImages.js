import React from 'react'
import Insta1 from '../../assets/images/instagram/InstagramTwo/1.png';
import Insta2 from '../../assets/images/instagram/InstagramTwo/2.png';
import Insta3 from '../../assets/images/instagram/InstagramTwo/3.png';
import Insta4 from '../../assets/images/instagram/InstagramTwo/4.png';
import Insta5 from '../../assets/images/instagram/InstagramTwo/5.png';
import Insta6 from '../../assets/images/instagram/InstagramTwo/6.png';

function FooterProductImages() {
    return (
        <div class="instagram-two">
            <div class="instagram-two-slider">
                <a class="slider-item" href="https://www.instagram.com/">
                    <img src={Insta1} alt="Instagram img"/>
                </a>
                <a class="slider-item" href="https://www.instagram.com/">
                    <img src={Insta2} alt="Instagram img"/>
                </a>
                <a class="slider-item" href="https://www.instagram.com/">
                    <img src={Insta3} alt="Instagram img"/>
                </a>
                <a class="slider-item" href="https://www.instagram.com/">
                    <img src={Insta4} alt="Instagram img"/>
                </a>
                <a class="slider-item" href="https://www.instagram.com/">
                    <img src={Insta5} alt="Instagram img"/>
                </a>
                <a class="slider-item" href="https://www.instagram.com/">
                    <img src={Insta6} alt="Instagram img"/>
                </a>
            </div>
        </div>
    )
}

export default FooterProductImages
