import React from 'react'
import { useSelector } from 'react-redux';

function FilterBar({
    setView,
    view
}) {

    const {activeCategory, pageLimit} = useSelector(state => state.products);

    return (
        <div className="shop-header">
            <div className="shop-header__view">
                <div className="shop-header__view__icon">
                    <a className={`${view==="Grid" ? 'active' : null}`} onClick={() => setView("Grid")}>
                        <i className="fas fa-th"></i>
                    </a>
                    <a className={`${view==="List" ? 'active' : null}`} onClick={() => setView("List")}>
                        <i className="fas fa-bars"></i>
                    </a>
                </div>
                <h5 className="shop-header__page">{!activeCategory && "All Products"} {activeCategory && `Products`}</h5>
            </div>
            {/* <select className="customed-select">
                <option value="az">A to Z</option>
                <option value="za">Z to A</option>
                <option value="low-high">Low to High Price</option>
                <option value="high-low">High to Low Price</option>
            </select> */}
        </div>
    )
}

export default FilterBar
