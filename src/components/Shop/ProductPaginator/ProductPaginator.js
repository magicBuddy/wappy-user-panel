import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import axiosInstance from '../../../axios/axios-instance';
import { getProductList, setActiveCategory, setActivePage, setLoadingState, setPaginationLinks, setProductList } from '../../../redux/action/action';

function ProductPaginator() {

    const dispatch = useDispatch();
    const {paginationLinks, activeCategory} = useSelector(state => state.products);

    const getProductListByKeyword = async (limit, page, title) => {

        dispatch(setLoadingState(true));
        const response = await axiosInstance.get('/getProductByKeyword', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            },
            params: {
                limit,
                search_text: title.replace(/%20/g, " "),
                page
            }
        }).catch(err => console.log(err))

        if (response && response.data && response.data.code === 200) {
            dispatch(setProductList(response.data.data && response.data.data.data));
            dispatch(setPaginationLinks(response.data.data && response.data.data.links));
            dispatch(setLoadingState(false));
        }
    }
    
    return (
        <ul className="paginator">
            
            {paginationLinks.map((link,index) => (

                <li className={`${ link.active && 'active' } page-item`}>
                    {   !isNaN(link.label) ? 
                            <button 
                                onClick={
                                    () => {
                                        if(link.active) return null;
                                        else if(activeCategory) getProductListByKeyword(12, Number(link.label), link.url.split("search_text=")[1].split("&")[0])
                                        else dispatch(getProductList(12, Number(link.label)))
                                    }
                                } 
                                className="page-link"
                            >
                                {link.label}
                            </button> :
                        
                        link.label.includes("Next") ? 
                            <button 
                                onClick={
                                    () => {
                                        if (link.url && activeCategory) dispatch(getProductListByKeyword(12, Number(link.url.split('=')[link.url.split('=').length - 1]), link.url.split("search_text=")[1].split("&")[0]))
                                        else if(link.url) dispatch(getProductList(12, Number(link.url.split('=')[link.url.split('=').length - 1])))
                                    }
                                } 
                                className="page-link"
                            >
                                <i className="far fa-angle-right"></i>
                            </button> :

                        link.label.includes("Previous") ? 
                            <button 
                                onClick={
                                    () => {
                                        if (link.url && activeCategory) dispatch(getProductListByKeyword(12, Number(link.url.split('=')[link.url.split('=').length - 1]), link.url.split("search_text=")[1].split("&")[0]))
                                        else if (link.url) dispatch(getProductList(12, Number(link.url.split('=')[link.url.split('=').length - 1])))
                                    }
                                } 
                                className="page-link"
                            >
                                <i className="far fa-angle-left"></i>
                            </button> :

                        link.label.includes("...") ? 
                            <button className="page-link">{link.label}</button> : 
                        
                        null
                    }
                </li>
      
            ))}

        </ul>
    )
}

export default ProductPaginator
