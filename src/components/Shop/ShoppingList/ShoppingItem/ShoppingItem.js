import { Chip, Tooltip } from '@material-ui/core';
import { Done, Star } from '@material-ui/icons';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import StaticProductImg from '../../../../assets/images/product/1.jpg';
import { addItemToCart, addItemToWishlist } from '../../../../redux/action/action';

function ShoppingItem({
    arrivalType,
    productImg,
    productCategory,
    productName,
    productPrice,
    productDescription,
    productKeywords,
    discountPrice,
    discountPercent,
    productTotalRating,
    productAverageRating,
    view,
    productId
}) {

    const dispatch = useDispatch();
    const history = useHistory();

    const {cartProducts} = useSelector(state => state.cart);
    const {userDetails} = useSelector(state => state.auth);

    const onAddItemToWishlistHandler = () => {

        if (Object.keys(userDetails).length === 0) return history.push('/login');
        
        dispatch(addItemToWishlist(productId));

    }

    const onAddItemToCartHandler = () => {

        if (Object.keys(userDetails).length === 0) return history.push('/login');
        
        const itemDetails = {productId, productQuantity: 1, productPrice, productName, productImg};
        const itemIndex = cartProducts.findIndex(product => product.productId === productId);
        
        if (itemIndex >= 0) {

            itemDetails.productQuantity = cartProducts[itemIndex].productQuantity + 1;
            dispatch(addItemToCart(itemDetails, itemIndex));

        }
        
        else dispatch(addItemToCart(itemDetails));

    }

    return (
        
        view === "Grid" ?

        (<div class="col-12 col-sm-6 col-lg-4">
            <div class="product ">
                <div class="product-type">
                    {
                        arrivalType 
                        ?
                            <h5 className="-new">{arrivalType}</h5>
                        :
                        discountPercent
                        ?
                            <h5 className="-sale">-{discountPercent}</h5>
                        :
                            null
                    }
                </div>
                <div class="product-thumb"><Link class="product-thumb__image shop-list-thumb__image" to={`/product-details/${productId}`}><img src={productImg ? productImg : StaticProductImg} alt="Product img"/><img src={productImg ? productImg : StaticProductImg} alt="Product img"/></Link>
                    <div class="product-thumb__actions">
                        <div class="product-btn">
                            <button data-toggle="tooltip" title="Add to Cart" class="btn -white product__actions__item -round product-atc" onClick={onAddItemToCartHandler}><i class="fas fa-shopping-bag"></i></button>
                        </div>
                        <div class="product-btn">
                            <button data-toggle="tooltip" title="Add to Wishlist" class="btn -white product__actions__item -round" onClick={onAddItemToWishlistHandler}><i class="fas fa-heart"></i></button>
                        </div>
                    </div>
                </div>
                <div class="product-content">
                    <div class="product-content__header">
                    <div class="product-category">{productCategory}</div>
                        
                    </div><Link class="product-name" to={`/product-details/${productId}`}>{productName}</Link>
                    <div class="justify-content-between product-content__footer">
                        { discountPrice ? 
                            <React.Fragment>
                                <h5 className="product-price--main">${discountPrice}</h5>
                                <h5 className="product-price--discount mr-2">${productPrice}</h5>            
                            </React.Fragment>
                        :
                            <React.Fragment>
                                <h5 className="product-price--main">${productPrice}</h5>
                                { productTotalRating > 0 &&
                                    <div class="mb-0 product-detail__content__header__comment-block">
                                        <div class="avg_rate">
                                            <span>{productAverageRating }</span>
                                            <Star/>
                                        </div>
                                        <div class="total_rating ml-2">
                                            ({productTotalRating})
                                        </div>
                                        {/* <div class="rate">
                                            {Array.from({length: productTotalRating}).map(item => (
                                                <i className="fas fa-star"></i>
                                            ))}
                                            
                                            {5 - productTotalRating > 0 ? 
                                                Array.from({length: (5-productTotalRating)}).map(item => (
                                                    <i className="far fa-star"></i>
                                                ))
                                                : null
                                            }
                                        </div> */}
                                    </div>
                                }
                            </React.Fragment>
                        }
                    </div>
                </div>
            </div>
        </div>
        )

        :

        (
            <div class="col-12">
                <div class="product-list">
                    <div class="product-list-thumb">
                        <div class="product-type">
                            {
                                arrivalType 
                                ?
                                    <h5 className="-new">{arrivalType}</h5>
                                :
                                discountPercent
                                ?
                                    <h5 className="-sale">-{discountPercent}</h5>
                                :
                                    null
                            }
                        </div>
                        <Link class="product-list-thumb__image" to={`/product-details/${productId}`}><img src={productImg ? productImg : StaticProductImg} alt="Product img"/><img src={productImg ? productImg : StaticProductImg} alt="Product img"/></Link>
                    </div>
                    <div class="product-list-content">
                        <div class="product-list-content__top">
                        <div class="product-category__wrapper">
                            { productTotalRating > 0 &&
                                <div class="product-detail__content__header__comment-block" style={{marginBottom: "0.5625em"}}>
                                    <div class="avg_rate">
                                        <span>{productAverageRating }</span>
                                        <Star/>
                                    </div>
                                    <div class="total_rating ml-2">
                                        ({productTotalRating})
                                    </div>
                                    {/* <div class="rate">
                                        {Array.from({length: productTotalRating}).map(item => (
                                            <i className="fas fa-star"></i>
                                        ))}
                                        
                                        {5 - productTotalRating > 0 ? 
                                            Array.from({length: (5-productTotalRating)}).map(item => (
                                                <i className="far fa-star"></i>
                                            ))
                                            : null
                                        }
                                    </div> */}
                                </div>
                            }
                        </div><Link class="product-name" to={`/product-details/${productId}`}>{productName}</Link>
                        <div class="product__price">
                            <div class="product__price__wrapper">
                                {discountPrice ? 
                                    <React.Fragment>
                                        <h5 className="product-price--main">${discountPrice}</h5>
                                        <h5 className="product-price--discount mr-2">${productPrice}</h5>            
                                    </React.Fragment>
                                :
                                    <h5 className="product-price--main">${productPrice}</h5>
                                }
                            </div>
                        </div>
                        </div>
                        <div class="product-list-content__bottom">
                        <p class="product-description">
                            {productKeywords.filter(keyword => keyword.trim().length !== 0).map(keyword => (
                                <Chip
                                    key={keyword}
                                    className="mr-2 mb-2"
                                    size="small"
                                    label={keyword}
                                    deleteIcon={<Done/>}
                                />
                            ))}
                        </p>
                        <div class="product-actions">
                            <div class="product-btn" onClick={onAddItemToCartHandler}>
                                <div class="add-to-cart ">
                                    <button style={{width: "45px", height: "45px"}} class="btn -round -red"><i class="fas fa-shopping-bag"></i></button>
                                    <h5>Add to cart</h5>
                                </div>
                            </div>
                            <div class="product-btn" onClick={onAddItemToWishlistHandler}>
                            <a href=" " onClick={e => e.preventDefault()} style={{marginTop:"-10px"}} class="btn -white product__actions__item -round"><i class="fas fa-heart"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    )
}

export default ShoppingItem
