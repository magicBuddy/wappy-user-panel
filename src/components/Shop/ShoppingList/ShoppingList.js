import React from 'react'
import FilterBar from '../FilterBar/FilterBar'
import ListView from './Views/ListView';
import GridView from './Views/GridView';
import ProductPaginator from '../ProductPaginator/ProductPaginator';

function ShoppingItems({
    view,
    setView,
    productList,
}) {
    return (
        <div class="col-12 col-md-8 col-lg-9">
            <FilterBar view={view} setView={setView} />
            <div class="shop-products">
                {   view === "Grid" ?
                
                    <div class="shop-products__gird">
                        <div class="row">
                            <GridView productList={productList} view={view} />
                        </div>
                    </div>
                    :
                    view === "List" ?

                    <div class="shop-products__list">
                        <div class="row">
                            <ListView productList={productList} view={view} />
                        </div>
                    </div>
                    : 
                    null
                }                
            </div>
            { productList.length > 0 && <ProductPaginator /> }
        </div>
    )
}

export default ShoppingItems
