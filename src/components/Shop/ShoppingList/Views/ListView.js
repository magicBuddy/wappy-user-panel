import React from 'react'
import { shopping_list } from '../../../../data/dummy/product_list'
import ShoppingItem from '../ShoppingItem/ShoppingItem'

function ListView({
    view,
    productList
}) {
    return (
        productList.length > 0 ? productList.map(product => (

            <ShoppingItem
                key = {product.id}
                productAverageRating = {product.avg_rate}
                productTotalRating = {product.total_rate}
                // productCategory = {product.productCategory}
                productDescription = {product.description}
                productPrice = {product.price}
                productImg = {product.image_url}
                productName = {product.name}
                productKeywords = {product.keywords.split(",")}
                // arrivalType = {product.arrivalType}
                // discountPercent = {product.discountPercent}
                // discountPrice = {product.discountPrice}
                // productRating = {product.productRating}
                productId = {product.id}
                view = {view}
            />
        ))

        :
        
        <h5 style={{margin: "15px" }}>No Products Found.</h5>

    )
}

export default ListView
