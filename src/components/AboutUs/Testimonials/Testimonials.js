import React from 'react'
import ContentDeco from '../../../assets/images/introduction/IntroductionOne/content-deco.png';
import { testimonials } from '../../../data/dummy/testimonial';
import TestimonialSlider from './TestimonialSlider/TestimonialSlider';

function Testimonials() {
    return (
        <div className="about_testimonials testimonial">
            <div className="section-title -center" style={{marginBottom: "3.125rem"}}>
                <h5>TESTIMONIAL</h5>
                <h2>What People Say?</h2><img src={ContentDeco} alt="Decoration"/>
            </div>
            <div className="container">
                <TestimonialSlider data={testimonials} showArrows/>
            </div>
        </div>
    )
}

export default Testimonials
