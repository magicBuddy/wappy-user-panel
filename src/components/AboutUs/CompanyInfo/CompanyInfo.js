import React from 'react'
import Img1 from '../../../assets/images/suppliers/kwadron.jpg';
import Img2 from '../../../assets/images/suppliers/membrane.png';
import Img3 from '../../../assets/images/suppliers/perma_blend.png';
import Img4 from '../../../assets/images/suppliers/Reynard-Health-Supplies-Logo.jpg';
import Img5 from '../../../assets/images/suppliers/stayve.jpg';
import Img6 from '../../../assets/images/suppliers/tina-davies-logo.gif';
import Benefits1 from '../../../assets/images/benefits/1.png';
import Benefits2 from '../../../assets/images/benefits/2.png';
import Benefits3 from '../../../assets/images/benefits/3.png';
import Benefits4 from '../../../assets/images/benefits/4.png';

// import FooterProductImages from '../../FooterProductImages/FooterProductImages';
import Slider from "react-slick";
import { PrevArrow, NextArrow } from '../../../utils/SlideArrow/SlideArrow';
import {Link} from 'react-router-dom';

function CompanyInfo() {
    const data = [Img2, Img5, Img4, Img6, Img3, Img1]
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        prevArrow: <PrevArrow /> ,
        nextArrow: <NextArrow /> ,
        responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                },
            },
        ],
    };
    return (
        <div className="company_info">
            <div class="introduction-nine">
                <div class="introduction-nine__logos">
                    <div class="container">
                        <Slider {...settings}>
                            {data.map((image, i) => {
                                return (
                                    <div key={i} className="slide__item">
                                    <img
                                        src={image}
                                        alt="Brand item"
                                    />
                                    </div>
                                );
                            })}
                        </Slider>
                    </div>
                </div>
                <div class="container">
                    <div class="introduction-nine__content">
                        <h3>New items are <br/>released weekly.</h3><Link class="btn -white" to="/shop">ALL NEW ITEMS</Link>
                    </div>
                </div>
            </div>
            <div class="mb-100">
                <div class="benefits">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="benefits__item">
                                    <div class="benefits__item__icon"><img src={Benefits1} alt="Free Shipping"/></div>
                                    <div class="benefits__item__content">
                                    <h5>Free Shipping</h5>
                                    <p>Free shipping on all order</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="benefits__item">
                                    <div class="benefits__item__icon"><img src={Benefits2} alt="Valuable Gifts"/></div>
                                    <div class="benefits__item__content">
                                    <h5>Valuable Gifts</h5>
                                    <p>Free gift after every 10 order</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="benefits__item">
                                    <div class="benefits__item__icon"><img src={Benefits3} alt="All Day Support"/></div>
                                    <div class="benefits__item__content">
                                    <h5>All Day Support</h5>
                                    <p>Call us: +01 234 567 89</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="benefits__item">
                                    <div class="benefits__item__icon"><img src={Benefits4} alt="Seasonal Sale"/></div>
                                    <div class="benefits__item__content">
                                    <h5>Seasonal Sale</h5>
                                    <p>Discounts up to 50% on all</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <FooterProductImages/> */}
        </div>
    )
}

export default CompanyInfo
