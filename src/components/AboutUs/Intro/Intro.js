import React from 'react'
import Img1 from '../../../assets/images/about/cosmetic_tattoo.jpg';
import Img2 from '../../../assets/images/introduction/IntroductionOne/img2.png';
import Bg1 from '../../../assets/images/introduction/IntroductionOne/bg1.png';
import Bg2 from '../../../assets/images/introduction/IntroductionOne/bg2.png';
import ContentDeco from '../../../assets/images/introduction/IntroductionOne/content-deco.png';
import Img11 from '../../../assets/images/introduction/IntroductionTwo/1.png';
import Img22 from '../../../assets/images/introduction/IntroductionTwo/2.png';
import Img33 from '../../../assets/images/introduction/IntroductionTwo/3.png';
import Img44 from '../../../assets/images/introduction/IntroductionTwo/4.png';
import { AcUnit } from '@material-ui/icons';
import { useSelector } from 'react-redux';

function Intro() {

    const {configData} = useSelector(state => state.products);

    const images = configData.filter(data => {
        
        if (data.name.includes("about-image") === true) return data
        
    });

    return (
        <div className="about_intro">
            <div className="introduction-one">
                <div className="container">
                    <div className="row align-items-center">
                    <div className="col-12 col-md-6">
                        <div className="introduction-one-image">
                        <div className="introduction-one-image__detail"><img src={images.length > 0 ? images[0].base_url +"/" +images[0].value : Img1} style={{objectFit: "contain", width: "100%", height: "100%"}} alt="background"/></div>
                        <div className="introduction-one-image__background">
                            <div className="background__item">
                            <div className="wrapper"><img data-depth="0.5" src={Bg1} alt="background"/></div>
                            </div>
                            <div className="background__item">
                            <div className="wrapper"><img data-depth="0.3" data-invert-x="" data-invert-y="" src={Bg2} alt="background"/></div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="introduction-one-content">
                        <h5>ABOUT<span></span></h5>
                                <div className="section-title " style={{marginBottom: "1.875em"}}>
                                    <h2 style={{fontWeight: 600}}>WELCOME TO <br/><span style={{color: "#F26460"}}>Brows & Beyond™️</span></h2>
                                    <img src={ContentDeco} alt="Decoration"/>
                                </div>
                        <p>
                            Jeni Hart is a qualified cosmetic tattoo specialist who runs “Brows & Beyond Cosmetic Tattooing and Pro Shop. She is the NZ distributor for Permablend Cosmetic Tattooing Pigments (World Famous Inks), Membrane Aftercare and ReelSkin Tattoo Practice Skins.
                            <br/>
                            <br/>
                            The clinic is upstairs at 84 Main Road Kumeu (in the Village near the BNZ) offering cosmetic tattoo and other beauty treatments such as Plasma Skin Tightening, Cosmetic Tattoo Removal, Yumi Lash Lifts, Microneedling and Teeth Whitening.
                            <br/>
                            <br/>
                            The Pro Shop accepts and delivers supplies on a daily basis and also offers a same day Auckland Metro delivery service if order received by midday.
                        </p>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div className="introduction-two introduction-one-content" style={{paddingTop: "0", paddingLeft: "0"}}>
                <div className="container text-center">                
                    <AcUnit style={{fontSize: "40px"}}/>					
                    <h2 className="mt-4 mb-4">“Designing Beauty That Lasts” by Jeni Hart.</h2>
                    <p><em>Jeni Hart is a qualified cosmetic tattoo specialist who runs “Brows &amp; Beyond Cosmetic Tattooing and Pro Shop. She is the NZ distributor for Permablend Cosmetic Tattooing Pigments (World Famous Inks), Membrane Aftercare and ReelSkin Tattoo Practice Skins.</em></p>
                    <div class="ult-spacer spacer-5fda7863c6d37" data-id="5fda7863c6d37" data-height="3" data-height-mobile="3" data-height-tab="3" data-height-tab-portrait="" data-height-mobile-landscape="" style={{clear:"both", display: "block"}}></div>
                    <div class="wpb_single_image wpb_content_element vc_align_center">
                        <figure class="wpb_wrapper vc_figure">
                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                { images.length > 1 ? 

                                    images.slice(1, images.length-1).map(image => (
                                        <img className="mr-3 mb-3" key={image.id} style={{width: "100%", height: "auto"}} src={image.base_url + "/" +image.value}/>
                                    ))
                                    
                                    :

                                    <img style={{width: "100%", height: "auto"}} src="https://browsandbeyond.co.nz/wp-content/uploads/2020/03/jeni-scaled.jpg" class="vc_single_image-img attachment-full" alt=""/>
                                }
                            </div>                                
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Intro
