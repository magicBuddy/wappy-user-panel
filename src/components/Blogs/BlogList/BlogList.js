import React from 'react'
import BlogImg1 from '../../../assets/images/blog/1.png';
function BlogList() {
    return (
        <div class="blog-content">
            <div class="row">
                <div class="col-12">
                    <div class="post-card-featured">
                        <div class="post-card-featured__image"><img src={BlogImg1} alt="Beauty Tips For Women Who Don’t Like Wearing Makeup"/></div>
                        <div class="post-card-featured__content">
                            <div class="post-card-featured__content__date">
                            <h5>05</h5>
                            <p>Feb</p>
                            </div>
                            <div class="post-card-featured__content__detail">
                            <div class="post-card-featured__info">
                                <p>by <span>Lindsay Weinberg</span></p><a href=" ">Beauty tips</a>
                            </div><a class="post-card-featured-title" href="/post-detail.html">Beauty Tips For Women Who Don’t Like Wearing Makeup</a>
                            <p class="post-card-featured-description">It is a long established fact that a reader will be distracted by the readable content of</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                        <div class="post-card-two">
                            <div class="post-card-two__image"><img src={BlogImg1} alt="The 8 Best Clay Masks For Acne Prone Skin"/>
                                <div class="post-card-two__info">
                                <div class="post-card-two__info__date">
                                    <h5>05</h5>
                                    <p>Feb</p>
                                </div>
                                <div class="post-card-two__info__detail">
                                    <p>by <span>Evelyn Tucker</span></p><a href=" ">Make up</a>
                                </div>
                                </div>
                            </div>
                            <div class="post-card-two__content"><a href="/post-detail.html">The 8 Best Clay Masks For Acne Prone Skin</a>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                            </div>
                        </div>
                </div>
                <div class="col-12 col-md-6">
                        <div class="post-card-two">
                            <div class="post-card-two__image"><img src={BlogImg1} alt="Well-ageing: Why is the skin matrisome important?"/>
                                <div class="post-card-two__info">
                                <div class="post-card-two__info__date">
                                    <h5>05</h5>
                                    <p>Feb</p>
                                </div>
                                <div class="post-card-two__info__detail">
                                    <p>by <span>Jackson Hunter</span></p><a href=" ">Skin care</a>
                                </div>
                                </div>
                            </div>
                            <div class="post-card-two__content"><a href="/post-detail.html">Well-ageing: Why is the skin matrisome important?</a>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece</p>
                            </div>
                        </div>
                </div>
                <div class="col-12 col-md-6">
                        <div class="post-card-two">
                            <div class="post-card-two__image"><img src={BlogImg1} alt="6 Clay Masks That Combat Oily, Acne-Prone Skin"/>
                                <div class="post-card-two__info">
                                <div class="post-card-two__info__date">
                                    <h5>05</h5>
                                    <p>Feb</p>
                                </div>
                                <div class="post-card-two__info__detail">
                                    <p>by <span>Lou Taylor</span></p><a href=" ">Body care</a>
                                </div>
                                </div>
                            </div>
                            <div class="post-card-two__content"><a href="/post-detail.html">6 Clay Masks That Combat Oily, Acne-Prone Skin</a>
                                <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently</p>
                            </div>
                        </div>
                </div>
                <div class="col-12 col-md-6">
                        <div class="post-card-two">
                            <div class="post-card-two__image"><img src={BlogImg1} alt="The Bold &amp; Beautiful Issue"/>
                                <div class="post-card-two__info">
                                <div class="post-card-two__info__date">
                                    <h5>05</h5>
                                    <p>Feb</p>
                                </div>
                                <div class="post-card-two__info__detail">
                                    <p>by <span>Lou Taylor</span></p><a href=" ">Videos</a>
                                </div>
                                </div>
                            </div>
                            <div class="post-card-two__content"><a href="/post-detail.html">The Bold &amp; Beautiful Issue</a>
                                <p>In partnership with recycling specialist Werner &amp; Mertz, the two companies aim to reduce uncertainty in the cosmetics</p>
                            </div>
                        </div>
                </div>
                <div class="col-12 col-md-6">
                        <div class="post-card-two">
                            <div class="post-card-two__image"><img src={BlogImg1} alt="This African make-up brand issues casting call for 100 British women"/>
                                <div class="post-card-two__info">
                                <div class="post-card-two__info__date">
                                    <h5>05</h5>
                                    <p>Feb</p>
                                </div>
                                <div class="post-card-two__info__detail">
                                    <p>by <span>Lou Taylor</span></p><a href=" ">Beauty tips</a>
                                </div>
                                </div>
                            </div>
                            <div class="post-card-two__content"><a href="/post-detail.html">This African make-up brand issues casting call for 100 British women</a>
                                <p>Zaron Cosmetics is said to be the fastest growing colour cosmetics company in the continent and has its eyes on the UK</p>
                            </div>
                        </div>
                </div>
                <div class="col-12 col-md-6">
                        <div class="post-card-two">
                            <div class="post-card-two__image"><img src={BlogImg1} alt="Coty appoints Princess Anna of Bavaria as new Chief of Corporate Affairs"/>
                                <div class="post-card-two__info">
                                <div class="post-card-two__info__date">
                                    <h5>05</h5>
                                    <p>Feb</p>
                                </div>
                                <div class="post-card-two__info__detail">
                                    <p>by <span>Lou Taylor</span></p><a href=" ">Make up</a>
                                </div>
                                </div>
                            </div>
                            <div class="post-card-two__content"><a href="/post-detail.html">Coty appoints Princess Anna of Bavaria as new Chief of Corporate Affairs</a>
                                <p>Journalist and author Anna von Bayern will take on the role as part of newly-appointed CEO Sue Y Nabi’s leadership</p>
                            </div>
                        </div>
                </div>
                <div class="col-12 col-md-6">
                        <div class="post-card-two">
                            <div class="post-card-two__image"><img src={BlogImg1} alt="How Covid-19 is forcing the beauty box industry to innovate"/>
                                <div class="post-card-two__info">
                                <div class="post-card-two__info__date">
                                    <h5>05</h5>
                                    <p>Feb</p>
                                </div>
                                <div class="post-card-two__info__detail">
                                    <p>by <span>Lou Taylor</span></p><a href=" ">Skin care</a>
                                </div>
                                </div>
                            </div>
                            <div class="post-card-two__content"><a href="/post-detail.html">How Covid-19 is forcing the beauty box industry to innovate</a>
                                <p>Journalist and author Anna von Bayern will take on the role as part of newly-appointed CEO Sue Y Nabi’s leadership</p>
                            </div>
                        </div>
                </div>
            </div>
            <ul class="paginator">
                <li class="page-item active">
                <button class="page-link">1</button>
                </li>
                <li class="page-item">
                <button class="page-link">2</button>
                </li>
                <li class="page-item">
                <button class="page-link"><i class="far fa-angle-right"></i></button>
                </li>
            </ul>
        </div>
    )
}

export default BlogList
