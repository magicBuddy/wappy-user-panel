import React from 'react'
import BlogList from './BlogList/BlogList'
import BlogImg1 from '../../assets/images/blog/1.png';

function BlogSection() {
    return (
        <div class="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="blog-sidebar">
                            <div class="blog-sidebar__section -search">
                            <form>
                                <input type="text" placeholder="Enter keyword" name="search"/>
                                <button><i class="fas fa-search"></i></button>
                            </form>
                            </div>
                            <div class="blog-sidebar__section">
                            <h5 class="blog-sidebar__title">Follow me</h5>
                                            <div class="social-icons -border -round -border--light-bg">
                                            <ul>
                                                <li><a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="https://twitter.com"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="https://instagram.com/"><i class="fab fa-instagram"> </i></a></li>
                                                <li><a href="https://www.youtube.com/"><i class="fab fa-youtube"></i></a></li>
                                            </ul>
                                            </div>
                            </div>
                            <div class="blog-sidebar__section -categories">
                            <h5 class="blog-sidebar__title">Categories</h5>
                            <ul>
                                <li><a href=" ">Beauty tips<span>1</span></a></li>
                                <li><a href=" ">Make up<span>2</span></a></li>
                                <li><a href=" ">Skin care<span>3</span></a></li>
                                <li><a href=" ">Videos<span>4</span></a></li>
                            </ul>
                            </div>
                            <div class="blog-sidebar__section -polular-post">
                            <h5 class="blog-sidebar__title">Popular post</h5>
                                    <div class="post-card-three">
                                        <div class="post-card-three__image"><img src={BlogImg1} alt="Beauty Tips For Women Who Don’t Like Wearing Makeup"/></div>
                                        <div class="post-card-three__content"><a href="/post-detail.html">Beauty Tips For Women Who Don’t Like Wearing Makeup</a>
                                        <p>2015-03-25</p>
                                        </div>
                                    </div>
                                    <div class="post-card-three">
                                        <div class="post-card-three__image"><img src={BlogImg1} alt="The 8 Best Clay Masks For Acne Prone Skin"/></div>
                                        <div class="post-card-three__content"><a href="/post-detail.html">The 8 Best Clay Masks For Acne Prone Skin</a>
                                        <p>2015-03-21</p>
                                        </div>
                                    </div>
                                    <div class="post-card-three">
                                        <div class="post-card-three__image"><img src={BlogImg1} alt="Well-ageing: Why is the skin matrisome important?"/></div>
                                        <div class="post-card-three__content"><a href="/post-detail.html">Well-ageing: Why is the skin matrisome important?</a>
                                        <p>2015-03-22</p>
                                        </div>
                                    </div>
                                    <div class="post-card-three">
                                        <div class="post-card-three__image"><img src={BlogImg1} alt="6 Clay Masks That Combat Oily, Acne-Prone Skin"/></div>
                                        <div class="post-card-three__content"><a href="/post-detail.html">6 Clay Masks That Combat Oily, Acne-Prone Skin</a>
                                        <p>2015-03-23</p>
                                        </div>
                                    </div>
                            </div>
                            <div class="blog-sidebar__section -newsletter">
                            <h5 class="blog-sidebar__title">News letter</h5>
                            <p>Subscribe to our newsletter a nd get our newest updates right on your inbox.</p>
                            <div class="subscribe-form blog-sidebar-newsletter">
                                <form class="mc-form">
                                <input class="email" id="mc-form-email" type="email" placeholder="Enter your email"/>
                                <label for="agree">
                                    <input id="agree" type="checkbox" name="agree"/>I agree to the terms & conditions
                                </label>
                                <button class="button">Subcribe</button>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <BlogList/>    
                    </div>
                </div>
            </div>
      </div>
    )
}

export default BlogSection
