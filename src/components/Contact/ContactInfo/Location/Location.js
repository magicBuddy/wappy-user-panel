import React from 'react'

function Location() {
    return (
        <div class="col-12">
            <iframe 
                title="Location"
                class="contact-map" 
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26544.761428132653!2d105.83081260286463!3d21.01523825635793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9bd9861ca1%3A0xe7887f7b72ca17a9!2zSMOgIE7hu5lpLCBIb8OgbiBLaeG6v20sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1594639675485!5m2!1svi!2s" 
                width="100%"
                height="450" 
                frameborder="0" 
                allowfullscreen="">    
            </iframe>
        </div>
    )
}

export default Location
