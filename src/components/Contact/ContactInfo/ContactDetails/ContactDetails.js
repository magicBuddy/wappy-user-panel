import React from 'react'

function ContactDetails() {
    return (
        <div class="col-12 col-md-6"> 
            <h3 class="contact-title">Contact info</h3>
            <div class="contact-info__item">
            <div class="contact-info__item__icon"><i class="fas fa-map-marker-alt"></i></div>
            <div class="contact-info__item__detail">
                <h3>Address</h3>
                <p>Level 1 84 Main Road, Kumeu (above Zacs Pizza near Phoenix Beauty) Auckland</p>
            </div>
            </div>
            <div class="contact-info__item">
            <div class="contact-info__item__icon"><i class="fas fa-phone-alt"></i></div>
            <div class="contact-info__item__detail">
                <h3>Phone</h3>
                <p> 021 300 065</p>
            </div>
            </div>
            <div class="contact-info__item">
            <div class="contact-info__item__icon"><i class="far fa-envelope"></i></div>
            <div class="contact-info__item__detail">
                <h3>Email</h3>
                <p>jeni@browsandbeyond.co.nz</p>
            </div>
            </div>
            <div class="contact-info__item">
            <div class="contact-info__item__icon"><i class="far fa-clock"></i></div>
            <div class="contact-info__item__detail">
                <h3>Working Hours</h3>
                <p>Mon-Fri: 10a.m – 3p.m</p>
            </div>
            </div>
        </div>
    )
}

export default ContactDetails
