import React from 'react'
import ContactDetails from './ContactDetails/ContactDetails'
import ContactForm from './ContactForm/ContactForm'
import Location from './Location/Location'

function ContactInfo() {
    return (
        <div class="contact">
        <div class="container">
            <div class="row">
                <ContactDetails/>
                <ContactForm/>
                <Location/>
            </div>
        </div>
      </div>
    )
}

export default ContactInfo
