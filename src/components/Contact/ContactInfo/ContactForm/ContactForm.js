import React from 'react'

function ContactForm() {
    return (
        <div class="col-12 col-md-6">
            <h3 class="contact-title">Get in touch</h3>
            <div class="contact-form">
            <form>
                <div class="input-validator">
                <input type="text" name="name" placeholder="Name"/>
                </div>
                <div class="input-validator">
                <input type="text" name="email" placeholder="Email"/>
                </div>
                <div class="input-validator">
                <textarea name="message" id="" cols="30" rows="3" placeholder="Message"></textarea>
                </div><a class="btn -dark" href=" ">SEND MESSAGE</a>
            </form>
            </div>
        </div>
    )
}

export default ContactForm
