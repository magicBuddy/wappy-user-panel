import React from 'react'
import { Link } from 'react-router-dom'
import TrendingCategory from './TrendingCategory/TrendingCategory'

function SeasonTrends({ newArrivalsList, trendingImage, seasonTrends }) {

    return (
        <div className="season-trends_section">
            <div className="introduction-four">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="introduction-four__item -style-0">
                                <div className="introduction-four__item__content">
                                <h3>Meet <span>Trends</span></h3>
                                <h5>of the season</h5>
                                </div>
                            </div>
                        </div>

                        {seasonTrends.map(category => (
                            <TrendingCategory
                                category={category}
                                key={category.id}
                            />
                        ))}                        
                    </div>
                </div>
            </div>
            <div class="introduction-five" style={{background: `url(${trendingImage}) rgba(0,0,0,0.5)`}}>
                <div className="overlay">
                    <div class="container">
                        <div class="introduction-five__content">
                            <h2>New items are<br/>released weekly.</h2><Link class="btn -red" to="/shop">All new items</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SeasonTrends
