import React from 'react'
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import { NextArrow, PrevArrow } from '../../../../utils/SlideArrow/SlideArrow';
import ProductItem from '../../NewArrivals/ProductItem/ProductItem';

function TrendingCategory({category}) {

     const settings = {
        centerMode: true,
        centerPadding: "250px",
        slidesToShow: category.products.length > 6 ? 3 : category.products.length < 6 ? category.products.length : 1,
        prevArrow: <PrevArrow /> ,
        nextArrow: <NextArrow /> ,
        responsive: [{
                breakpoint: 1500,
                settings: {
                    centerPadding: "200px",
                    slidesToShow: category.products.length > 6 ? 3 : category.products.length < 6 ? category.products.length : 1
                },
            },
            {
                breakpoint: 1140,
                settings: {
                    centerPadding: "100px",
                    slidesToShow: category.products.length > 6 ? 3 : category.products.length < 6 ? category.products.length : 1,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    centerPadding: "100px",
                    slidesToShow: category.products.length > 6 ? 2 : category.products.length < 6 && category.products.length > 1 ? 2 : 1,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    // centerMode: false,
                    centerPadding: "40px",
                    slidesToShow: category.products.length > 6 ? 2 : category.products.length < 6 && category.products.length > 1 ? 2 : 1,
                },
            },
            {
                breakpoint: 516,
                settings: {
                    // centerMode: false,
                    centerPadding: "40px",
                    slidesToShow: 1,
                },
            }
        ],
    };

    const match = {'/': '-', '&': 'and', ',': '', ' ': '-'};

    return (
        <div  className="col-12">
            <div className="introduction-four__item -style-2">
                <div className="introduction-four__item__content">
                <h3><span style={{color: "#111"}}>{category.category_name}</span></h3>
                <span 
                    style={{
                        color: "#F26460",
                        borderTop: "8px solid #F26460",
                        height: "10px",
                        marginLeft: "0.5rem",
                        width: "7rem",
                        display: "block"
                    }}
                ></span>
                {/* <Link className="btn btn -transparent -underline" to={`/categories/${category.category_name.replace(/[/ /&/,]/g, ch => match[ch])}`}>Shop Now</Link> */}
                </div>
            </div>
            <div className="product-tab-slide__content">
                <div className="product-slider">
                        
                    <Slider {...settings}>
                        {category.products.map(product => (                                                
                            <div key={product.id} className="product-slide__item">
                                <ProductItem
                                    key={product.id}
                                    productCategory={product.productCategory}
                                    productPrice={product.price ? product.price: ""}
                                    productImg={product.image_url ? product.image_url : ""}
                                    productName={product.name ? product.name : ""}
                                    arrivalType={product.arrivalType ? product.arrivalType : ""}
                                    discountPercent={product.discountPercent}
                                    discountPrice={product.discountPrice}
                                    productRating={product.productRating}
                                    productId={product.id}
                                />
                            </div>                                                
                        ))}
                    </Slider>
                </div>
            </div>
        </div>
    )
}

export default TrendingCategory
