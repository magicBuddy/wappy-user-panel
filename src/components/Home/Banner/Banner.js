import React from 'react'
import Carousel from "react-multi-carousel";
import 'react-multi-carousel/lib/styles.css';

function Banner({
    bannerImages
}) {
    
    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 1
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 1
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 1
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1
        }
    };

    return (
        <div className="banner_section" style={{marginBottom: "6rem"}}> 
            { bannerImages.length > 0 ?
                <Carousel responsive={responsive}>
                    { bannerImages.map(image => (

                            <img className="w-100 h-auto" src={image}/>
                        ))
                    }
                </Carousel>
                
             : null }
        </div>
    )
}

export default Banner
