import React from 'react'
import SplashImg from '../../../assets/images/introduction/IntroductionThree/bg.png';
import Img1 from '../../../assets/images/about/cosmetic_tattoo.jpg';
import Img2 from '../../../assets/images/about/cosmetic_tattoo.jpg';
import DecorationImg from '../../../assets/images/introduction/IntroductionThree/decoration.png';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

function AboutUs() {

    const {configData} = useSelector(state => state.products);

    const images = configData.filter(data => {
        
        if (data.name.includes("about-image") === true) return data
        
    });

    return (
        <div className="about_section">
            <div className="introduction-three">
                <div className="container" style={{maxWidth: "1170px"}}>
                    <div className="row align-items-center">
                        <div className="col-12 col-md-6">
                            <div className="introduction-three__image">
                                <div className="introduction-three__image__background"><img src={SplashImg} alt="background"/></div>
                                <div className="introduction-three__image__detail">
                                <div className="image__item">
                                    <div className="wrapper"><img data-depth="0.3" src={images.length > 0 ? images[0].base_url +"/" +images[0].value : Img1} alt="Img1"/></div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className="introduction-three__content">
                                <h5>About</h5>
                                <h3>WELCOME TO <br/><span>Brows & Beyond™️</span></h3>
                                <div className="more-description">
                                    <img src={DecorationImg} alt="Decoration"/>
                                    <span>
                                        “Designing Beauty That Lasts” <br/> - Jeni Hart.
                                    </span>
                                </div>
                                <p>Jeni Hart is a qualified cosmetic tattoo specialist who runs “Brows & Beyond Cosmetic Tattooing and Pro Shop. She is the NZ distributor for Permablend Cosmetic Tattooing Pigments (World Famous Inks), Membrane Aftercare and ReelSkin Tattoo Practice Skins.
                                </p>
                                <Link className="btn -dark" to="/about">Read more</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AboutUs
