import React from 'react'
import { product_list } from '../../../data/dummy/product_list';
import ProductItem from './ProductItem/ProductItem';
import Slider from "react-slick";
import { NextArrow, PrevArrow } from "../../../utils/SlideArrow/SlideArrow";
import { Link } from 'react-router-dom';

function NewArrivals({
    newArrivalsList
}) {
    
    const settings = {
        centerMode: true,
        centerPadding: "250px",
        slidesToShow: newArrivalsList.length > 6 ? 4 : newArrivalsList.length < 6 ? newArrivalsList.length : 1,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        responsive: [{
            breakpoint: 1500,
            settings: {
                centerPadding: "200px",
                slidesToShow: newArrivalsList.length > 6 ? 3 : newArrivalsList.length < 6 ? newArrivalsList.length : 1
            },
        },
        {
            breakpoint: 1140,
            settings: {
                centerPadding: "100px",
                slidesToShow: newArrivalsList.length > 6 ? 3 : newArrivalsList.length < 6 ? newArrivalsList.length : 1,
            },
        },
        {
            breakpoint: 992,
            settings: {
                centerPadding: "100px",
                slidesToShow: newArrivalsList.length > 6 ? 2 : newArrivalsList.length < 6 && newArrivalsList.length > 1 ? 2 : 1,
            },
        },
        {
            breakpoint: 600,
            settings: {
                // centerMode: false,
                centerPadding: "40px",
                slidesToShow: newArrivalsList.length > 6 ? 2 : newArrivalsList.length < 6 && newArrivalsList.length > 1 ? 2 : 1,
            },
        },
        {
            breakpoint: 516,
            settings: {
                // centerMode: false,
                centerPadding: "40px",
                slidesToShow: 1,
            },
        }
        ],
    };
    
    return (
        <div className="new-arrivals_section">
            <div className="product-tab-slide">
                <div className="container">
                    <div className="product-tab-slide__header">
                        <h5>New Arrivals</h5>
                        <div className="product-tab-slide__header__controller">
                        {/* <ul> */}
                            {/* <li className="active"><Link to="/shop">All</Link></li>
                            <li><a href=" ">Eyes</a></li>
                            <li><a href=" ">Face</a></li>
                            <li><a href=" ">Lips</a></li> */}
                        {/*     </ul> */}
                        <Link className="btn -white" to="/shop">View all</Link>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="product-tab-slide__content">
                        <div className="product-slider">
                                
                            <Slider {...settings}>
                                {newArrivalsList.map(product => (
                                    <div key={product.id} className="product-slide__item">
                                        <ProductItem
                                            key={product.id}
                                            productCategory={product.productCategory}
                                            productPrice={product.price ? product.price: ""}
                                            productImg={product.image_url ? product.image_url : ""}
                                            productName={product.name ? product.name : ""}
                                            arrivalType={product.arrivalType ? product.arrivalType : ""}
                                            discountPercent={product.discountPercent}
                                            discountPrice={product.discountPrice}
                                            productRating={product.productRating}
                                            productId={product.id}
                                        />
                                    </div>
                                ))}
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewArrivals 
