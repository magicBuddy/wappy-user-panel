import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import StaticProductImg from '../../../../assets/images/product/1.jpg';
import { addItemToCart, addItemToWishlist } from '../../../../redux/action/action';

function ProductItem({
    arrivalType,
    productImg,
    productCategory,
    productName,
    productPrice,
    discountPrice,
    discountPercent,
    productRating,
    productId
}) {

    const history = useHistory();
    const dispatch = useDispatch();

    const {cartProducts} = useSelector(state => state.cart);
    const {userDetails} = useSelector (state => state.auth);
    
    const onAddItemToWishlistHandler = () => {

        if (Object.keys(userDetails).length === 0) return history.push('/login');
        
        dispatch(addItemToWishlist(productId));
        
    }

    const onAddItemToCartHandler = () => {

        if (Object.keys(userDetails).length === 0) return history.push('/login');

        const itemDetails = {productId, productQuantity: 1, productPrice, productName, productImg};
        
        const itemIndex = cartProducts.findIndex(product => product.productId === productId);
        
        if (itemIndex >= 0) {

            itemDetails.productQuantity = cartProducts[itemIndex].productQuantity + 1;
            dispatch(addItemToCart(itemDetails, itemIndex));

        }
        
        else dispatch(addItemToCart(itemDetails));

    }
    
    return (
        <div className="product">
            <div className="product-type">
                {
                    arrivalType 
                    ?
                        <h5 className="-new">{arrivalType}</h5>
                    :
                    discountPercent
                    ?
                        <h5 className="-sale">-{discountPercent}</h5>
                    :
                        null
                } 
            </div>
            <div className="product-thumb"><Link className="product-thumb__image" to={`/product-details/${productId}`}><img style={{objectFit: "contain"}} src={productImg ? productImg: StaticProductImg} alt="Product img"/><img style={{objectFit: "contain"}} src={productImg ? productImg: StaticProductImg} alt="Product img"/></Link>
                <div class="product-thumb__actions">
                    <div class="product-btn">
                        <button data-toggle="tooltip" title="Add to Cart" class="btn -white product__actions__item -round product-atc" onClick={onAddItemToCartHandler}><i class="fas fa-shopping-bag"></i></button>
                    </div>
                    <div class="product-btn">
                        <button data-toggle="tooltip" title="Add to Wishlist" class="btn -white product__actions__item -round" onClick={onAddItemToWishlistHandler}><i class="fas fa-heart"></i></button>
                    </div>
                </div>
            </div>
            <div className="product-content">
                <div className="product-content__header">
                    { productCategory ? <div className="product-category">{productCategory}</div> : null }
                    {/* <div className="rate">
                        {Array.from({length: productRating}).map(item => (
                            <i className="fas fa-star"></i>
                        ))}
                        {5 - productRating > 0 ? 
                            Array.from({length: (5-productRating)}).map(item => (
                                <i className="far fa-star"></i>
                            ))
                            : null
                        }
                    </div> */}
                </div><Link className="product-name" to={`/product-details/${productId}`}>{productName}</Link>
                <div className="product-content__footer">
                        {discountPrice ? 
                            <React.Fragment>
                                <h5 className="product-price--main">${discountPrice}</h5>
                                <h5 className="product-price--discount ml-2">${productPrice}</h5>            
                            </React.Fragment>
                        : productPrice ? 
                            <h5 className="product-price--main">${productPrice}</h5>
                        : null}
                        
                    {/* <div className="product-colors">
                        <div className="product-colors__item" style={{backgroundColor: "#8B0000"}}></div>
                        <div className="product-colors__item" style={{backgroundColor: "#4169E1"}}></div>
                    </div> */}
                </div>
            </div>
        </div>
    )
}

export default ProductItem
