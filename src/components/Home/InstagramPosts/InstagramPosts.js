import React from 'react'
import Slider from 'react-slick'
import { NextArrow, PrevArrow } from "../../../utils/SlideArrow/SlideArrow";
import InstagramFeed from './InstagramFeed';

function InstagramPosts({
  instagramPosts
}) {

  const settings = {
    centerMode: true,
    centerPadding: "250px",
    slidesToShow: instagramPosts.length > 6 ? 4 : instagramPosts.length < 6 ? instagramPosts.length : 1,
    prevArrow: < PrevArrow / > ,
    nextArrow: < NextArrow / > ,
    responsive: [{
        breakpoint: 1500,
        settings: {
          centerPadding: "200px",
          slidesToShow: instagramPosts.length > 6 ? 3 : instagramPosts.length < 6 ? instagramPosts.length : 1
        },
      },
      {
        breakpoint: 1140,
        settings: {
          centerPadding: "100px",
          slidesToShow: instagramPosts.length > 6 ? 3 : instagramPosts.length < 6 ? instagramPosts.length : 1,
        },
      },
      {
        breakpoint: 992,
        settings: {
          centerPadding: "100px",
          slidesToShow: instagramPosts.length > 6 ? 2 : instagramPosts.length < 6 && instagramPosts.length > 1 ? 2 : 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          // centerMode: false,
          centerPadding: "40px",
          slidesToShow: instagramPosts.length > 6 ? 2 : instagramPosts.length < 6 && instagramPosts.length > 1 ? 2 : 1,
        },
      },
      {
        breakpoint: 516,
        settings: {
          // centerMode: false,
          centerPadding: "40px",
          slidesToShow: 1,
        },
      }
    ],
  };
    
    return (
        
        instagramPosts.length > 0 &&
          <div class="product-slide" style={{marginTop: "100px"}}>
            <div class="container">
              <div class="section-title -center" style={{marginBottom: "1.875em"}}>
                <h2>Instagram Feed</h2>
              </div>
            </div>
            <div className="product-tab-slide__content">
              <div class="product-slider">
                <Slider {...settings}>
                    {instagramPosts.map(instaPost => (
                        <div key={instaPost.id} className="product-slide__item">
                            <InstagramFeed
                                key={instaPost.id}
                                type={instaPost.type}
                                url={instaPost.url}
                                instagramId={instaPost.id}
                                caption={instaPost.caption}
                                permalink={instaPost.permalink}
                            />
                        </div>
                    ))}
                </Slider>
              </div>
            </div>
          </div>
    )
}

export default InstagramPosts
