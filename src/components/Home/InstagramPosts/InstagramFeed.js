import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import StaticProductImg from '../../../assets/images/product/1.jpg';
import { addItemToCart } from '../../../redux/action/action';

function InstagramFeed({
    instagramId,
    url,
    type,
    permalink
}) {

    const onViewInstagramSource = () => window.open(permalink);
    
    return (
        <div className="product">
            <div className="product-thumb">
                { type === "video" &&
                    <video className="w-100" height="260px" controls>
                        <source src={url} type="video/mp4"/>
                    </video>
                }
                { type === "image" &&
                    <Link className="product-thumb__image">
                        <img className="w-100" src={url ? url: StaticProductImg} alt="Instagram Post"/>
                        <img className="w-100" src={url ? url: StaticProductImg} alt="Instagram Post"/>
                    </Link>
                    
                }
            </div>
        </div>
    )
}

export default InstagramFeed
