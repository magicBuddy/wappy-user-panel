import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import ContentDeco from '../../assets/images/introduction/IntroductionOne/content-deco.png';
import axiosInstance from '../../axios/axios-instance';
import { getProductList, setActiveCategory, setLoadingState, setPaginationLinks, setProductList } from '../../redux/action/action';

function ParentCategorySidebar({
    categories,
    productsLength
}) {

    const dispatch = useDispatch();
    const {activeCategory, pageLimit} = useSelector(state => state.products);
    const [openCategoryMenu, setOpenCategoryMenu] = useState(true);
    const match = {'/': '-', '&': 'and', ',': '', ' ': '-'};
    
    const getProductListByKeyword = async (limit, title) => {

        dispatch(setLoadingState(true));
        const response = await axiosInstance.get('/getProductByKeyword', {
            headers: {
                Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
            },
            params: {
                limit,
                search_text: title
            }
        }).catch(err => console.log(err))

        if (response && response.data && response.data.code === 200) {
            dispatch(setActiveCategory(title));
            dispatch(setProductList(response.data.data && response.data.data.data));
            dispatch(setPaginationLinks(response.data.data && response.data.data.links));
            dispatch(setLoadingState(false));
        }
    }

    const onToggleSubCategoryMenu = (categoryTitle,index) => {

        const categoryTitleEl = document.getElementById(`${categoryTitle}${index}`);
        if (categoryTitleEl.style.display === "block") {
            categoryTitleEl.style.display = "none";
            document.getElementById(`${index}${categoryTitle}`).classList.replace('fa-angle-up','fa-angle-down')

        }
        else {
            categoryTitleEl.style.display = "block";
            document.getElementById(`${index}${categoryTitle}`).classList.replace('fa-angle-down','fa-angle-up')
        }
    }

    return (
        <div class="col-12 col-md-4 col-lg-3">
            <div class="shop-sidebar">
            <div class="shop-sidebar__content">
                <div class="shop-sidebar__section -categories">
                                <div class="section-title -style1 -medium" style={{marginBottom: "1.875em"}}>
                                <h2>Categories</h2><img src={ContentDeco} alt="Decoration"/>
                                </div>
             
                        {
                            categories && categories.length > 0 ?

                            <ul class="dropdown-menu" style={{display: 'block'}}>

                                {categories.map((category,index) => (

                                    <ul class="dropdown-menu__col">   

                                        <li>
                                            
                                            <Link style={{fontWeight: activeCategory === category.title && 600}} className={`${activeCategory===category.title && "category-active"}`} to={`/categories/${category.title.replace(/[/ /&/,]/g, ch => match[ch])}`} onClick={(e) => {getProductListByKeyword(pageLimit,category.title);}}> {category.title}
                                            {
                                                category.sub_categories && category.sub_categories.length > 0 ?  <span class="dropable-icon ml-2" onClick={(e) => {e.preventDefault(); e.stopPropagation(); onToggleSubCategoryMenu(category.title, index);}}><i id={`${index}${category.title}`} class={`fas fa-angle-up`}></i></span> : null 
                                            }                                                               
                                            </Link>

                                            {category.sub_categories && category.sub_categories.length > 0 ?

                                                <ul id={`${category.title}${index}`} style={{display: "block"}} class="dropdown-menu">

                                                {category.sub_categories.map((sub_category,index) => (

                                                    <ul class="dropdown-menu__col ml-2">                                
                                                        <li>
                                                            
                                                            <Link style={{fontWeight: activeCategory === sub_category.title && 600}} to={`/categories/${category.title.replace(/[/ /&/,]/g, ch => match[ch])}/${sub_category.title.replace('/','-').replace(/&/g,'and').replace(/ /g, '-')}`} className={`${activeCategory === sub_category.title && "category-active"}`} onClick={(e) => {getProductListByKeyword(pageLimit, sub_category.title);}}> {sub_category.title}
                                                            {
                                                                sub_category.sub_categories && sub_category.sub_categories.length > 0 ?  <span onClick={(e) => { e.preventDefault(); e.stopPropagation(); onToggleSubCategoryMenu(sub_category.title, index);}} class="ml-2 dropable-icon"><i id={`${index}${sub_category.title}`} class={`fas fa-angle-up`}></i></span> : null 
                                                            }                                                               
                                                            </Link>

                                                            {sub_category.sub_categories && sub_category.sub_categories.length > 0 ?

                                                                <ul id={`${sub_category.title}${index}`} style={{display: "block"}} class="dropdown-menu">

                                                                {sub_category.sub_categories.map((sub_sub_category,index) => (

                                                                    <ul class="dropdown-menu__col ml-3">                                
                                                                        <li>
                                                                            
                                                                            <Link style={{fontWeight: activeCategory === sub_sub_category.title && 600}} className={`${activeCategory === sub_sub_category.title && "category-active"}`} to={`/categories/${category.title.replace(/[/ /&/,]/g, ch => match[ch])}/${sub_category.title.replace('/','-').replace(/&/g,'and').replace(/ /g, '-')}/${sub_sub_category.title.replace(/[/ /&/,]/g, ch => match[ch])}`} onClick={(e) => { getProductListByKeyword(pageLimit, sub_sub_category.title);}}> {sub_sub_category.title}
                                                                            {
                                                                                sub_sub_category.sub_categories && sub_sub_category.sub_categories.length > 0 ?  <span onClick={(e) => {e.preventDefault(); e.stopPropagation(); onToggleSubCategoryMenu(sub_sub_category.title, index);}} class="ml-2 dropable-icon"><i id={`${index}${sub_sub_category.title}`} class={`fas fa-angle-up`}></i></span> : null 
                                                                            }                                                               
                                                                            </Link>

                                                                        </li>
                                                                    </ul>

                                                                ))}

                                                                </ul>

                                                                : null
                                                            }

                                                        </li>
                                                    </ul>

                                                ))}

                                            </ul>
                                                : null
                                            }

                                        </li>
                                    </ul>

                                ))}

                            </ul>

                            : null
                            
                        }
                        
                    {/* </li>
                    
                </ul> */}
                </div>                
            </div>
            </div>
        </div>
    )
}

export default ParentCategorySidebar
