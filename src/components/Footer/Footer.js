import React from 'react'
import PaymentImg from '../../assets/images/footer/payment.png';
import Logo from '../../assets/images/logo.png';
import {Link} from 'react-router-dom';
import { useSelector } from 'react-redux';

function Footer() {

    const {configData} = useSelector(state => state.products);

    const footerData = configData.filter(data => data.name.includes("footer"));
    
    let footerValue;

    if (footerData.length === 1) {
    
        footerValue = JSON.parse(footerData[0].value)
    }

    return (
        <div className="footer_section">
            <div className="footer-one">
                <div className="container">
                <div className="footer-one__header">
                    <div className="footer-one__header__logo"><a href="/homepages/homepage1"><img src={Logo} alt="Logo"/></a></div>
                    <div className="footer-one__header__newsletter"><span>Subscribe Newletter:</span>
                    <div className="footer-one-newsletter">
                        <div className="subscribe-form">
                        <div className="mc-form">
                            <input type="text" placeholder="Enter your email"/>
                                <button className="btn "><i className="fas fa-paper-plane"></i>
                                </button>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="footer-one__header__social">
                            <div className="social-icons -border">
                            <ul>
                                <li><a href="https://www.facebook.com/"><i className="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com"><i className="fab fa-twitter"></i></a></li>
                                <li><a href="https://instagram.com/"><i className="fab fa-instagram"> </i></a></li>
                                <li><a href="https://www.youtube.com/"><i className="fab fa-youtube"></i></a></li>
                            </ul>
                            </div>
                    </div>
                </div>
                <div className="footer-one__body">
                    <div className="row">
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="footer__section -info">
                        <h5 className="footer-title">Contact info</h5>
                        <p>Address: 
                            { footerValue && footerValue.address ? 
                                <span> {footerValue && footerValue.address}</span>
                                : 
                                <span> -</span>
                            }
                        </p> 
                        <p>Phone: 
                            { footerValue && footerValue.phone ? 
                                <span> {footerValue && footerValue.phone}</span> 
                                :
                                <span> -</span>
                            }
                        </p>
                        <p>Email: 
                            { footerValue && footerValue.email ?
                                <span> {footerValue && footerValue.email}</span>
                                :
                                <span> -</span>
                            }
                        </p>
                        <p>Working Hours: 
                            { footerValue && footerValue.workingHours ?
                                <span> {footerValue && footerValue.workingHours}</span>
                                :
                                <span> -</span>
                            }
                            </p>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-4">
                        <div className="footer__section -links">
                        <div className="row">
                            <div className="col-12 col-sm-6">
                            <h5 className="footer-title">Account</h5>
                            <ul>
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/cart">Cart</Link></li>
                                <li><Link to="/shop">Shop</Link></li>
                                <li><Link to="/checkout">Checkout</Link></li>
                            </ul>
                            </div>
                            <div className="col-12 col-sm-6">
                                <h5 className="footer-title">Information</h5>
                                <ul>
                                    <li><Link to="/about">About us</Link></li>
                                    <li><Link to="/contact">Contact Us</Link></li>
                                    {/* <li><Link to="/">Delivery Information</Link></li>
                                    <li><Link to="/">Privacy Policy</Link></li>
                                    <li><Link to="/">Terms &amp; Condition</Link></li> */}
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-12 col-lg-4">
                        <div className="footer__section -payment">
                        <h5 className="footer-title">Payment methods</h5>
                        {/* <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit gravida facilisis.</p> */}
                        <div className="payment-methods"><img src={PaymentImg} alt="Payment methods"/></div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="footer-one__footer">
                <div className="container">
                    <div className="footer-one__footer__wrapper">
                    <ul>
                        <li><Link to="/">Privacy Policy</Link></li>
                        <li><Link to="/">Terms &amp; Condition</Link></li>
                        <li><Link to="/">Site map</Link></li>
                    </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}

export default Footer
