import Slider from "react-slick";
import React, { useState } from 'react';
import ProductImg1 from '../../../assets/images/product/1.jpg';
import { PrevArrow, NextArrow } from '../../../utils/SlideArrow/SlideArrow';
import Carousel from "react-multi-carousel";

function ProductImages({
    productImages
}) {

    const productImgs = productImages.length > 0 ? [...productImages] : [{image_url: ProductImg1}]
    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 1
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 1
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 1
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1
        }
    };
    const slider1Settings = {
        arrows: false,
        swipe: false,
    };
    const slider2Settings = {
        arrows: true,
        focusOnSelect: true,
        slidesToShow: productImgs.length > 2 ? 3 : productImgs.length < 3 ? 1 : 1,
        centerMode: true,
        centerPadding: "80px",
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        responsive: [
        {
            breakpoint: 992,
            settings: {
            slidesToShow: 1,
            },
        },
        {
            breakpoint: 768,
            settings: {
            centerPadding: "50px",
            slidesToShow: 1
            },
        },
        {
            breakpoint: 576,
            settings: {
            slidesToShow: 1,
            centerPadding: "50px",
            },
        },
        ],
    };
    const [nav1, setNav1] = useState();
    const [nav2, setNav2] = useState();

    return (
        <div className="product-detail__slide-two">
            <div className="product-detail__slide-two__big">
                {/* <Slider asNavFor={nav2} ref={(c) => setNav1(c)} {...slider1Settings}>
                    {productImgs &&
                        productImgs.map((img, index) => (
                        <div key={index} className="slider__item">
                            { img.image_url ? <img src={img.image_url} alt="Product img" /> : null }
                        </div>
                    ))}
                </Slider> */}
                { productImgs.length > 0 ?
                <Carousel responsive={responsive}>
                    { productImgs.map(image => (

                            <img className="w-100 h-auto" src={image.image_url}/>
                        ))
                    }
                </Carousel>
                
             : null }
            </div>
        </div>
    )
}

export default ProductImages
