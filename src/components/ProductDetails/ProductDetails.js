import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import axiosInstance from '../../axios/axios-instance';
import { addItemToCart, addLikeToProductReview, addDislikeToProductReview, addReviewToProductReviewsList, removeReviewFromProductReviewList, setProgressState, showSnackbar, addItemToWishlist } from '../../redux/action/action';
import MoreDetails from './MoreDetails/MoreDetails';
import ProductImages from './ProductImages/ProductImages';
import Star from '@material-ui/icons/Star';
import { useHistory } from 'react-router-dom';
import { Chip } from '@material-ui/core';
import { Done } from '@material-ui/icons';

function ProductDetails({
    productName,
    productCategory,
    productRating,
    productPrice,
    discountPrice,
    productImage,
    productImages,
    productDescription,
    productAvailability,
    productId,
    productStatus,
    productReviewList,
    canUserReview,
    productTotalRating,
    productAverageRating,
    productKeywords
}) {

    const [productReviewDetails, setProductReviewDetails] = useState({
        subject: '',
        description: '',
        rating: 0
    })
    const [imagesForPreview, setImagesForPreview] = useState([]);
    const [imageFiles, setImageFiles] = useState([]);

    const dispatch = useDispatch();
    const history = useHistory();

    const {cartProducts} = useSelector(state => state.cart);
    const {userDetails} = useSelector(state => state.auth);

    const changeProductReviewDetails = ({target}) => {

        setProductReviewDetails({...productReviewDetails, [target.name] : target.value})

    }

    const onChangeRating = (value) => {

        setProductReviewDetails({...productReviewDetails, rating: value})

    }

    const isProductReviewDetailsValid = () => {

      for (let key of Object.keys(productReviewDetails)) {

        if ((key === "subject" || key === "description") && !productReviewDetails[key].trim()) {

          dispatch(showSnackbar({message: "Subject and Description cannot be empty!", type: "warning", open: true}))
          return;

        }

        if (key === "rating" && (productReviewDetails[key] && (productReviewDetails[key] < 1 || productReviewDetails[key] > 5))) {

          dispatch(showSnackbar({message: "Rating should be between 1 to 5 only!", type: "warning", open: true}))
          return;

        } 

      }

      return true;

    } 

    const onSubmitProductReview = async (e) => {
        
        e.preventDefault();

        if (isProductReviewDetailsValid()) {

            dispatch(setProgressState(true));

            const formData = new FormData();
            formData.append('product_id', productId);
            formData.append('subject', productReviewDetails.subject);
            formData.append('description', productReviewDetails.description);
            formData.append('rate', productReviewDetails.rating);
            
            for (let i = 0; i < imageFiles.length; i++) {

                formData.append(`files[${i}]`, imageFiles[i]);

            }

            const productReviewResponse = await axiosInstance.post('/addProductReview', formData, 
            {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            }).catch(err => console.log(err))

            if (productReviewResponse && productReviewResponse.data.code === 200) {

                const user = {
                    id: userDetails?.id,
                    name: userDetails?.name,
                    image_url: userDetails?.image_url,
                    email: userDetails?.email,
                    phone_number: userDetails?.phone_number
                }
                const productReviewToAdd = {...productReviewResponse.data.data, user};

                dispatch(addReviewToProductReviewsList(productReviewToAdd));
                dispatch(setProgressState(false));
                dispatch(showSnackbar({message: "Review addded successfully!", type: "success", open: true}))
                setProductReviewDetails({
                    subject: '',
                    description: '',
                    rating: ''
                });
                setImageFiles([]);
                setImagesForPreview([]);
            }
        }
    }

    const onDeleteProductReview = async (reviewId) => {

        if (reviewId) {

            dispatch(setProgressState(true));
            const response = await axiosInstance.post('/deleteProductReview', {
                review_id: reviewId
            }, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            }).catch(err => console.log(err))

            if (response && response.data.code === 200) {

                dispatch(removeReviewFromProductReviewList(reviewId));
                dispatch(setProgressState(false));
                dispatch(showSnackbar({message: "Review removed successfully!", type: "success", open: true}));

            }
        }

    }

    const onLikeReview = async (reviewId) => {

        if (Object.keys(userDetails).length > 0) {

            const response = await axiosInstance.post('/like', {review_id: reviewId}, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            }).catch(err => {

                if (err.response && err.response.data.code === 422) {

                    dispatch(showSnackbar({message: "Review already liked!", type: "error", open: true}))
                    return;
                }
            })

            if (response && response.data.code === 200) {

                const reviewIndex = productReviewList.findIndex(reviews => reviews.id === reviewId);
                
                if (reviewIndex !== -1) {

                    dispatch(addLikeToProductReview(reviewIndex));
                }

            }
        }

        else {

            dispatch(showSnackbar({message: "You need to login first to like/dislike this review!", type: "error", open: true}))
            return;
        }
    }

    const onDislikeReview = async (reviewId) => {

        if (Object.keys(userDetails).length > 0) {

            const response = await axiosInstance.post('/dislike', {review_id: reviewId}, {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }            
            }).catch(err => {
                
                if (err.response && err.response.data.code === 422) {

                    dispatch(showSnackbar({message: "Review already disliked!", type: "error", open: true}))
                    return;
                }
            });

            if (response && response.data.code === 200) {

                const reviewIndex = productReviewList.findIndex(reviews => reviews.id === reviewId);
                if (reviewIndex !== -1) {

                    dispatch(addDislikeToProductReview(reviewIndex));
                }

            }
        }

        else {

            dispatch(showSnackbar({message: "You need to login first to like/dislike this review!", type: "error", open: true}))
            return;
        }
    }

    const onAddItemToCartHandler = () => {

        if (Object.keys(userDetails).length === 0) return history.push('/login');

        const itemDetails = {productId, productQuantity: 1, productPrice, productName, productImg: productImage};
        
        const itemIndex = cartProducts.findIndex(product => product.productId === productId);
        
        if (itemIndex >= 0) {

            itemDetails.productQuantity = cartProducts[itemIndex].productQuantity + 1;
            dispatch(addItemToCart(itemDetails, itemIndex));

        }
        
        else dispatch(addItemToCart(itemDetails));

    }

    const onAddItemToWishlistHandler = () => {

        if (Object.keys(userDetails).length === 0) return history.push('/login');
        
        dispatch(addItemToWishlist(productId));

    }

    return (
        <div class="container shop">
            <div class="product-detail__wrapper">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <ProductImages productImages = {productImages} />
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="product-detail__content">
                            <div class="product-detail__content">
                                <div class="product-detail__content__header">
                                { productCategory ? <h5>{productCategory}</h5> : null }
                                <h2>{productName}</h2>
                                </div>
                                
                                { productTotalRating > 0 &&
                                    <div class="product-detail__content__header__comment-block">
                                        <div class="avg_rate">
                                            <span>{productAverageRating}</span>
                                            <Star/>
                                        </div>
                                        <div class="total_rating ml-2">
                                            ({productTotalRating})
                                        </div>
                                        {/* <div class="rate">
                                            {Array.from({length: productTotalRating}).map(item => (
                                                <i className="fas fa-star"></i>
                                            ))}
                                            
                                            {5 - productTotalRating > 0 ? 
                                                Array.from({length: (5-productTotalRating)}).map(item => (
                                                    <i className="far fa-star"></i>
                                                ))
                                                : null
                                            }
                                        </div> */}
                                    </div>
                                }
                                <div className="product-content__footer">
                                    {discountPrice ? 
                                        <React.Fragment>
                                            <h3>${discountPrice}</h3>
                                            <h3 className="product-price--discount ml-2">${productPrice}</h3>            
                                        </React.Fragment>
                                    :
                                        <h3>${productPrice}</h3>
                                    }
                                </div>
                                <div class="divider"></div>
                                <p style={{maxHeight: "100%"}} class="product-description">
                                    {productKeywords.filter(keyword => keyword.trim().length !== 0).map(keyword => (
                                        <Chip
                                            key={keyword}
                                            className="mr-2 mb-2"
                                            size="small"
                                            label={keyword}
                                        />
                                    ))}
                                </p>
                                <div class="product-detail__content__footer">
                                    <ul className="mt-3">
                                        <li>Product ID: {productId}</li>
                                        <li>Status: {productStatus ? productStatus: "-"}</li>
                                        <li>Availability: {productAvailability ? "In Stock" : "Out of Stock"}</li>
                                    </ul>

                                    {/* <div class="product-detail__colors"><span>Color:</span>
                                        <div class="product-detail__colors__item" style={{backgroundColor: "#8B0000"}}></div>
                                        <div class="product-detail__colors__item" style={{backgroundColor: "#4169E1"}}></div>
                                    </div> */}

                                    <div class="product-detail__controller">
                                            {/* <div class="quantity-controller -border -round">
                                            <div onClick={onDecreaseQuantity} class="quantity-controller__btn -descrease">-</div>
                                            <div class="quantity-controller__number">{quantity ? quantity : 0}</div>
                                            <div onClick={onIncreaseQuantity} class="quantity-controller__btn -increase">+</div>
                                            </div> */}
                                            <div onClick={onAddItemToCartHandler} class="add-to-cart -dark"><a class="btn -round -red" href=" "><i class="fas fa-shopping-bag"></i></a>
                                            <h5>Add to cart</h5>
                                            </div>
                                        <div class="product-detail__controler__actions"></div><button onClick={onAddItemToWishlistHandler} class="btn -round -white" href=" "><i class="fas fa-heart"></i></button>
                                    </div>
                                    <div class="divider"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mt-5 col-12">
                        <MoreDetails 
                            onDeleteProductReview={onDeleteProductReview}
                            productReviewList={productReviewList}
                            onSubmitProductReview={onSubmitProductReview}
                            productDescription={productDescription}
                            productReviewDetails={productReviewDetails}
                            changeProductReviewDetails={changeProductReviewDetails}
                            onChangeRating={onChangeRating}
                            imageFiles={imageFiles}
                            imagesForPreview={imagesForPreview}
                            setImageFiles={setImageFiles}
                            setImagesForPreview={setImagesForPreview}
                            canUserReview={canUserReview}
                            onLikeReview={onLikeReview}
                            onDislikeReview={onDislikeReview}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductDetails
