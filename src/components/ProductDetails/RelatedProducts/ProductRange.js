import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import StaticProductImg from '../../../assets/images/product/1.jpg';
import axiosInstance from '../../../axios/axios-instance';
import { setActiveCategory, setLoadingState, setPaginationLinks, setProductList } from '../../../redux/action/action';

function ProductRange({
    categoryImage,
    currentCategory,
    parentCategory,
    productCount
}) {

    const match = {'/': '-', '&': 'and', ',': '', ' ': '-'};
    const dispatch = useDispatch();
    const {pageLimit} = useSelector(state => state.products);
    
    const getProductListByKeyword = async (limit, title) => {

        dispatch(setLoadingState(true));

        const response = await axiosInstance.get('/getProductByKeyword', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("access_token_wappy")
            },
            params: {
                limit,
                search_text: title
            }
        }).catch(err => console.log(err))

        if (response && response.data && response.data.code === 200) {

            dispatch(setActiveCategory(title));
            dispatch(setProductList(response.data.data && response.data.data.data));
            dispatch(setPaginationLinks(response.data.data && response.data.data.links));
            dispatch(setLoadingState(false));
        }
    }
    
    return (
        <div className="product">
            <div className="product-thumb">
                <Link onClick={() => getProductListByKeyword(pageLimit, currentCategory)} className="product-thumb__image" to={`/categories${parentCategory ? '/'+parentCategory.replace(/[/ /&/,]/g, ch => match[ch])+'/' : '/'}${currentCategory.replace(/[/ /&/,]/g, ch => match[ch])}`}>
                    <img style={{objectFit: "contain"}} src={categoryImage ? categoryImage: StaticProductImg} alt="Category Img"/>
                    <img style={{objectFit: "contain"}} src={categoryImage ? categoryImage: StaticProductImg} alt="Category Img"/>
                </Link>
            </div>
            <div className="product-content">
                <div className="product-content__header">
                    { currentCategory ? <div className="product-category">{currentCategory} ({productCount})</div> : null }
                </div>
                { parentCategory &&
                    <div className="product-content__footer">            
                        <h5 className="product-price--main">{parentCategory}</h5>
                    </div>
                }
            </div>
        </div>
    )
}

export default ProductRange
