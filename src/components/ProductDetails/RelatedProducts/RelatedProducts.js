import React from 'react'
import Slider from 'react-slick'
import ProductRange from './ProductRange'
import { product_list } from '../../../data/dummy/product_list'
import { NextArrow, PrevArrow } from "../../../utils/SlideArrow/SlideArrow";

function RelatedProducts({
  relatedProducts
}) {

  const settings = {
    centerMode: true,
    centerPadding: "250px",
    slidesToShow: relatedProducts.length > 6 ? 4 : relatedProducts.length < 6 ? relatedProducts.length : 1,
    prevArrow: < PrevArrow / > ,
    nextArrow: < NextArrow / > ,
    responsive: [{
        breakpoint: 1500,
        settings: {
          centerPadding: "200px",
          slidesToShow: relatedProducts.length > 6 ? 3 : relatedProducts.length < 6 ? relatedProducts.length : 1
        },
      },
      {
        breakpoint: 1140,
        settings: {
          centerPadding: "100px",
          slidesToShow: relatedProducts.length > 6 ? 3 : relatedProducts.length < 6 ? relatedProducts.length : 1,
        },
      },
      {
        breakpoint: 992,
        settings: {
          centerPadding: "100px",
          slidesToShow: relatedProducts.length > 6 ? 2 : relatedProducts.length < 6 && relatedProducts.length > 1 ? 2 : 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          // centerMode: false,
          centerPadding: "40px",
          slidesToShow: relatedProducts.length > 6 ? 2 : relatedProducts.length < 6 && relatedProducts.length > 1 ? 2 : 1,
        },
      },
      {
        breakpoint: 516,
        settings: {
          // centerMode: false,
          centerPadding: "40px",
          slidesToShow: 1,
        },
      }
    ],
  };
    
    return (
        
        relatedProducts.length > 0 &&
          <div class="product-slide">
            <div class="container">
              <div class="section-title -center" style={{marginBottom: "1.875em"}}>
                <h2>Product Range</h2>
              </div>
            </div>
            <div className="product-tab-slide__content">
              <div class="product-slider">
                <Slider {...settings}>
                    {relatedProducts.map(product => (
                        <div key={product.id} className="product-slide__item">
                            <ProductRange
                              key={product.id}
                              categoryImage={product.category.image_url ? product.category.image_url : ""}
                              currentCategory={product.category.title ? product.category.title : ""}
                              parentCategory={product.category && product.category.parent_category_name}
                              productCount = {product.category && product.category.products_count}
                            />
                        </div>
                    ))}
                </Slider>
              </div>
            </div>
          </div>
    )
}

export default RelatedProducts
