import React from 'react'
import Parser from 'html-react-parser'

function ProductDescription({
    productDescription
}) {

    let updatedProductDescription = "";
    for (let i=0; i<productDescription.length; i++) {

        if (productDescription[i] === '\\' && productDescription[i+1] === 'n') {
            i++;
        } else {
            updatedProductDescription += productDescription[i]
        }
    }

    return (
        <div class="tab-content__item -description">
            { 
                productDescription ? 
                    <div 
                        className="prod-description"
                        // dangerouslySetInnerHTML={{ __html: productDescription.trim().replace('\n', "") }} 
                    >
                        {Parser(updatedProductDescription)}
                    </div>
                    : 
                    "No Description" 
            }
        </div>

    )
}

export default ProductDescription
