import React, { useState } from 'react'
import ProductDescription from './ProductDescription'
import ProductReview from './ProductReview'
import ProductShipping from './ProductShipping'

function MoreDetails({
    productDescription,
    changeProductReviewDetails,
    productReviewDetails,
    onSubmitProductReview,
    productReviewList,
    onDeleteProductReview,
    onEditProductReview,
    onChangeRating,
    imageFiles,
    imagesForPreview,
    setImageFiles,
    setImagesForPreview,
    canUserReview,
    onLikeReview,
    onDislikeReview
}) {

    const [currentActiveTab, setCurrentActiveTab] = useState(0);

    return (
        <div className="product-detail__content__tab">
            <ul className="tab-content__header">
                <li onClick={() => setCurrentActiveTab(0)} className={`${currentActiveTab === 0 ? 'active' : ''} tab-switcher`}>Description</li>
                <li onClick={() => setCurrentActiveTab(1)}  className={`${currentActiveTab === 1 ? 'active' : ''} tab-switcher`}>Reviews ( {productReviewList.length} )</li>
            </ul>
            <div id="allTabsContainer">
                {
                    currentActiveTab === 0 ? <ProductDescription productDescription={productDescription}/> 
                    : currentActiveTab === 1 ? 
                        <ProductReview
                            imageFiles={imageFiles}
                            imagesForPreview={imagesForPreview}
                            setImageFiles={setImageFiles}
                            setImagesForPreview={setImagesForPreview}
                            onChangeRating={onChangeRating}
                            onDeleteProductReview={onDeleteProductReview}
                            productReviewList={productReviewList}
                            onSubmitProductReview={onSubmitProductReview}
                            productReviewDetails={productReviewDetails}
                            changeProductReviewDetails={changeProductReviewDetails}
                            canUserReview={canUserReview}
                            onLikeReview={onLikeReview}
                            onDislikeReview={onDislikeReview}
                        />
                    : null
                }
            </div>
        </div>
    )
}

export default MoreDetails
