import React, { useState } from 'react'
import moment from 'moment'
import { CameraAlt, Cancel, Delete, Edit, ThumbDownAltSharp, ThumbsUpDown, ThumbUpAltSharp } from '@material-ui/icons'
import { useDispatch, useSelector } from 'react-redux'
import { IconButton, Tooltip } from '@material-ui/core';
import ProductReviewUpdateDialog from './ProductReviewUpdateDialog/ProductReviewUpdateDialog';
import { setProgressState, showSnackbar, updateProductReview } from '../../../redux/action/action';
import axiosInstance from '../../../axios/axios-instance';
import Rating from 'react-rating';
import FullStar from '../../../assets/images/star-full.png';
import EmptyStar from '../../../assets/images/star-empty.png';
import ProductReviewDeleteDialog from './ProductReviewDeleteDialog/ProductReviewDeleteDialog';
import Logo from '../../../assets/images/logo.png';

function ProductReview({
    productReviewDetails,
    changeProductReviewDetails,
    onSubmitProductReview,
    productReviewList,
    onDeleteProductReview,
    onChangeRating,
    imageFiles,
    imagesForPreview,
    setImageFiles,
    setImagesForPreview,
    canUserReview,
    onLikeReview,
    onDislikeReview
}) {

    const {userDetails} = useSelector(state => state.auth);

    const dispatch = useDispatch();
    
    const [openProductReviewUpdateDialog, setOpenProductReviewUpdateDialog] = useState(false);
    const [productReviewDetailsToEdit, setProductReviewDetailsToEdit] = useState(null);
    const [subject, setSubject] = useState('');
    const [rate, setRate] = useState(0);
    const [description, setDescription] = useState('');
    const [openProductReviewDeleteDialog, setOpenProductReviewDeleteDialog] = useState(false);
    const [activeProductReview, setActiveProductReview] = useState(null);
    const [imageUrls, setImageUrls] = useState([]);
    const [imageFilesToBeAdded, setImageFilesToBeAdded] = useState([]);
    const [imagesForPreviewToBeAdded, setImagesForPreviewToBeAdded] = useState([]);

    const isFileSizeValid = (fileList) => {

        let isFileSizeGreater = false;

        for (let file of fileList) {

            if (Math.floor(file.size / 1000) > 1000) {

                isFileSizeGreater = true;
                break;
            }
        }

        if (isFileSizeGreater) {

            dispatch(showSnackbar({message: 'You cannot upload file with size of more than 1MB!', type: 'warning', open: 'true'}));
            return false;

        }

        else return true;
        
    }

    const onFileChangeHandler = event => {

        const fileList = event.target.files;

        if (fileList.length > 0) {
            
            if (isFileSizeValid(fileList)) {

                for (let i = 0; i < fileList.length; i++) {

                    convertIntoBase64(fileList[i]);

                    if (openProductReviewUpdateDialog) setImageFilesToBeAdded(imageFilesToBeAdded => imageFilesToBeAdded.concat(fileList[i]));
                    else setImageFiles(imageFiles => imageFiles.concat(fileList[i]));

                }

            }

        }
    }

    const convertIntoBase64 = (file) => {
        
        let pattern = /image-*/;
        let fileReader = new FileReader();
        
        if (file.type.match(pattern)) {

            fileReader.onloadend = fileReaderLoaded.bind(this);
            fileReader.readAsDataURL(file);

        } 
        
        else {

            dispatch(showSnackbar({message: "You can only upload .jpg or .png files!", type: "error", open: "true"}));                
            return;

        }
    }

    const fileReaderLoaded = (e) => {
        
        let reader = e.target;
        let base64image = reader.result;
        let imageItem = {
            id: new Date().getTime(),
            url: base64image
        }

        if (openProductReviewUpdateDialog) setImagesForPreviewToBeAdded(imagesForPreviewToBeAdded => imagesForPreviewToBeAdded.concat(imageItem));
        else setImagesForPreview(image => image.concat(imageItem));

    }

    const onRemoveImageHandler = (fileId) => {

        if (openProductReviewUpdateDialog) {

            const updatedImageForPreviewList = [...imagesForPreviewToBeAdded];
            let updatedImageFiles = [...imageFilesToBeAdded];
            const indexOfRemovedImage = updatedImageForPreviewList.findIndex(imageItem => imageItem.id === fileId);
            updatedImageFiles = updatedImageFiles.filter((file, index) => index !== indexOfRemovedImage);
            setImageFilesToBeAdded(updatedImageFiles);
            setImagesForPreviewToBeAdded(imagesForPreviewToBeAdded => imagesForPreviewToBeAdded.filter(imageItem => imageItem.id !== fileId));
        
        } 
        
        else {
        
            const updatedImageForPreviewList = [...imagesForPreview];
            let updatedImageFiles = [...imageFiles];
            const indexOfRemovedImage = updatedImageForPreviewList.findIndex(imageItem => imageItem.id === fileId);
            updatedImageFiles = updatedImageFiles.filter((file,index) => index !== indexOfRemovedImage);
            setImageFiles(updatedImageFiles);
            setImagesForPreview(imagesForPreview => imagesForPreview.filter(imageItem => imageItem.id !== fileId));
        
        }
    }

    const onOpenProductReviewUpdateDialog = (review) => {

        setOpenProductReviewUpdateDialog(true);
        setSubject(review.subject);
        setRate(review.rate);
        setDescription(review.description);
        setProductReviewDetailsToEdit(review);
        setImageUrls(review.images);

    }

    const onCloseUpdateReviewDialog = () => {

        setSubject('');
        setDescription('');
        setRate('');
        setImageUrls([]);
        setOpenProductReviewUpdateDialog(false);
        setImageFilesToBeAdded([]);
        setImagesForPreviewToBeAdded([]);

    }

    const onOpenProductReviewDeleteDialog = (review) => {

        setActiveProductReview(review);
        setOpenProductReviewDeleteDialog(true);
        
    }

    const onConfirmDeleteProductReview = () => {
        
        onDeleteProductReview(activeProductReview?.id);
        setOpenProductReviewDeleteDialog(false);
        setActiveProductReview(null);

    }

    const onCloseProductReviewDeleteDialog = () => {

        setOpenProductReviewDeleteDialog(false);
        setActiveProductReview(null);
        
    }

    const isProductReviewDetailsValid = () => {
        
        if (!subject.trim() || !description.trim()) {

            dispatch(showSnackbar({
                message: "Subject and Description cannot be empty!",
                type: "warning",
                open: true
            }))
            return;

        }

        if ((rate) > 0 && ((rate) < 1 || (rate) > 5)) {

            dispatch(showSnackbar({
                message: "Rating should be between 1 to 5 only!",
                type: "warning",
                open: true
            }))
            return;

        }

        return true;

    }
    
    const onEditProductReviewDetails = async () => {

        if (productReviewDetailsToEdit.id && isProductReviewDetailsValid()) {

            dispatch(setProgressState(true));
            
            const formData = new FormData();
            formData.append('review_id', productReviewDetailsToEdit.id);
            formData.append('subject', subject);
            formData.append('description', description);
            formData.append('rate', rate);

            for (let i = 0; i < imageFilesToBeAdded.length; i++) {

                formData.append(`files[${i}]`, imageFilesToBeAdded[i]);

            }

            const response = await axiosInstance.post('/updateProductReview', formData, 
            {
                headers: {
                    Authorization: "Bearer " +localStorage.getItem("access_token_wappy")
                }
            }).catch(err => console.log(err))

            if (response && response.data.code === 200) {

                const reviewIndex = productReviewList.findIndex(review => review.id === productReviewDetailsToEdit.id);
                if (reviewIndex !== -1) {

                    const reviewData = {...productReviewDetailsToEdit};
                    reviewData.subject = subject;
                    reviewData.rate = rate;
                    reviewData.description = description;
                    reviewData.created_at = new Date();
                    reviewData.images = [...productReviewDetailsToEdit.images, ...imagesForPreviewToBeAdded];
                    dispatch(updateProductReview(reviewIndex, reviewData));
                    dispatch(setProgressState(false));
                    dispatch(showSnackbar({message: "Review updated successfully!", type: "success", open: true}));
                    onCloseUpdateReviewDialog();
                }

            }
        }

    }

    return (
        <div class="tab-content__item -review">
            {
                productReviewList.length > 0 ? productReviewList.map(review => (

                    <div key={review.id} class="review">
                        <div class="review__header">
                            <div class="review__header__avatar">
                                <img src={review?.user?.image_url} alt="Reviewer avatar"/></div>
                            <div class="review__header__info">
                                <h5>{review?.user?.name}</h5>
                                <p>{moment(review?.created_at).format('ll')}</p>
                            </div>
                            { review.user_id === userDetails.id &&
                                <Tooltip arrow title="Edit">
                                    <Edit 
                                        onClick={() => onOpenProductReviewUpdateDialog(review)}
                                        color="primary"
                                        className="align-self-start ml-2" 
                                        style={{fontSize: "1.2rem", cursor: "pointer", marginTop: "0.2rem"}}
                                    />
                                </Tooltip>
                            }
                            { review.user_id === userDetails.id &&
                                <Tooltip arrow title="Delete">
                                    <Delete 
                                        onClick={() => onOpenProductReviewDeleteDialog(review)}
                                        color="secondary"
                                        className="align-self-start ml-2" 
                                        style={{fontSize: "1.2rem", cursor: "pointer", marginTop: "0.2rem"}}
                                    />
                                </Tooltip>
                            }
                            
                            {   review.rate > 0 &&

                                <div class="review__header__rate">
                                    <div class="rate" style={{position: "absolute", right: "16px"}}>
                                        {Array.from({length: review.rate}).map(item => (
                                            <i className="fas fa-star"></i>
                                        ))}
                                        
                                        {5 - review.rate > 0 ? 
                                            Array.from({length: (5-review.rate)}).map(item => (
                                                <i className="far fa-star"></i>
                                            ))
                                            : null
                                        }
                                    </div>
                                    <div style={{marginTop: "20px"}}>
                                        <ThumbUpAltSharp onClick={() => onLikeReview(review.id)} style={{fontSize: "0.9rem", marginTop: "4px", cursor: "pointer", color: review.has_logged_user_liked_review && "green" }}/> 
                                        <span className="ml-1" style={{fontSize: "0.9rem"}}>{review.likes ? `(${review.likes})` : "(0)"}</span>
                                        <ThumbDownAltSharp className="ml-1" onClick={() => onDislikeReview(review.id)} style={{fontSize: "0.9rem", marginTop: "4px", cursor: "pointer", color: review.has_logged_user_disliked_review && "crimson"}} className="ml-2"/> 
                                        <span className="ml-1" style={{fontSize: "0.9rem"}}>{review.dislikes ? `(${review.dislikes})` : "(0)"}</span>
                                    </div>
                                </div>
                            }
                            {
                                !review.rate && 
                                <div style={{marginLeft: review.rate ? "10px": "auto"}}>
                                    <ThumbUpAltSharp onClick={() => onLikeReview(review.id)} style={{fontSize: "0.9rem", marginTop: "4px", cursor: "pointer", color: review.has_logged_user_liked_review && "green" }}/> 
                                    <span className="ml-1" style={{fontSize: "0.9rem"}}>{review.likes ? `(${review.likes})` : "(0)"}</span>
                                    <ThumbDownAltSharp className="ml-1" onClick={() => onDislikeReview(review.id)} style={{fontSize: "0.9rem", marginTop: "4px", cursor: "pointer", color: review.has_logged_user_disliked_review && "crimson"}} className="ml-2"/> 
                                    <span className="ml-1" style={{fontSize: "0.9rem"}}>{review.dislikes ? `(${review.dislikes})` : "(0)"}</span>
                                </div>
                            }
                        </div>
                        <p class="review__content mb-0"><strong>Subject - </strong> {review.subject}</p>
                        <p class="review__content"><strong>Description - </strong> {review.description}</p>
                        { review.images.length > 0 &&
                            <div className="row">
                                <div class="col-12">
                                    <h5 className="mb-2">Photos:</h5>
                                    <div class="d-flex flex-wrap">
                                        { review.images.map(item => (

                                                <div key={item.id} className="image-preview image-preview-review">
                                                    <img 
                                                        src={item.url}
                                                        className="mr-3 mb-3"
                                                    />
                                                </div>
                                    
                                        ))}
                                    </div>
                                </div>
                            </div>
                        }
                        <ProductReviewUpdateDialog
                            openProductReviewUpdateDialog={openProductReviewUpdateDialog}
                            productReviewDetailsToEdit={productReviewDetailsToEdit}
                            onCloseUpdateReviewDialog={onCloseUpdateReviewDialog}
                            onEditProductReviewDetails={onEditProductReviewDetails}
                            setRate={setRate}
                            setDescription={setDescription}
                            setSubject={setSubject}
                            rate={rate}
                            description={description}
                            subject={subject}
                            imageUrls={imageUrls}
                            setImageFilesToBeAdded={setImageFilesToBeAdded}
                            imageFilesToBeAdded={imageFilesToBeAdded}
                            setImagesForPreviewToBeAdded={setImagesForPreviewToBeAdded}
                            imagesForPreviewToBeAdded={imagesForPreviewToBeAdded}
                            onFileChangeHandler={onFileChangeHandler}
                            onRemoveImageHandler={onRemoveImageHandler}
                        />

                        <ProductReviewDeleteDialog
                            onConfirmDeleteProductReview={onConfirmDeleteProductReview}
                            onCloseProductReviewDeleteDialog={onCloseProductReviewDeleteDialog}
                            openProductReviewDeleteDialog={openProductReviewDeleteDialog}
                        />
                    </div>
                ))
                : <h5>No Reviews.</h5>
            }
            
            { canUserReview ?
                <form onSubmit={onSubmitProductReview}>
                    <h5>Write a review</h5>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="input-validator">
                            <input type="text" value={productReviewDetails && productReviewDetails.subject} required name="subject" onChange={changeProductReviewDetails} placeholder="Subject*"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 d-flex mb-4">
                            <h4 className="mt-2 mr-2">Rating :</h4>
                            <Rating
                                initialRating={productReviewDetails && productReviewDetails.rating}
                                onChange={(value) => onChangeRating(value)}
                                emptySymbol={<img src={FullStar} style={{width: "25px", height: "25px", marginLeft: "5px"}} />}
                                fullSymbol={<img src={EmptyStar} style={{width: "25px", height: "25px", marginLeft: "5px"}} />}
                            />
                        </div>
                        <div class="col-12">
                            <div class="input-validator">
                            <textarea name="description" value={productReviewDetails && productReviewDetails.description} required onChange={changeProductReviewDetails} placeholder="Description*" rows="5"></textarea>
                            </div><span class="input-error"></span>
                        </div>
                        <div class="col-12">
                            <input onChange={onFileChangeHandler} id="media-upload" style={{display: "none"}} type="file" accept=".jpg, .png" multiple/>
                            <label htmlFor="media-upload">
                                <Tooltip title="Add Photos" arrow>
                                    <CameraAlt className="mb-3" style={{cursor:"pointer"}}/>
                                </Tooltip>
                            </label>
                        </div>
                        <div class="col-12">
                            <div class="d-flex flex-wrap">
                                { imagesForPreview.length > 0 && imagesForPreview.map(item => (

                                    <div key={item.id} className="image-preview">
                                        <Cancel 
                                            className="image-preview-cancel"
                                            onClick={() => onRemoveImageHandler(item.id)}
                                        />
                                        <img 
                                            src={item.url}
                                            className="mr-3 mb-3"
                                        />
                                    </div>
                                
                                ))}
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn -dark">Write a review
                            </button>
                        </div>
                    </div>
                </form>
                : null
            }
        </div>
    )
}

export default ProductReview
