import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core'
import { Cancel } from '@material-ui/icons'
import React from 'react'

function ProductReviewDeleteDialog({
    onCloseProductReviewDeleteDialog,
    openProductReviewDeleteDialog,
    onConfirmDeleteProductReview
}) {
    return (
        <Dialog open={openProductReviewDeleteDialog} onClose={openProductReviewDeleteDialog}>
            <DialogTitle>
                <div className="d-flex align-items-center">
                    <Cancel style={{color : "crimson", marginRight: "5px"}}/>
                    <span>Delete Product Review</span>
                </div>
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Do you want to remove this product review? This action cannot be undone.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" className="btn -red" onClick={onCloseProductReviewDeleteDialog}>Cancel</Button>
                <Button variant="contained" className="btn -dark" onClick={onConfirmDeleteProductReview}>Yes</Button>
            </DialogActions>
        </Dialog>
    )
}

export default ProductReviewDeleteDialog
