import React from 'react'

function ProductShipping() {
    return (
        <div class="tab-content__item -ship">
            <h5><span>Ship to </span>New york</h5>
            <ul>
                <li>Standard Shipping on order over 0kg - 5kg. <span>+10.00</span></li>
                <li>Heavy Goods Shipping on oder over 5kg-10kg . <span>+20.00</span></li>
            </ul>
            <h5>Delivery &amp; returns</h5>
            <p>We diliver to over 100 countries around the word. For full details of the delivery options we offer, please view our Delivery information.</p>
        </div>
    )
}

export default ProductShipping
