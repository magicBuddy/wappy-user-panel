import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Tooltip } from '@material-ui/core'
import React from 'react'
import Rating from 'react-rating'
import FullStar from '../../../../assets/images/star-full.png';
import EmptyStar from '../../../../assets/images/star-empty.png';
import { CameraAlt, Cancel } from '@material-ui/icons';

function ProductReviewUpdateDialog({
    onCloseUpdateReviewDialog,
    onEditProductReviewDetails,
    openProductReviewUpdateDialog,
    setSubject,
    setDescription,
    setRate,
    rate,
    description,
    subject,
    imageUrls,
    onFileChangeHandler,
    onRemoveImageHandler,
    imagesForPreviewToBeAdded
}) {

    
    return (
        <Dialog open={openProductReviewUpdateDialog} onClose={onCloseUpdateReviewDialog}>
            <DialogTitle>Update Review</DialogTitle>
            <DialogContent>               
                    
                <TextField 
                    id="subject" 
                    fullWidth 
                    className="mb-1"
                    required
                    label="Subject"
                    name="subject" 
                    value={subject}
                    onChange={(e) => setSubject(e.target.value)}
                />

                <TextField 
                    id="description" 
                    fullWidth 
                    required
                    className="mb-1"
                    label="Description"
                    name="description"
                    value={description} 
                    onChange={(e) => setDescription(e.target.value)}
                />

                <p style={{fontSize: "0.8rem"}} className="mt-2 mb-2 mr-2">Rating :</p>
                <Rating
                    initialRating={rate}
                    onChange={(value) => setRate(value)}
                    emptySymbol={<img src={FullStar} style={{width: "25px", height: "25px", marginLeft: "5px"}} />}
                    fullSymbol={<img src={EmptyStar} style={{width: "25px", height: "25px", marginLeft: "5px"}} />}
                />

                { imageUrls?.length > 0 &&
                    <div className="row">
                        <div class="col-12">
                            <p style={{fontSize: "0.8rem"}} className="mt-2 mb-2 mr-2">Photos :</p>
                            <div class="d-flex flex-wrap">
                                { imageUrls.map(item => (

                                        <div key={item.id} className="image-preview image-preview-update">
                                            <img 
                                                src={item.url}
                                                className="mr-3 mb-3"
                                            />
                                        </div>
                            
                                ))}
                            </div>
                        </div>
                    </div>
                }

                { imagesForPreviewToBeAdded.length > 0 &&
                    <div className="row">
                        <div class="col-12">
                            <p style={{fontSize: "0.8rem"}} className="mt-2 mb-2 mr-2">New Photos :</p>
                            <div class="d-flex flex-wrap">
                                { imagesForPreviewToBeAdded.map(item => (

                                    <div key={item.id} className="image-preview">
                                        <Cancel 
                                            className="image-preview-cancel"
                                            onClick={() => onRemoveImageHandler(item.id)}
                                        />
                                        <img 
                                            src={item.url}
                                            className="mr-3 mb-3"
                                        />
                                    </div>
                            
                                ))}
                            </div>
                        </div>
                    </div>
                }

                <div class="col-12">
                    <input onChange={onFileChangeHandler} id="photos-upload" style={{display: "none"}} type="file" accept=".jpg, .png" multiple/>
                    <label htmlFor="photos-upload">
                        <Tooltip title="Add new Photos" arrow>
                            <CameraAlt className="mb-3" style={{cursor:"pointer"}}/>
                        </Tooltip>
                    </label>
                </div>

            </DialogContent>
            <DialogActions>
                <Button variant="contained" className="btn -red" onClick={onCloseUpdateReviewDialog}>Cancel</Button>
                <Button variant="contained" className="btn -dark" onClick={onEditProductReviewDetails}>Update</Button>
            </DialogActions>
        </Dialog>
    )
}

export default ProductReviewUpdateDialog
