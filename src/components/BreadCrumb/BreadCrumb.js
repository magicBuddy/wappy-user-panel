import React from 'react'

function BreadCrumb({
    title,
    productName,
    subTitle
}) {
    return (
        <div className="breadcrumb">
            <div className="container">
                <h2>{title}</h2>
                <ul>
                    <li style={{marginBottom: "10px"}}>Home</li>
                    {
                        productName && subTitle ? 
                            <React.Fragment>
                                <li>{subTitle}</li>
                                <li style={{lineHeight: "20px"}} class="active">{productName}</li>
                            </React.Fragment>
                        : 
                        <li className="active">{title}</li>
                    }
                </ul>
            </div>
        </div>
    )
}

export default BreadCrumb
