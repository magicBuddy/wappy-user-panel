import { Button, Dialog, DialogActions, DialogContent, DialogContentText } from '@material-ui/core'

import React from 'react'

function DeleteItemConfirmationDialog({
    openDeleteItemConfirmationDialog,
    setOpenDeleteItemConfirmationDialog,
    onDeleteItemHandler
}) {

    return (

        <Dialog
        
            open={openDeleteItemConfirmationDialog}
        
            onClose={() => setOpenDeleteItemConfirmationDialog(false)}
        
            aria-labelledby="alert-dialog-title"
        
            aria-describedby="alert-dialog-description"
        
        >
        
            <DialogContent>
        
                <DialogContentText id="alert-dialog-description">
        
                Do you want to remove this product from cart? This action cannot be
                undone.
        
                </DialogContentText>
        
            </DialogContent>
        
            <DialogActions>
        
                <Button
        
                onClick={() => setOpenDeleteItemConfirmationDialog(false)}
        
                color="primary"
        
                >
        
                No
        
                </Button>
        
                <Button
        
                onClick={onDeleteItemHandler}
        
                color="primary"
        
                autoFocus
        
                >
        
                Yes
        
                </Button>
        
            </DialogActions>
        
        </Dialog>
    
    )

}

export default DeleteItemConfirmationDialog
