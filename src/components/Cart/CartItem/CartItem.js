import { Link } from '@material-ui/core';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import ProductImg1 from '../../../assets/images/product/1.jpg';
import { addItemToCart, decreaseQuantityFromCart, removeItemFromCart } from '../../../redux/action/action';
import DeleteItemConfirmationDialog from './DeleteItemConfirmationDialog';

function CartItem({
    productImg,
    productName,
    productQuantity,
    productPrice,
    productId,
    cartProductId
}) {

    const dispatch = useDispatch()
    const {cartProducts} = useSelector(state => state.cart);
    const [openDeleteItemConfirmationDialog, setOpenDeleteItemConfirmationDialog] = useState(false);
    const onRemoveCartItem = () => dispatch(removeItemFromCart(productId, cartProductId));

    const onDeleteItemHandler = () => {

        const itemIndex = cartProducts.findIndex(product => product.productId === productId);
        
        if (itemIndex >= 0) {

            setOpenDeleteItemConfirmationDialog(false);
            dispatch(decreaseQuantityFromCart(cartProductId, itemIndex, productQuantity));
            
        }

    }

    const onDecreaseQuantity = () => {
        
        if (productQuantity === 1) {

            setOpenDeleteItemConfirmationDialog(true);
            return;
        }

        const itemIndex = cartProducts.findIndex(product => product.productId === productId);
        
        if (itemIndex >= 0) {

            dispatch(decreaseQuantityFromCart(cartProductId, itemIndex, productQuantity));

        }
        
    }

    const onIncreaseQuantity = () => {

        const itemDetails = {productId, productQuantity, productPrice, productName, productImg};
        const itemIndex = cartProducts.findIndex(product => product.productId === productId);
        
        if (itemIndex >= 0) {

            itemDetails.productQuantity = cartProducts[itemIndex].productQuantity + 1;
            dispatch(addItemToCart(itemDetails, itemIndex));

        }
        
    }
    
    return (
        <tr>
            <td>
                <div class="cart-product">
                    <div class="cart-product__image"><img src={productImg ? productImg : ProductImg1} alt="Product img"/></div>
                    <div class="cart-product__content">
                    <h5 style={{whiteSpace: "break-spaces", lineHeight: "1.5em"}}>{productName}</h5>
                    </div>
                </div>
            </td>
            <td>${productPrice}</td>
            <td>
                <div class="quantity-controller ">
                    <div class="quantity-controller__btn -descrease" onClick={onDecreaseQuantity}>-</div>
                    <div class="quantity-controller__number">{productQuantity}</div>
                    <div class="quantity-controller__btn -increase" onClick={onIncreaseQuantity}>+</div>
                </div>
            </td>
            <td>${(productQuantity * Number(productPrice)).toFixed(2)}</td>
            <td><Link to="/cart"><i onClick={onRemoveCartItem} class="fal fa-times"></i></Link></td>
            
            <DeleteItemConfirmationDialog
                openDeleteItemConfirmationDialog={openDeleteItemConfirmationDialog}
                setOpenDeleteItemConfirmationDialog={setOpenDeleteItemConfirmationDialog}
                onDeleteItemHandler={onDeleteItemHandler}
            />

        </tr>
    )
}

export default CartItem
