import React from 'react';
import CartTotal from '../CartTotal/CartTotal';
import CartItem from '../CartItem/CartItem';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { clearShoppingCart } from '../../../redux/action/action';

function CartList({cartProducts}) {
        
    const dispatch = useDispatch();
    const onClearShoppingCart = () => dispatch(clearShoppingCart())
    
    return (
        <div class="container">
            <div class="cart">
                <div class="container">
                    { cartProducts.length > 0 ?
                        <div class="cart__table">
                            <div class="cart__table__wrapper">
                                <table>
                                    <colgroup>
                                        <col style={{width: "40%"}}/>
                                        <col style={{width: "17%"}}/>
                                        <col style={{width: "17%"}}/>
                                        <col style={{width: "17%"}}/>
                                        <col style={{width: "9%"}}/>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        {
                                            cartProducts.map(product => (
                                                <CartItem
                                                    key = {product.productId}
                                                    productImg = {product.productImg}
                                                    productName = {product.productName}
                                                    productQuantity = {product.productQuantity}
                                                    productPrice = {product.productPrice}
                                                    productId = {product.productId}
                                                    cartProductId = {product.cartProductId}
                                                />

                                            ))
                                        }
                                    
                                    </tbody>
                                </table>
                            </div>
                            <div class="cart__table__footer">
                                <Link to="/shop"><i class="fal fa-long-arrow-left"></i>Continue Shopping</Link>
                                <Link onClick={onClearShoppingCart} to="/cart"><i class="fal fa-trash"></i>Clear Shopping Cart</Link>
                            </div>
                        </div>
                    :
                    <React.Fragment>
                        <div class="cart__table__footer">
                            <Link to="/shop"><i class="fal fa-long-arrow-left"></i>Continue Shopping</Link>
                        </div>
                        <h5 className="mt-5">Your cart is empty.</h5>
                    </React.Fragment>
                    }
                    { cartProducts.length > 0 ? <CartTotal/> : null}
                </div>
            </div>
        </div>
    )
}

export default CartList
