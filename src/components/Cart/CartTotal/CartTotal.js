import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

function CartTotal() {

    const {totalCartPrice} = useSelector(state => state.cart);

    return (
        <div class="cart__total">
            <div class="row">
                {/* <div class="col-12 col-md-8">
                    <div class="cart__total__discount">
                    <p>Enter coupon code. It will be applied at checkout.</p>
                    <div class="input-validator">
                        <form action="">
                        <input type="text" name="discountCode" placeholder="Your code here"/>
                            <button class="btn -dark">Apply
                            </button>
                        </form>
                    </div>
                    </div>
                </div> */}
                <div class="col-12 col-md-4" style={{margin: "auto 0 auto auto"}}>
                    <div class="cart__total__content">
                    <h3>Cart</h3>
                    <table>
                        <tbody>
                        <tr>
                            <th>Subtotal</th>
                            <td>${totalCartPrice}</td>
                        </tr>
                        <tr>
                            <th>Total</th>
                            <td class="final-price">${totalCartPrice}</td>
                        </tr>
                        </tbody>
                    </table><Link class="btn -dark" to="/checkout">Proceed to checkout</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CartTotal
