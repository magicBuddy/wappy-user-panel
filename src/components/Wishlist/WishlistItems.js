import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { clearWishlist, addItemToCart, removeItemFromWishlist } from '../../redux/action/action';

function WishlistItems({wishlistProducts}) {

    const dispatch = useDispatch();    
    const history = useHistory();

    const {userDetails} = useSelector(state => state.auth);
    const {cartProducts} = useSelector(state => state.cart);

    const onRemoveWishlistItem = (wishlistId) => dispatch(removeItemFromWishlist(wishlistId));

    const onAddItemToCartHandler = (product) => {

        if (Object.keys(userDetails).length === 0) return history.push('/login');
        
        const itemDetails = {productId: product.id, productQuantity: 1, productPrice: product.price, productName: product.name, productImg: product.image_url};
        const itemIndex = cartProducts.findIndex(prod => prod.productId === product.id);
        
        if (itemIndex >= 0) {

            itemDetails.productQuantity = cartProducts[itemIndex].productQuantity + 1;
            dispatch(addItemToCart(itemDetails, itemIndex));

        }
        
        else dispatch(addItemToCart(itemDetails));

    }

    const onClearWishlist = () => dispatch(clearWishlist())
    
    return (
        <div class="wishlist">
            <div class="container">
            { wishlistProducts.length > 0 ?
                
                <div class="wishlist__table">
                    <div class="wishlist__table__wrapper">
                        <table>
                            <colgroup>
                                <col style={{width: "40%"}}/>
                                <col style={{width: "20%"}}/>
                                <col style={{width: "20%"}}/>
                                <col style={{width: "20%"}}/>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Stock</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            { wishlistProducts.map(wishlist => (
                                <tr key={wishlist.id}>
                                    <td>
                                        <div class="wishlist-product">
                                            <div class="wishlist-product__image">
                                                <img src={wishlist.product.image_url} alt={wishlist.product.name}/>
                                            </div>
                                            <div class="wishlist-product__content">
                                                <h5 style={{whiteSpace: "break-spaces", lineHeight: "1.5em"}}>{wishlist.product.name}</h5>
                                            </div>
                                        </div>
                                    </td>
                                    <td>${wishlist.product.price}</td>
                                    <td>{wishlist.product.in_stock ? "In stock" : "Out of stock"}</td>
                                    <td>
                                        <button onClick={() => onAddItemToCartHandler(wishlist.product)} class="btn -dark">Add to cart</button>
                                        <div class="remove-btn">
                                            <i onClick={() => onRemoveWishlistItem(wishlist.id)} class="fal fa-times"></i>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                    <div class="cart__table__footer">
                        <Link to="/shop"><i class="fal fa-long-arrow-left"></i>Continue Shopping</Link>
                        <Link onClick={onClearWishlist} to="/wishlist"><i class="fal fa-trash"></i>Clear Wishlist</Link>
                    </div>
                </div>
                :
                <React.Fragment>
                    <div class="cart__table__footer">
                        <Link to="/shop"><i class="fal fa-long-arrow-left"></i>Continue Shopping</Link>
                    </div>
                    <h5 className="mt-5">Your wishlist is empty.</h5>
                </React.Fragment>
            }
            </div>
        </div>
    )
}

export default WishlistItems
